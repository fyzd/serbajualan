<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<head>
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/bootstrap.css')}}">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/fonts/icomoon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/fonts/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/vendors/css/extensions/pace.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/vendors/css/ui/prism.min.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/colors.css')}}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/core/menu/menu-types/vertical-overlay-menu.css')}}">

    @yield('cssfield')
    <!-- END Page Level CSS-->
</head>

<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav">
                    <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
                    <li class="nav-item"><a href="" class="navbar-brand nav-link"><h2 style="color:white">Serbajualan</h2> <!--<img alt="branding logo" src="{{url('assets-admin/images/logo/sj.png')}}" data-expand="{{url('assets-admin/images/logo/sj.png')}}" data-collapse="{{url('assets-admin/images/logo/robust-logo-small.png')}}" height="30"class="brand-logo">--></a></li>
                    <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container content container-fluid">
                <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                    <ul class="nav navbar-nav">
                        <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5">         </i></a></li>
                        <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav float-xs-right">
                        
                        <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="{{url('assets/images/profile/'.Auth::user()->user_image->image)}}" alt="avatar"><i></i></span><span class="user-name">{{Auth::user()->user_name->name}}</span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!--<a href="#" class="dropdown-item"><i class="icon-head"></i> Edit Profile</a>-->
                                    <div class="dropdown-divider"></div><a href="{{url('logout')}}" class="dropdown-item"><i class="icon-power3"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
        <!-- main menu header-->
        <div class="main-menu-header">
        </div>
        <!-- / main menu header-->
        <!-- main menu content-->
        <div class="main-menu-content">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                <li class="{{Request::is('admin') ? 'active' : ''}} nav-item"><a href="{{url('admin')}}"><i class="icon-home3"></i><span data-i18n="nav.dash.main" class="menu-title">Dashboard</span></a>
                </li>
                <li class="{{Request::is('admin/transaction/*') ? 'active' : ''}} nav-item"><a href="#"><i class="icon-stack-2"></i><span data-i18n="nav.page_layouts.main" class="menu-title">Transaksi</span></a>
                    <ul class="menu-content">
                        <li><a href="{{url('admin/transaction/invoices')}}" class="menu-item">Tagihan</a>
                        </li>
                        <li><a href="{{url('admin/transaction/orders')}}" class="menu-item">Pesanan</a>
                        </li>
                    </ul>
                </li>
                <li class="{{Request::is('admin/income') ? 'active' : ''}} nav-item"><a href="{{url('admin/income')}}"><i class="icon-money"></i><span data-i18n="nav.page_layouts.main" class="menu-title">Pendapatan</span></a>
                </li>
            </ul>
        </div>
        <!-- /main menu content-->
        <!-- main menu footer-->
        <!-- include includes/menu-footer-->
        <!-- main menu footer-->
    </div>
    <!-- / main menu-->

    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            
            @yield('content')
                                
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-light navbar-border">
        <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Hak cipta &copy; 2017 <a href="https://pixinvent.com" target="_blank" class="text-bold-800 grey darken-2">Serbajualan </a></span>
            <span
                class="float-md-right d-xs-block d-md-inline-block"><!--Hand-crafted & Made with <i class="icon-heart5 pink"></i>--></span>
        </p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="{{URL::asset('assets-admin/js/core/libraries/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/tether.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/js/core/libraries/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/unison.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/blockUI.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/jquery.matchHeight-min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/screenfull.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/extensions/pace.min.js')}}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{URL::asset('assets-admin/js/core/app-menu.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/js/core/app.js')}}" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    @yield('jsfield')
    <!-- END PAGE LEVEL JS-->

    </body>
</html>