@extends('admin.templates.t_index')
@section('title', 'Dashboard')
@section('cssfield')
@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
        <h2 class="content-header-title">
        @if($status!=null)
            @foreach($status_detail as $s)
                @if($s->id==$status)
                Tagihan {{$s->description}}
                @endif
            @endforeach
        @else
        Semua Tagihan
        @endif
        </h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item"><a href="{{url('admin/transaction')}}">Transaksi</a>
            </li>
            <li class="breadcrumb-item">Tagihan
            </li>
        </ol>
        </div>
    </div>
</div>
<div class="content-body"><!-- Basic Tables start -->
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Filter
                    </h4>
                        <small class="btn-group">
                            <form action="{{url()->current()}}" method="get" id="form">
                                <select name="filter" class="form-control">
                                    <option value="">Semua status tagihan</option>
                                    @foreach($status_detail as $s)
                                    <option value="{{$s->id}}" {{$status==$s->id ? 'selected' : ''}}>{{$s->description}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </small>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <div class="table-responsive">
                            <table class="table" id="users-table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>No. Tagihan</th>
                                        <th>Pembeli</th>
                                        @if($status==null)
                                        <th>Status Pembayaran</th>
                                        @else
                                        <th>Barang yang dipesan</th>
                                        @endif
                                        <th>Waktu Pemesanan</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($invoices as $invoice)
                                    <tr>
                                        <td>{{$invoice->id}}</td>
                                        <td><a href="{{url('admin/transaction/invoices/'.$invoice->id)}}">{{$invoice->invoice_number}}</a></td>
                                        <td><a href="{{url('u/'.$invoice->order[0]->shipments->buyer_address->user->username)}}">{{$invoice->order[0]->shipments->buyer_address->user->user_name->name}}</a></td>
                                        @if($status==null)
                                        <td>{{$invoice->invoice_status->description}}</td>
                                        @else
                                        <td>
                                            @php $barang=0 @endphp
                                            @foreach($invoice->order as $o)
                                            @php $item=$o->order_item->count(); $barang+=$item @endphp
                                            @endforeach
                                            {{$barang}}
                                            barang dari {{$invoice->order->count()}} penjual
                                        </td>
                                        @endif
                                        <td>{{$invoice->created_at->diffForHumans()}}</td>
                                        <td>
                                            @if($invoice->invoice_status_id==1 || $invoice->invoice_status_id==2)
                                            <a class="btn btn-primary" href="{{url('admin/transaction/invoices/'.$invoice->id)}}" data-toggle="tooltip" title="Detail"><i class="icon-eye6"></i></a>
                                            
                                            <form action="{{url('admin/transaction/invoices/cancel')}}" method="post" class="d-inline">
                                                {{csrf_field()}}
                                                {{method_field('put')}}
                                                <input type="hidden" name="id" value="{{$invoice->id}}">
                                                <button type="submit" class="btn btn-danger" data-toggle="tooltip" title="Kedaluarsakan"><i class="icon-circle-cross"></i></button>
                                            </form>
                                            @else
                                            <a class="btn btn-primary" href="{{url('admin/transaction/invoices/'.$invoice->id)}}"><i class="icon-eye6"></i> Detail</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="text-xs-center">
                        {{$invoices->appends(Request::except('page'))->links()}}
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <!-- Basic Tables end -->


</div>
@endsection

@section('jsfield')
<script type="text/javascript">
    $('select[name=filter]').on('change', function(){
        $('#form').submit();
    })
</script>
@endsection