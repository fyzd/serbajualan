@extends('admin.templates.t_index')
@section('title', 'Dashboard')
@section('cssfield')
<link href="{{URL::asset('assets/css2/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/css2/dataTables.checkboxes.css')}}" rel="stylesheet">
@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
        <h2 class="content-header-title">Transaksi</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item"><a href="{{url('admin/transaction')}}">Transaksi</a>
            </li>
        </ol>
        </div>
    </div>
</div>
<div class="content-body"><!-- Basic Tables start -->
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Transaksi</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <table class="table table-condensed" id="users-table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                                <th>Id</th>
                                <th>No. Tagihan</th>
                                <th>Status Pembayaran</th>
                                <th>Waktu Pemesananan</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        </table>
                        <button class="btn btn-danger"id="hapus">Hapus</button>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <!-- Basic Tables end -->


</div>
@endsection

@section('jsfield')
<script src="{{URL::asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('assets/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('assets/js/dataTables.checkboxes.min.js')}}"></script>
<script>
$(function() {
    var table=$('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url("admin/transaction/datatables")}}',
            type: 'POST',
            data: { _token: '{{csrf_token()}}'}
        },
        columns: [
            { data: function(data){
                return '<input type="checkbox" name="id[]" value="'+ data.id +'" />';
            }, orderable: false, searchable:false, name:'id' },
            { data: 'id', name: 'id', 'searchable': false },
            { data: 'invoice_number', name: 'invoice' },
            { data: 'order_details', name: 'status'},
            { data: 'created_at', name: 'created'},
            { data: 'id', name: 'edit', 'searchable': false, selectRow: false, render:  
                function ( data, url, type, full) {
                    var url='{{url("admin/transaction")}}/'+data;
                    return '<a class="btn btn-success" href="'+url+'">Lihat Rincian</a>';
                }  
            },
        ],
        columnDefs: [
                {
                    targets: 0,
                    checkboxes: {
                        selectRow: true
                    }
                }
            ],
            select:{
                style: 'multi'
            },
            order: [[1, 'desc']]
        
    });
    $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.api().rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

    $('#users-table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });
   
   $('#hapus').on("click", function(event){
       var data = table.rows(' .selected ').data();
       $.each(data, function() {
           var key = Object.keys(this)[0];
            var value = this[key];
            var user_id={{Auth::id()}};
 
            $.ajax({
                type: "POST",
                url: "{{ url('') }}",
                data: {
                    "_method": "DELETE",
                    "_token": "{{ csrf_token() }}",
                    "id": value,
                    "user_id": user_id
                },
                success: function(result) {
                    $('#users-table').DataTable().ajax.reload();
                }
            });
            });
    });


       $('input[type=checkbox]').change( function(){
           if($('input[type=checkbox]:checked')) {
      $('#panel').slideToggle();
           }
   });
});
</script>
@endsection