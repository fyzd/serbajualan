@extends('admin.templates.t_index')
@section('title', 'Dashboard')
@section('cssfield')
@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
        <h2 class="content-header-title">
        @if($status!=null)
            @foreach($status_detail as $s)
                @if($s->id==$status)
                Pesanan {{$s->description}}
                @endif
            @endforeach
        @else
        Semua Pesanan
        @endif
        </h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item"><a href="{{url('admin/transaction')}}">Transaksi</a>
            </li>
            <li class="breadcrumb-item">Pesanan
            </li>
        </ol>
        </div>
    </div>
</div>
<div class="content-body"><!-- Basic Tables start -->
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Filter
                    </h4>
                        <small class="btn-group">
                            <form action="{{url()->current()}}" method="get" id="form">
                                <select name="filter" class="form-control">
                                    <option value="">Semua status pesanan</option>
                                    @foreach($status_detail as $s)
                                    <option value="{{$s->id}}" {{$status==$s->id ? 'selected' : ''}}>{{$s->description}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </small>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <div class="table-responsive">
                            <table class="table" id="users-table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>No Tagihan</th>
                                        <th>Pembeli</th>
                                        <th>Penjual</th>
                                        @if($status==null)
                                        <th>Status Pembayaran</th>
                                        @else
                                        <th>Barang yang dipesan</th>
                                        @endif
                                        <th>Waktu Pemesanan</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td><a href="{{url('admin/transaction/orders/'.$order->id)}}">{{$order->id}}</a></td>
                                        <td><a href="{{url('admin/transaction/invoices/'.$order->invoices->id.'#'.$order->id)}}">{{$order->invoices->invoice_number}}</a></td>
                                        <td>{{$order->shipments->buyer_address->user->user_name->name}}</td>
                                        <td>{{$order->order_item[0]->products->stores->users->user_name->name}}</td>
                                        @if($status==null)
                                        <td>{{$order->order_status->description}}</td>
                                        @else
                                        <td>{{$order->order_item->count()}} barang</td>
                                        @endif
                                        
                                        <td>{{$order->created_at->diffForHumans()}}</td>
                                        <td>
                                            <a class="btn btn-primary" href="{{url('admin/transaction/orders/'.$order->id)}}" data-toggle="tooltip" title="Detail"><i class="icon-eye6"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="text-xs-center">
                        {{$orders->appends(Request::except('page'))->links()}}
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <!-- Basic Tables end -->


</div>
@endsection

@section('jsfield')
<script type="text/javascript">
    $('select[name=filter]').on('change', function(){
        $('#form').submit();
    })
</script>
@endsection