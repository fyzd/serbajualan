@extends('admin.templates.t_index')
@section('title', 'Dashboard')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
        <h2 class="content-header-title">Detail Pesanan {{$o->id}}</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item"><a href="{{url('admin/transaction')}}">Transaksi</a>
            </li>
            <li class="breadcrumb-item"><a href="{{url('admin/transaction/iorders')}}">Pesanan</a>
            </li>
            <li class="breadcrumb-item">Detail Pesanan
            </li>
        </ol>
        </div>
    </div>
</div>
<div class="content-body"><!-- Basic Tables start -->
    <div class="row">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                    Detail Penjualan
                    </h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    
                    <div class="heading-elements">
                        
                        <ul class="list-inline mb-0">
                        
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <div class="row">
                            <div class="col-sm-3">
                                <p>Id Pesanan</p>
                                <label>{{$o->id}}</label>
                            </div>
                            <div class="col-sm-4">
                                <p>Penjual</p>
                                <label><a href="{{url('u/'.$o->order_item[0]->products->stores->users->username)}}">{{$o->order_item[0]->products->stores->users->user_name->name}}</a></label>
                            </div>
                            <div class="col-sm-5">
                                <p>Alamat Penjual</p>
                                <label>{{$o->shipments->store_address->pp}}</label><br>
                                {{$o->shipments->store_address->address}} <br>
                                Kecamatan {{$o->shipments->store_address->kecamatan->nama}}, {{ucwords(strtolower($o->shipments->store_address->kota->nama))}}<br>
                                {{$o->shipments->store_address->provinsi->nama}}, {{$o->shipments->store_address->zip_code}}<br>
                                {{$o->shipments->store_address->phone}}    
                            </div>
                        </div>
                        <hr>
                    @foreach($o->order_item as $order)
                        
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('assets/images/products/'.$order->products->user_id.'/'.$order->products->product_image_first->name)}}" width="100">
                            </div>
                            <div class="col-sm-6">
                                <p>{{$order->products->name}}</p>
                                <p>Jumlah: {{$order->quantity}}</p>
                                <p>Berat: {{$order->products->weight*$order->quantity}}gram</p>
                            </div>
                            <div class="col-sm-3">
                                <label>Rp{{number_format($order->price,0,',','.')}}</label>
                            </div>
                        </div>
                        <hr>
                            
                    @endforeach
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Status Pemesanan</p>
                                <label>{{$o->order_status->description}}</label>
                            </div>
                        </div>
                        <hr>
                        @if($o->order_status_id==2)
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Gagalkan Pesanan</p>
                                <form action="{{url('admin/transaction/invoices/reject')}}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('put')}}
                                    <div class="row">
                                        <div class="col-xs-10">
                                            <input type="hidden" name="order_id" value="{{$o->id}}">
                                            <input type="text" placeholder="Alasan pesanan digagalkan" name="reason" class="form-control">
                                        </div>
                                        <div class="col-xs-2">
                                            <input type="submit" class="btn btn-success" value="Submit">  
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </div><hr>
                        @endif
                        @if($o->order_status_id==7)
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Alasan Pesanan Digagalkan</p>
                                <label>{{$o->order_details}}</label>
                            </div>
                        </div><hr>
                        @endif
                        @if($o->order_status_id==4)
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Status Transfer Uang ke Penjual</p>
                                @if($o->selling_mutation!=null)
                                <label>Uang sudah ditransfer ke dompet virtual penjual sebesar Rp{{number_format($o->selling_mutation->mutation,0, ', ', '.')}}</label>
                                @else
                                
                                <form action="{{url('admin/transaction/orders/transfer')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="user_id" value="{{$o->order_item[0]->products->stores->users->id}}">
                                    <input type="hidden" name="order_id" value="{{$o->id}}">
                                    @php $total=0 @endphp
                                    @foreach($o->order_item as $order)
                                        @php
                                        $harga=0;
                                        $amount=$order->price;
                                        
                                        if($amount<$commission[0]->higherorequalto)
                                            $harga=$order->price;
                                        else{
                                            foreach($commission as $c){
                                                if($amount>=$c->higherorequalto)
                                                    $harga=$amount-$c->cost;
                                            }
                                        }
                                        
                                        $total+=$harga;
                                        @endphp
                                    @endforeach
                                    <p>Uang belum ditransfer, transfer uang ke dompet virtual penjual sebesar</p>
                                    <small class="btn-group">
                                        <input type="text" name="amount" class="form-control" value="{{$total+$o->shipments->cost}}" readonly>
                                    </small>
                                    <input type="hidden" value="{{$amount-$total}}" name="commission">
                                    <input type="submit" class="btn btn-success" value="Submit">
                                    <p>setelah dikurangi komisi penjualan sebesar Rp{{$amount-$total}}</p> 
                                </form>
                                
                                @endif
                            </div>
                        </div><hr>
                        @endif
                        @if($o->order_status_id==5)
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Alasan Pengembalian</p>
                                <label>{{$o->order_details}}</label>
                            </div>
                        </div><hr>
                        @endif
                        @if($o->order_status_id==5 || $o->order_status_id==7)
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Status Pengembalian Uang</p>
                                @if($o->refund_mutation!=null)
                                <label>Uang sudah dikembalikan ke dompet virtual pembeli sebesar Rp{{number_format($o->refund_mutation->mutation, 0, ', ', '.')}}</label>
                                @else
                                <form action="{{url('admin/transaction/orders/refund')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="user_id" value="{{$o->shipments->buyer_address->user->id}}">
                                    <input type="hidden" name="order_id" value="{{$o->id}}">
                                    @foreach($o->order_item as $order)
                                        @php
                                        $amount=0;
                                        $amount+=$order->price;
                                        @endphp
                                    @endforeach
                                    <p>Uang belum dikembalikan, kembalikan uang ke dompet virtual pembeli sebesar</p>
                                    <small class="btn-group">
                                        <input type="text" name="amount" class="form-control" value="{{$amount+$o->shipments->cost}}" readonly>
                                    </small>
                                    <input type="submit" class="btn btn-success" value="Submit">
                                </form>
                                @endif
                            </div>
                        </div><hr>
                        @endif
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Dikirim Tanggal</p>
                                <label>{{$o->shipments->updated_at != null ? $o->shipments->updated_at : 'Barang belum dikirim'}}</label>
                            </div>
                        </div><hr>
                        @if($o->order_status_id==3)
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Konfirmasi Penerimaan Barang (Jika pembeli tidak mengkonfirmasi penerimaan)</p>
                                <small class="btn-group"><a style="cursor:pointer" class="form-control btn btn-warning pull-right" id="{{$o->id}}" data-toggle="modal" data-target="#konfir-diterima" onclick="getOrderId({{$o->id}})">Konfirmasi Barang Diterima</a></small>
                            </div>
                        </div><hr>
                        @endif
                        <div class="row">
                            <div class="col-sm-4">
                                <p>Jasa Pengiriman</p>
                                <label>{{$o->shipments->service}}</label>
                            </div>
                            <div class="col-sm-4">
                                <p>No. Resi</p>
                                <label>{{$o->shipments->tracking_number!=null ? $o->shipments->tracking_number : 'Pengiriman barang belum dikonfirmasi'}}</label>
                            </div>
                            <div class="col-sm-4">
                                <p>Biaya</p>
                                <label>Rp{{number_format($o->shipments->cost,0,',','.')}}</label>
                            </div>
                        </div>
                        
                    </div>
                
                </div>
            </div>
        
        <!-- Basic Tables end -->
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                    Detail Pembeli
                    </h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    
                    <div class="heading-elements">
                        
                        <ul class="list-inline mb-0">
                        
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <p>No. Tagihan</p>
                        <label><a href="{{url('admin/transaction/invoices/'.$o->invoice_id)}}">{{$o->invoices->invoice_number}}</a></label><hr>
                        <p>Pembeli</p>
                        <label><a href="{{url('u/'.$o->shipments->buyer_address->user->username)}}">{{$o->shipments->buyer_address->user->user_name->name}}</a></label><hr>
                        <p>Dibeli Tanggal</p>
                        <label>{{$o->invoices->created_at}}</label><hr>
                        <p>Status Tagihan</p>
                        <label>{{$o->invoices->invoice_status->description}}</label><hr>
                        @if($o->invoices->invoice_status_id==1 || $o->invoices->invoice_status_id==2)
                        <label>Dipesan {{$o->invoices->created_at->diffForHumans()}}</label>
                        <hr>
                        @endif
                        <p>Rincian Pembayaran</p>
                        <label>Rp{{number_format($o->invoices->amount,0,',','.')}}</label><br>
                        <hr>
                        <p>Alamat Pengiriman</p>
                        <label>{{$o->shipments->buyer_address->pp}}</label><br>
                        {{$o->shipments->buyer_address->address}} <br>
                        Kecamatan {{$o->shipments->buyer_address->kecamatan->nama}}, {{ucwords(strtolower($o->shipments->buyer_address->kota->nama))}}<br>
                        {{$o->shipments->buyer_address->provinsi->nama}}, {{$o->shipments->buyer_address->zip_code}}<br>
                        {{$o->shipments->buyer_address->phone}}  
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="konfir-diterima" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Konfirmasi Barang Diterima</h2>
                    </div>
                    <div class="modal-body">
                    <div class="alert alert-success" role="alert">
                        Anda yakin?
                    </div>

                    </div>
                    <div class="modal-footer">
                        <form action="{{url('admin/transaction/invoices/receive')}}" method="post" id="received">
                            {{csrf_field()}}
                            <input type="hidden" name="order_id" id="orderid">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                            <input type="submit" value="Konfirmasi" class="btn btn-success" id="confirm">
                        </form>

                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsfield')
<script type="text/javascript">

    function getOrderId(id)
    {
        $('#orderid').val(id);
    }
</script>
@endsection