@extends('admin.templates.t_index')
@section('title', 'Dashboard')
@section('cssfield')
@endsection

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
        <h2 class="content-header-title">
            Pendapatan
        </h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item"><a href="{{url('admin/transaction')}}">Pendapatan</a>
            </li>
        </ol>
        </div>
    </div>
</div>
<div class="content-body"><!-- Basic Tables start -->
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Total Rp{{number_format($profits->sum('amount'), 0, ',', '.')}}
                    </h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <div class="table-responsive">
                            <table class="table" id="users-table">
                                <thead>
                                    <tr>
                                        <th>Order Id</th>
                                        <th>Pemasukan</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($profits as $profit)
                                    <tr>
                                        <td>{{$profit->order_id}}</td>
                                        <td>Rp{{number_format($profit->amount, 0, ',', '.')}}</td>
                                        
                                        <td>{{$profit->created_at}}</td>
                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="text-xs-center">
                        {{$profits->links()}}
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <!-- Basic Tables end -->


</div>
@endsection

@section('jsfield')
@endsection