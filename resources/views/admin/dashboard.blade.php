@extends('admin.templates.t_index')
@section('title', 'Dashboard')
@section('cssfield')
@endsection

@section('content')
<!--
Total Saldo Dompet User
Total Saldo Serbajualan dari user yang sudah bayar, yaitu pesanan yang diproses, dikirim, diterima tp belum dipindahkan ke dompet user
Total Saldo Profit Serbajualan dari komisi penjualan
Total Transaksi berhasil
-->

<div class="content-body">
    <div class="row">
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <div class="card bg-cyan">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="icon-pencil white font-large-2 float-xs-left"></i>
                            </div>
                            <div class="media-body white text-xs-right">
                                <h3>{{$maincard1}}</h3>
                                <span>Total tagihan hari ini</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <div class="card bg-deep-orange">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="icon-chat1 white font-large-2 float-xs-left"></i>
                            </div>
                            <div class="media-body white text-xs-right">
                                <h3>{{$maincard2}}</h3>
                                <span>Total pesanan hari ini</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <div class="card bg-teal">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="icon-trending_up white font-large-2 float-xs-left"></i>
                            </div>
                            <div class="media-body white text-xs-right">
                                <h3>{{$maincard3}}</h3>
                                <span>Total tagihan</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <div class="card bg-pink">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="icon-map1 white font-large-2 float-xs-left"></i>
                            </div>
                            <div class="media-body white text-xs-right">
                                <h3>{{$maincard4}}</h3>
                                <span>Total pesanan</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <a href="{{url('admin/transaction/invoices?filter=1')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="cyan">{{$card1}}</h3>
                                    <span>Tagihan belum memiliki metode pembayaran</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-pencil cyan font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <a href="{{url('admin/transaction/invoices?filter=2')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="deep-orange">{{$card2}}</h3>
                                    <span>Tagihan menunggu pembayaran</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-chat1 deep-orange font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-xl-3 col-lg-6 col-xs-12">
            <a href="{{url('admin/transaction/orders?filter=2')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="teal">{{$card3}}</h3>
                                    <span>Pesanan diproses penjual</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-trending_up teal font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <a href="{{url('admin/transaction/orders?filter=3')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="pink">{{$card4}}</h3>
                                    <span>Pesanan dalam perjalanan</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-map1 pink font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <a href="{{url('admin/transaction/invoices?filter=4')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="pink">{{$card5}}</h3>
                                    <span>Tagihan kedaluarsa</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-bag2 pink font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <a href="{{url('admin/transaction/invoices?filter=5')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="teal">{{$card6}}</h3>
                                    <span>Tagihan gagal</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-user1 teal font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-xl-3 col-lg-6 col-xs-12">
            <a href="{{url('admin/transaction/orders?filter=5')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="deep-orange">{{$card7}}</h3>
                                    <span>Pesanan dikembalikan</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-diagram deep-orange font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <a href="{{url('admin/transaction/orders?filter=4')}}">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="cyan">{{$card8}}</h3>
                                    <span>Pesanan diterima</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-ios-help-outline cyan font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- Basic Tables end -->


</div>
@endsection

@section('jsfield')

@endsection