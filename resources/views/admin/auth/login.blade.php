<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<head>
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/bootstrap.css')}}">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/fonts/icomoon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/fonts/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/vendors/css/extensions/pace.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/vendors/css/ui/prism.min.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/colors.css')}}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets-admin/css/core/menu/menu-types/vertical-overlay-menu.css')}}">

    <!-- END Page Level CSS-->
</head>

<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
        <div class="card border-grey border-lighten-3 m-0">
            <div class="card-header no-border">
                <div class="card-title text-xs-center">
                    <div class="p-1"><img src="{{url('assets-admin/images/logo/sj.png')}}" alt="branding logo" height="100"></div>
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Login Admin</span></h6>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body collapse in">
                <div class="card-block">
                    <form class="form-horizontal form-simple" action="{{url('admin/login')}}" method="post" novalidate>
                        {{csrf_field()}}
                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="text" class="form-control form-control-lg input-lg" id="user-name" placeholder="E-mail / Username" name="emailusername" required>
                            <div class="form-control-position">
                                <i class="icon-head"></i>
                            </div>
                        </fieldset>
                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="password" class="form-control form-control-lg input-lg" id="user-password" placeholder="Password" name="password" required>
                            <div class="form-control-position">
                                <i class="icon-key3"></i>
                            </div>
                        </fieldset>
                        <fieldset class="form-group row">
                            <div class="col-md-6 col-xs-12 text-xs-center text-md-left">
                                <fieldset>
                                    <label for="remember-me"> 
                                        <input type="checkbox" id="remember-me" name="remember" class="chk-remember"> Ingat saya
                                        </label>
                                </fieldset>
                            </div>
                        </fieldset>
                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-unlock2"></i> Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <!-- BEGIN VENDOR JS-->
    <script src="{{URL::asset('assets-admin/js/core/libraries/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/tether.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/js/core/libraries/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/unison.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/blockUI.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/jquery.matchHeight-min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/ui/screenfull.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/vendors/js/extensions/pace.min.js')}}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{URL::asset('assets-admin/js/core/app-menu.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets-admin/js/core/app.js')}}" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->

    </body>
</html>