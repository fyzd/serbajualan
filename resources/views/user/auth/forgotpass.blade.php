@extends('user.templates.t_index')
@section('title', 'Reset Password')
@section('content')
<section id="form" style="margin-top:0;padding-bottom:30px"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="login-form"><!--login form-->
                    <h4 style="margin-bottom:30px" class="text-center">Masukkan email</h4>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{url('email')}}" method="post">
                        
                        {{csrf_field()}}
                        <input type="email" class="form-group form-control" name="email" placeholder="Email" value="{{old('email')}}"/>
                        <button type="submit" class="btn btn-success form-control">Kirim</button>
                        
                    </form>
                </div><!--/login form-->
            </div>
            
        </div>
    </div>
</section><!--/form-->
@endsection