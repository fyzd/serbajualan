@extends('user.templates.t_index')
@section('title','Register')
@section('content')
<section id="form" style="margin-top:0; padding-bottom:30px"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="signup-form"><!--sign up form-->
                    <h4 style="margin-bottom:30px" class="text-center">Daftar akun baru sekarang</h4>
                    @if($status=Session::get('status'))
                    <div class="alert alert-info">
                        {{$status}}
                    </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{url('postregister')}}" method="post">
                        {{csrf_field()}}
                        <input type="text" name="name" class="form-group form-control" placeholder="Nama Lengkap" value="{{old('name')}}"/>
                        <input type="email" name="email" class="form-group form-control" placeholder="E-mail" value="{{old('email')}}"/>
                        <div class="form-group">
                            <label class="radio-inline">
                                <input type="radio" name="sex" value="L">Laki-Laki
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="sex" value="P">Perempuan
                            </label>
                        </div>
                        <div class="input-group form-group">
                            <span class="input-group-addon" id="basic-addon3">Serbajualan.com/</span>
                            <input type="text" name="username" class=" form-control"placeholder="Username" value="{{old('username')}}"/>
                        </div>
                        <input type="password" name="password" class="form-group form-control" placeholder="Password"/>
                        <input type="password" name="password_confirmation" class="form-group form-control" placeholder="Konfirmasi Password"/>
                        <p>Dengan klik daftar, Anda telah menyetujui Aturan Penggunaan dan Kebijakan Privasi dari Serbajualan</p>
                        <button type="submit" class="btn btn-success form-control">Daftar</button>
                        
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section>

@endsection