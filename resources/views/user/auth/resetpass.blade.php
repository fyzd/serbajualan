@extends('user.templates.t_index')
@section('title', 'Reset Password')
@section('content')
<section id="form" style="margin-top:0;padding-bottom:30px"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="login-form"><!--login form-->
                    <h4 style="margin-bottom:30px" class="text-center">Reset Password</h4>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{url('reset')}}" method="post">
                        
                        {{csrf_field()}}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="email" class="form-group form-control" name="email" placeholder="Email"/>
                        <input type="password" class="form-group form-control" name="password" placeholder="Password"/>
                        <input type="password" class="form-group form-control" name="password_confirmation" placeholder="Confirm Password"/>
                        <button type="submit" class="btn btn-success form-control">Reset Password</button>
                        
                    </form>
                </div><!--/login form-->
            </div>
            
        </div>
    </div>
</section><!--/form-->
@endsection