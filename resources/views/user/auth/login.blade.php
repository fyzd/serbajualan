@extends('user.templates.t_index')
@section('title', 'Login')
@section('content')

<section id="form" style="margin-top:0; padding-bottom:30px"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="login-form"><!--login form-->
                    <h4 style="margin-bottom:30px"class="text-center">Silakan masuk ke dalam akun Anda</h4>
                    @if($status=Session::get('status'))
                    <div class="alert alert-info">
                        {{$status}}
                    </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{url('postlogin')}}" method="post">
                        {{csrf_field()}}
                        <input type="text" class="form-group form-control" name="emailusername" placeholder="Email / Username" value="{{old('emailusername')}}"/>
                        <input type="password" class="form-group form-control" name="password" placeholder="Password">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            Ingat saya
                            </label>
                            <span class="pull-right">
                                <a class="link" href="{{url('reset')}}">Lupa password?</a>
                            </span>
                        </div>
                        
                        <button type="submit" class="btn btn-success form-control">Login</button>
                        
                    </form>
                </div><!--/login form-->
            </div>
            
        </div>
    </div>
</section><!--/form-->

@endsection