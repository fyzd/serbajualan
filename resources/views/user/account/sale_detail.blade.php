@extends('user.templates.t_index')
@section('title', 'Serbajualan')
@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('sale')}}">Transaksi Penjualan</a></li>
                                    <li class="active"><a>Detail Penjualan</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div class="col-sm-12">
                    <div class="row">
                        <h4>Detail Penjualan</h4>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-7">
                    
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Daftar Penjualan</h3>
                            </div>
                            <div class="panel-body">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if($orders->order_status_id==2)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Konfirmasi Pengiriman</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="alert alert-danger">Jika barang dibawah ini telah Anda kirim, harap masukkan no. resi untuk mengkonfirmasi pengiriman.</div>
                                    
                                        <form action="{{url('sale/update_tr')}}" method="post">
                                            {{csrf_field()}}
                                            {{method_field('put')}}
                                            <input type="hidden" name="order_id" value="{{$orders->id}}">
                                            <input type="text" class="form-control form-group" placeholder="Masukkan no. resi" name="resi">
                                            <input type="submit" value="Kirim" class="form-control btn btn-success">
                                        </form>
                                    </div>
                                </div>
                                
                            @endif
                            @if($orders->order_status_id==3)
                                <div class="alert alert-info">
                                    Pengiriman barang telah dikonfirmasi
                                </div>
                            @endif
                            @if($orders->order_status_id==4)
                                
                                <div class="alert alert-success">
                                    Barang telah sampai di tujuan dan diterima oleh pembeli
                                </div>
                            @endif
                                
                                <div class="panel panel-default" id="{{$orders->id}}">
                                    <div class="panel-body">
                                    
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>No. Transaksi</p>
                                                <label>{{$orders->id}}</label>
                                            </div>
                                        </div>
                                        <hr>
                                        @foreach($orders->order_item as $order)
                                        <div class="row">
                                            <div class="col-sm-2 col-xs-2">
                                                <a href="{{url('p/'.$order->products->product_category->slug.'/'.$order->products->product_subcategory->slug.'/'.$order->products->id)}}"><img src="{{url('assets/images/products/'.$order->products->user_id.'/'.$order->products->product_image_first->name)}}" height="50" width="50"></a>
                                            </div>
                                            <div class="col-sm-7 col-xs-10">
                                                <label class="pcart"><a href="{{url('p/'.$order->products->product_category->slug.'/'.$order->products->product_subcategory->slug.'/'.$order->products->id)}}">{{$order->products->name}}</a></label>
                                                <p>Jumlah: {{$order->quantity}}</p>
                                                <p>Berat: {{$order->products->weight*$order->quantity}}gram</p>
                                            </div>
                                            <div class="col-sm-3 col-xs-12">
                                                <label>Rp{{number_format($order->price,0,',','.')}}</label>
                                            </div>
                                        </div>
                                        @endforeach
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Status Penjualan</p>
                                                <ul class="steps">
                                                    @if($orders->order_status_id==2 || $orders->order_status_id==3 || $orders->order_status_id==4)
                                                    <li style="width:auto;line-height:1.8"><div class="sprite icon-processed" style="margin:5px 10px"><span style="padding-left:30px">Diproses</span></div></li>
                                                    @endif
                                                    @if($orders->order_status_id==3 || $orders->order_status_id==4)
                                                    <li style="width:auto;line-height:1.8"><div class="sprite icon-sent" style="margin:5px 10px"><span style="padding-left:30px">Dikirim</span></div></li>
                                                    @endif
                                                    @if($orders->order_status_id==4)
                                                    <li style="width:auto;line-height:1.8"><div class="sprite icon-received" style="margin:5px 10px"><span style="padding-left:30px">{{$orders->order_status->description}}</span></div></li>
                                                    @endif
                                                    @if($orders->order_status->id==5)
                                                    <li style="width:auto;line-height:1.8"><div class="sprite icon-refund" style="margin:5px 10px"><span style="padding-left:30px">{{$orders->order_status->description}}</span></div></li>
                                                    @endif
                                                    @if($orders->order_status_id==6 || $orders->order_status_id==7)
                                                    {{$orders->order_status->description}}
                                                    @endif
                                                </ul>  
                                            </div>
                                        </div>
                                        <hr>
                                        @if($orders->order_status_id==4)
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Status Transfer</p>
                                                @if($orders->selling_mutation!=null)
                                                <p>Uang hasil penjualan sudah masuk ke dompet virtual sebesar</p><p><b>Rp{{number_format($orders->selling_mutation->mutation,0,',','.')}}</b> *)</p>
                                                @else
                                                <p>Uang hasil penjualan belum masuk ke dompet virtual, harap menunggu.</p>
                                                @endif
                                            </div>
                                        </div><hr>
                                        @endif
                                        @if($orders->order_status_id==5)
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Alasan Pengembalian</p>
                                                <p>{{$orders->order_details}}</p>
                                            </div>
                                        </div><hr>
                                        @endif
                                        @if($orders->order_status_id==7)
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Alasan Digagalkan</p>
                                                <p>{{$orders->order_details}}</p>
                                            </div>
                                        </div><hr>
                                        @endif
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Dikirim Tanggal</p>
                                                <label>{{$orders->shipments->updated_at != null ? $orders->shipments->updated_at : 'Barang belum dikirim'}}</label><hr> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-6">
                                                <p>Jasa Pengiriman</p>
                                                <label>{{$orders->shipments->service}}</label>
                                            </div>
                                            <div class="col-sm-4 col-xs-6">
                                                <p>No. Resi @if($orders->order_status_id==3)<a style="cursor:pointer" class="pull-right" id="{{$orders->id}}" data-toggle="modal" data-target="#edit-resi" onclick="setOrderId(this);return false;">Edit</a>@endif </p>
                                                <label>{{$orders->shipments->tracking_number!=null ? $orders->shipments->tracking_number : 'Pengiriman barang belum dikonfirmasi'}}</label>
                                                
                                            </div>
                                            <div class="col-sm-4 col-xs-6">
                                                <p>Biaya</p>
                                                <label>Rp{{number_format($orders->shipments->cost,0,',','.')}}</label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                @if($orders->order_status_id==4)
                                <p>*) Setelah dikurangi komisi penjualan. <a href="" class="link">Apa itu komisi penjualan?
                                @endif</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5">
                    @if($orders->order_status_id==2)
                    <div class="panel panel-default">
                        <div class="panel-body">
                        <button class="btn btn-danger form-control" data-toggle="modal" data-target="#tolak-pesanan">Tolak Pesanan</button>
                        </div>
                    </div>
                    @endif
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>Pembeli</p>
                                <label><a href="{{url('u/'.$orders->shipments->buyer_address->user->username)}}">{{$orders->shipments->buyer_address->user->user_name->name}}</a></label><hr>
                                <p>Dibeli Tanggal</p>
                                <label>{{$orders->created_at}}</label><hr>
                                <p>Status Penjualan</p>
                                <label>{{$orders->order_status->description}}</label><hr>
                                <p>Rincian Pembayaran</p>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        Harga Barang
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <p class="pull-right">Rp{{number_format($barang,0,',','.')}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        Biaya Pengiriman
                                    </div>
                                    <div class="col-sm-6 pull-right">
                                        <p class="pull-right">Rp{{number_format($kirim,0,',','.')}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label>Total Pembayaran</label>
                                    </div>
                                    <div class="col-sm-6 col-xs-6 pull-right">
                                        <label class="pull-right">Rp{{number_format($total,0,',','.')}}</label>
                                    </div>
                                </div><hr>
                                <p>Alamat Pengiriman</p>
                                <label>{{$orders->shipments->buyer_address->pp}}</label><br>
                                {{$orders->shipments->buyer_address->address}} <br>
                                Kecamatan {{$orders->shipments->buyer_address->kecamatan->nama}}, {{ucwords(strtolower($orders->shipments->buyer_address->kota->nama))}}<br>
                                {{$orders->shipments->buyer_address->provinsi->nama}}, {{$orders->shipments->buyer_address->zip_code}}<br>
                                {{$orders->shipments->buyer_address->phone}}    
                            </div>
                        </div>
                    </div>
                </div>
                
                

                
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="edit-resi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Edit No. Resi</h2>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning" role="alert">
                        Edit resi hanya jika Anda salah memasukkan resi sebelumnya.
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <form action="{{url('sale/update_tr')}}" method="post">
                            {{csrf_field()}}
                            {{method_field('put')}}
                            <input type="text" class="form-control form-group" placeholder="Masukkan no. resi" name="resi">
                            
                </div>
                <div class="modal-footer">
                    
                            <input type="hidden" id="oidbaru" name="order_id">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                            <input type="submit" value="Kirim" class="btn btn-success">
                            </form>
                        </form>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tolak-pesanan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Tolak Pesanan</h2>
                </div>
                <div class="modal-body">
                    <p>Alasan penolakan pesanan</p>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <form action="{{url('sale/reject_order')}}" method="post">
                            {{csrf_field()}}
                            {{method_field('put')}}

                            <textarea class="form-control" cols="30" rows="5" placeholder="Alasan" name="reason"></textarea>
                            
                </div>
                <div class="modal-footer">
                    
                            <input type="hidden" name="order_id" value="{{$orders->id}}">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                            <input type="submit" value="Tolak Pesanan" class="btn btn-danger">
                            </form>
                        </form>
                    
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('jsfield')
<script type="text/javascript">
    function setOrderId(idnya)
    {
        var id;
        id=$(idnya).attr('id');
        $('#oidbaru').val(id);
    }
</script>
@endsection