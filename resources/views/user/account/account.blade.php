@extends('user.templates.t_index')
@section('title', 'Serbajualan')
@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li class="active"><a>Panel Akun</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div class="row">
                    <div class="col-sm-6 acc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left" style="padding-top: 8px;">Informasi Pribadi</h3>
                                <a class="btn btn-default pull-right" href="{{url('users/edit?section=general')}}">Ubah</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <div class="iframe-img" style="margin-bottom:10px">
                                    <img src="{{Auth::user()->user_info->image != null ? url('assets/images/profile').'/'.Auth::user()->user_info->image : url('assets/images/pp-default.png')}}" alt="" width="34" height="34" style="margin-right:10px;border-radius:50%">
                                    {{ Auth::user()->user_name->name }}
                                </div>
                                <p> {{ Auth::user()->email }} {!!Auth::user()->confirmed==1 ? '<i class="fa fa-check"></i>' : ''!!}</p>
                                <a class="link" href="{{url('users/edit?section=email')}}">UBAH EMAIL</a> <br>
                                <a class="link" href="{{url('users/edit?section=password')}}">UBAH PASSWORD</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 acc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left" style="padding-top: 8px;">Alamat Pengiriman Utama</h3>
                                <a class="btn btn-default pull-right" href="{{url('user_addresses')}}">Ubah</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                @if($address!=null)
                                <p>
                                <label>{{$address->pp}}</label><br>
                                <span>{{$address->address}}</span><br>
                                <span>Kec. {{$address->kecamatan->nama}}, 
                                {{ucwords(strtolower(str_replace(strpos($address->kota->nama, 'KOTA ') === false ? 'KAB. ' : 'KOTA. ','',$address->kota->nama)))}}</span><br>
                                <span>{{$address->provinsi->nama.', '.$address->zip_code}}</span><br>
                                <span>Telepon/Handphone: {{$address->phone}}</span>
                                </p>
                                @else
                                <p>Alamat belum ditambahkan</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            <h3 class="panel-title pull-left" style="padding-top: 8px;">Pembelian Terbaru</h3>
                            <a class="btn btn-default pull-right" href="{{url('payment/invoices')}}">Selengkapnya</a>
                            <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="trx">
                                            <tr>
                                                <th>No. Tagihan</th>
                                                <th>Total Pembayaran</th>
                                                <th style="width:250px">Status Tagihan</th>
                                                <th style="width:250px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($invoices as $invoice)
                                            <tr>
                                                <td><a class="link" href="{{url('payment/invoices/'.$invoice->invoice_number)}}">{{$invoice->invoice_number}}</a></td>
                                                <td>Rp{{number_format($invoice->amount, 0,',','.')}}</td>
                                                <td><span style="margin-right:10px" class="label label-@if($invoice->invoice_status_id==1 || $invoice->invoice_status_id==2)warning  @elseif($invoice->invoice_status_id==3)success @elseif($invoice->invoice_status_id==4 || $invoice->invoice_status_id==5 || $invoice->invoice_status_id==6)danger @endif">{{$invoice->invoice_status->description}}</span> ({{$invoice->updated_at}})
                                                {!!$invoice->invoice_status_id==1 ? '<br> <a class="link" href="'.url('payment/purchases/'.$invoice->invoice_number).'">Pilih Metode Bayar</a>' : '' !!}</td>
                                                <td>
                                                    <a href="{{url('payment/invoices/'.$invoice->invoice_number)}}" class="btn btn-success pull-right">Lihat Detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left" style="padding-top: 8px;">Penjualan Terbaru</h3>
                                <a class="btn btn-default pull-right" href="{{url('sale')}}">Selengkapnya</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="trx">
                                            <tr>
                                                <th>No. Transaksi</th>
                                                <th>Status Barang</th>
                                                <th style="width:250px">Dibeli oleh pelanggan pada</th>
                                                <th style="width:250px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orders as $order)
                                            <tr>
                                                <td>{{$order->id}}</td>
                                                <td><span class="label label-@if($order->order_status_id==2)warning @elseif($order->order_status_id==3)primary @elseif($order->order_status_id==4)success @elseif($order->order_status_id==5 || $order->order_status_id==6 || $order->order_status_id==7)danger @endif">{{$order->order_status->description}}</span></td>
                                                <td>{{$order->created_at}}</td>
                                                <td class="text-center"><a href="{{url('sale/'.$order->id)}}" class="btn btn-success pull-right">Lihat Detail</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection