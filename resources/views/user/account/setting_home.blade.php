@extends('user.templates.t_index')
@section('title', 'Checkout')

@section('content')

<div class="setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('account_settings')}}">Pengaturan</a></li>
                                    <li class="active"><a>Pengaturan Akun</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>

                @include('user.templates.t_user_setting_nav')
                
                <div class="setting-container">
                    <div class="setting-head">
                        <h4>Profil Akun</h4>
                        <span>Data Anda selalu rahasia dan tidak akan kami beritahukan kepada pihak ketiga.</span>
                    </div>
                    <div class="col-sm-12 setting-head2">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <h3>Informasi Umum</h3>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <a href="{{url('users/edit?section=general')}}" class="btn btn-success pull-right">Edit</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">Foto Profil</label>
                                <img src="{{$data->image!=null ? url('assets/images/profile').'/'.$data->image : url('assets/images/pp-default.png')}}" height="52" width="52">
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">Username</label>
                                <span class="col-sm-10 col-xs-9 row">{{Auth::user()->username}}</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">Nama</label>
                                <span class="col-sm-10 col-xs-9 row">{{$data->name}}</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">Tanggal Lahir</label>
                                <span class="col-sm-10 col-xs-9 row">{{$data->birth!=null ? date('d F Y', strtotime($data->birth)) : ''}}</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">Jenis Kelamin</label>
                                <span class="col-sm-10 col-xs-9 row">{{$data->sex=='L' ? 'Laki-laki' : 'Perempuan'}}</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">Telepon</label>
                                <span class="col-sm-10 col-xs-9 row">{{$data->phone}}</span>
                            </div>
                        </form>
                    </div>

                    <div class="col-sm-12 setting-head2">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <h3>E-mail</h3>
                            </div>
                            <div class="col-sm-6 col-xs-6 ">
                                <a href="{{url('users/edit?section=email')}}" class="btn btn-success pull-right">Edit</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">E-mail</label>
                                <span class="col-sm-10 col-xs-9 row">{{Auth::user()->email}} {!!Auth::user()->confirmed==1 ? '<i class="fa fa-check"></i>' : ''!!}</span>
                            </div>
                        </form>
                    </div>
                    
                    <div class="col-sm-12 setting-head2">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <h3>Password</h3>
                            </div>
                            <div class="col-sm-6 col-xs-6 ">
                                <a href="{{url('users/edit?section=password')}}" class="btn btn-success pull-right">Edit</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">Password</label>
                                <span class="col-sm-10 col-xs-9 row"><i>Perubahan terakhir {{Auth::user()->updated_at->diffForHumans()}}</i></span>
                            </div>
                        </form>
                    </div>
                    <div style="clear:both"></div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection