@extends('user.templates.t_index')
@section('title', 'Serbajualan')
@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('payment/invoices')}}">Transaksi Pembelian</a></li>
                                    <li class="active"><a>Detail Pembelian</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div class="col-sm-12">
                    <div class="row">
                        <h4>Detail Pembelian</h4>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-7">
                    @if($invoice->invoice_status_id==1)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Petunjuk Pembayaran</h3>
                            </div>
                            <div class="panel-body">
                                <p>Anda belum memilih metode pembayaran. Tekan tombol Pilih Metode Bayar untuk melanjutkan pembayaran.</p>
                                <a href="{{url('payment/purchases/'.$invoice->invoice_number)}}" class="btn btn-primary form-control">Pilih Metode Bayar</a>
                            </div>
                        </div>
                    @endif
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Daftar Pembelian</h3>
                            </div>
                            <div class="panel-body">
                            @foreach($invoice->order as $o)
                            
                            
                                <div class="panel panel-default" id="{{$o->id}}">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <p>No. Transaksi</p>
                                                <label>{{$o->id}}</label>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <p>Penjual</p>
                                                <label><a href="{{url('u/'.$o->order_item[0]->products->stores->users->username)}}">{{$o->order_item[0]->products->stores->users->user_name->name}}</a></label>
                                            </div>
                                        </div>
                                        <hr>
                                        @foreach($o->order_item as $order)
                                        <div class="row">
                                            <div class="col-sm-2 col-xs-2">
                                                <a href="{{url('p/'.$order->products->product_category->slug.'/'.$order->products->product_subcategory->slug.'/'.$order->products->id)}}"><img src="{{url('assets/images/products/'.$order->products->user_id.'/'.$order->products->product_image_first->name)}}" height="50" width="50"></a>
                                            </div>
                                            <div class="col-sm-7 col-xs-10">
                                                <label class="pcart"><a href="{{url('p/'.$order->products->product_category->slug.'/'.$order->products->product_subcategory->slug.'/'.$order->products->id)}}">{{$order->products->name}}</a></label>
                                                <p>Jumlah: {{$order->quantity}}</p>
                                                <p>Berat: {{$order->products->weight*$order->quantity}}gram</p>
                                            </div>
                                            <div class="col-sm-3 col-xs-12">
                                                <label>Rp{{number_format($order->price,0,',','.')}}</label>
                                            </div>
                                        </div>
                                        @endforeach 
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Status Pembelian</p>
                                                <ul class="steps">
                                                    @if($o->order_status->id==2 || $o->order_status->id==3 || $o->order_status->id==4)
                                                    <li style="width:auto;line-height:1.8"><div class="sprite icon-processed" style="margin:5px 10px"><span style="padding-left:30px">Diproses</span></div></li>
                                                    @endif
                                                    @if($o->order_status->id==3 || $o->order_status->id==4)
                                                    <li style="width:auto;line-height:1.8"><div class="sprite icon-sent" style="margin:5px 10px"><span style="padding-left:30px">Dikirim</span></div></li>
                                                    @endif
                                                    @if($o->order_status->id==4)
                                                    <li style="width:auto;line-height:1.8"><div class="sprite icon-received" style="margin:5px 10px"><span style="padding-left:30px">{{$o->order_status->description}}</span></div></li>
                                                    @endif
                                                    @if($o->order_status->id==5)
                                                    <li style="width:auto;line-height:1.8"><div class="sprite icon-refund" style="margin:5px 10px"><span style="padding-left:30px">{{$o->order_status->description}}</span></div></li>
                                                    @endif
                                                    @if($o->order_status->id==1 || $o->order_status->id==6 || $o->order_status->id==7)
                                                    {{$o->order_status->description}}
                                                    @endif
                                                </ul>  
                                                
                                            </div>
                                        </div>
                                        @if($o->order_status_id==5)
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Alasan Pengembalian</p>
                                                <p>{{$o->order_details}}</p>
                                            </div>
                                        </div>
                                        @endif
                                        @if($o->order_status_id==7)
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Alasan Digagalkan</p>
                                                <p>{{$o->order_details}}</p>
                                            </div>
                                        </div>
                                        @endif
                                        @if($o->order_status_id==5 || $o->order_status_id==7)
                                        <hr>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <p>Status Pengembalian Uang</p>
                                                    @if($o->refund_mutation!=null)
                                                    <p>Uang sudah dikembalikan ke dompet virtual Anda sebesar Rp{{number_format($o->refund_mutation->mutation, 0, ', ', '.')}}</p>
                                                    @else
                                                        @foreach($o->order_item as $order)
                                                            @php
                                                            $amount=0;
                                                            $amount+=$order->price;
                                                            @endphp
                                                        @endforeach
                                                        <p>Pengembalian uang sebesar Rp{{number_format($amount+$o->shipments->cost, 0, ', ', '.')}} akan segera diproses</p>
                                                    @endif
                                                </div>
                                            </div>
                                            @endif
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>Dikirim Tanggal</p>
                                                <label>{{$o->shipments->updated_at != null ? $o->shipments->updated_at : 'Barang belum dikirim'}}</label><hr> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-6">
                                                <p>Jasa Pengiriman</p>
                                                <label>{{$o->shipments->service}}</label>
                                            </div>
                                            <div class="col-sm-4 col-xs-6">
                                                <p>No.Resi</p>
                                                <label>{{$o->shipments->tracking_number!=null ? $o->shipments->tracking_number : 'Pengiriman barang belum dikonfirmasi'}}</label>
                                            </div>
                                            <div class="col-sm-4 col-xs-12">
                                                <p>Biaya</p>
                                                <label>Rp{{number_format($o->shipments->cost,0,',','.')}}</label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            
                            @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>No. Tagihan</p>
                                <label>{{$invoice->invoice_number}}</label><hr>
                                <p>Status Tagihan</p>
                                <label>{{$invoice->invoice_status->description}}</label><hr>
                                <p>Rincian Pembayaran</p>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        Harga Barang
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="pull-right">Rp{{number_format($barang,0,',','.')}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        Biaya Pengiriman
                                    </div>
                                    <div class="col-sm-6 pull-right">
                                        <p class="pull-right">Rp{{number_format($kirim,0,',','.')}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label>Total Pembayaran</label>
                                    </div>
                                    <div class="col-sm-6 col-xs-6 pull-right">
                                        <label class="pull-right">Rp{{number_format($total,0,',','.')}}</label>
                                    </div>
                                </div><hr>
                                <p>Alamat Pengiriman</p>
                                <label>{{$invoice->order[0]->shipments->buyer_address->pp}}</label><br>
                                {{$invoice->order[0]->shipments->buyer_address->address}} <br>
                                Kecamatan {{$invoice->order[0]->shipments->buyer_address->kecamatan->nama}}, {{ucwords(strtolower($invoice->order[0]->shipments->buyer_address->kota->nama))}}<br>
                                {{$invoice->order[0]->shipments->buyer_address->provinsi->nama}}, {{$invoice->order[0]->shipments->buyer_address->zip_code}}<br>
                                {{$invoice->order[0]->shipments->buyer_address->phone}}    
                            </div>
                        </div>
                    </div>
                </div>
                
                

                
            </div>
        </div>
    </div>
</div>

@endsection