@extends('user.templates.t_index')
@section('title', 'Serbajualan')
@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li class="active"><a>Dompet Virtual</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Dompet Virtual</h4>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <p>Total Saldo</p>
                                    <h3>Rp{{$wallet!=null ? number_format($wallet->balance,0,',','.') : '0'}}</h3>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Mutasi</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-left">Waktu</th>
                                                <th class="text-right">Mutasi</th>
                                                <th class="text-right">Saldo</th>
                                                <th class="text-left">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if($wallet==null)

                                        @else
                                        @foreach($wallet->wallet_mutations as $mutation)
                                            <tr>
                                                <td class="text-left">{{$mutation->created_at}}</td>
                                                <td class="text-right">{{$mutation->mutation<0 ? '-Rp'.number_format(abs($mutation->mutation),0,',','.') : '+Rp'.number_format($mutation->mutation,0,',','.')}}</td>
                                                <td class="text-right">Rp{{number_format($mutation->remaining_bal,0,',','.')}}</td>
                                                <td class="text-left">
                                                {{$mutation->wallet_mutation_detail->description}}
                                                @if($mutation->mutation_detail_id==1)
                                                <a class="link" href="{{url('payment/invoices/'.$mutation->invoice->invoice_number)}}">#{{$mutation->invoice->invoice_number}}</a>
                                                @elseif($mutation->mutation_detail_id==2)
                                                <a class="link" href="{{url('payment/invoices/'.$mutation->order->invoices->invoice_number.'#'.$mutation->order_id)}}">#{{$mutation->order_id}}</a>
                                                @elseif($mutation->mutation_detail_id==3)
                                                <a class="link" href="{{url('sale/'.$mutation->order_id)}}">#{{$mutation->order_id}}</a>
                                                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                

                
            </div>
        </div>
    </div>
</div>

@endsection