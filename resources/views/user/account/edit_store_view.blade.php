@extends('user.templates.t_index')
@section('title', 'Checkout')

@section('content')

<div class="setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('account_settings')}}">Pengaturan</a></li>
                                    <li><a href="{{url('store_settings')}}">Pengaturan Toko</a></li>
                                    <li class="active"><a>Edit Informasi Toko</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>

                @include('user.templates.t_user_setting_nav')
                
                <div class="setting-container">
                    <div class="setting-head">
                        <span>Data Anda selalu rahasia dan tidak akan kami beritahukan kepada pihak ketiga.</span>
                    </div>

                    @if(Request::get('section')=='store_general')
                        @include('user.account.edit_store_general')
                    @elseif(Request::get('section')=='seller_note')
                        @include('user.account.edit_seller_note')
                    @endif
                    

                    <div style="clear:both"></div>
                </div>
                
            </div>
        </div>
    </div>
</div>


@endsection