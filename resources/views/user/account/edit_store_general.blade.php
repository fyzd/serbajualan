<div class="col-sm-12" style="margin-top:20px">
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{url('users/store_general/put')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('put')}}
            <div class="form-group">
                <label class="control-label col-sm-2" style="height:100px">Foto Header Toko<p>945x188 piksel</p></label>
                <div class="col-sm-6" style="overflow:hidden;height:80px">
                    <img src="{{$data->image!=null ? url('assets/images/store').'/'.$data->image: url('assets/images/header-store-default.jpg')}}" width="100%">
                </div>
                <div class="col-sm-6">
                    <input type="file" name="image" class="form-control">
                </div>
                
            </div>
            <hr>
            <div class="form-group">
                <label class="control-label col-sm-2">Deskripsi Toko</label>
                <div class="col-sm-10">
                    <textarea name="description" rows="10" class="form-control">{{$data->description}}</textarea>
                </div>
                
            </div>
                <button class="btn btn-success pull-right">Simpan</button>
        </form>
    </div>
</div>