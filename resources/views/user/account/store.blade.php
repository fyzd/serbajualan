@extends('user.templates.t_index') 
@section('title', 'Serbajualan') 
@section('cssfield')
<link href="{{URL::asset('assets/css2/style2.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/css2/rateit.css')}}" rel="stylesheet">
@endsection
@section('content')

<!-- desktop bar -->
<section class="desktop-bar setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                    <li class="active"><a>{{$user->user_name->name}}</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="desk-com setting-field" style="border:none">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="p-info">
                    <p class="big">Penjual</p>
                    <div class="p-pic" style="float:none; margin-bottom:10px">
                        <img src="{{$user->user_info->image!=null ? url('assets/images/profile').'/'.$user->user_info->image : url('assets/images/pp-default.png')}}" width="60" height="60"  style="border-radius:50%">
                    </div>
                    <div class="p-name">
                        <label style="font-size:16px">{{$user->user_name->name}}</label>
                        <p class="ltl">Username: <label class="ltl2">{{$user->username}}</label></p>
                        <p class="ltl ltl2">{!!$user->address!=null ? '<i class="fa fa-map-marker" style="margin-right:10px"></i>'.ucwords(strtolower($user->address->kota->nama)) : ''!!}</p>
                    </div>

                </div>
                <hr>
                <div class="p-detail">
                    <p class="ltl">Bergabung {{date('d F Y', strtotime($user->created_at))}}</p>
                    <p class="ltl">Memiliki {{$customer}} pelanggan</p>
                    <p class="ltl">
                    @if($semua>0)
                        Menerima {{$terima}} dari {{$semua}} ({{intval($terima/$semua*100)}}%)
                    @else
                        Tidak pernah menolak pesanan
                    @endif
                    </p>
                </div>
                <hr>
                <div class="p-shipping">
                    <p class="big">Pengiriman</p>
                    <table class="table borderless text-center">
                        <tbody>
                            <tr>
                                <td><span class="sp-courier icon-jne" title="JNE"></span></td>
                                <td>{!!$user->courier->jne==1 ? '<i class="fa fa-check">' : '<i class="fa fa-close">'!!}</i></td>
                            </tr>
                            <tr>
                                <td><span class="sp-courier icon-tiki" title="TIKI"></span></td>
                                <td>{!!$user->courier->tiki==1 ? '<i class="fa fa-check">' : '<i class="fa fa-close">'!!}</td>
                            </tr>
                            <tr>
                                <td><span class="sp-courier icon-pos" title="POS"></span></td>
                                <td>{!!$user->courier->pos==1 ? '<i class="fa fa-check">' : '<i class="fa fa-close">'!!}</td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>
            <div class="col-sm-10 set">
                <div class="row">
                    <div class="col-sm-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <div class="sampul">
                            <img src="{{$user->store->image!=null ? url('assets/images/store').'/'.$user->store->image: url('assets/images/header-store-default.jpg')}}" style="width:100%">
                            {!!Auth::id()==$user->id ? '<a href="'.url('users/edit_store?section=store_general').'" class="btn btn-default img-edit">Ganti Gambar</a>' : '' !!}
                        </div>
                    </div>
                </div>
                    
                <div class="row">
                    <div class="col-sm-12">
                        <div class="user-name col-sm-12">
                            <h1>{{$user->user_name->name}}</h1>
                            {!!Auth::id()==$user->id ? '<a href="'.url('users/edit?section=general').'" class="btn btn-default name-edit">Ganti Nama</a>' : ''!!}
                        </div>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="user-name col-sm-12">
                            <p style="color:#808080">{{$user->store->description}}</p>
                            {!!Auth::id()==$user->id ? '<a href="'.url('users/edit_store?section=store_general').'" class="btn btn-default desc-edit">Ganti Deskripsi Toko</a>' : '' !!} 
                        </div>
                        
                    </div>
                </div>

                <div class="row description">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#barang" aria-controls="home" role="tab" data-toggle="tab">Barang</a></li>
                        <li role="presentation"><a href="#note" aria-controls="messages" role="tab" data-toggle="tab">Catatan Penjual</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content sect product-list">
                        <div role="tabpanel" class="tab-pane active featured-product row" id="barang">
                            @if($user->store->closed==1 && $user->id!=Auth::id())
                            <h1 class="text-center">Maaf, saat ini toko sedang tutup</h1>
                            @else
                            @if($user->store->closed==1 && $user->id==Auth::id())
                            <div class="col-sm-12">
                                <div class="alert alert-warning">
                                    <p class="text-center">Saat ini toko Anda sedang tutup, <a href="{{url('store_settings')}}">buka</a></p>
                                </div> 
                            </div>
                            @endif
                            
                            @foreach($products as $product)
                                <div class="col-sm-3 col-xs-6">
                                    <a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}">
                                        <div class="thumbnail paddingless">
                                            <!--span class="e-label"><div>Sale</div></span-->

                                            <span class="service-link text-center">
                                            @if($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                                                <div class="disc-badge">
                                                    <div class="disc-content">
                                                        {{$product->discount->discount}}%
                                                    </div>
                                                </div>
                                            @endif
                                                <img class="img-responsive show-byk" src="{{url('assets/images/products').'/'.$user->id.'/'.$product->product_image_first->name}}" alt="{{$product->name}}">
                                            </span>
                                            <div class="caption">
                                                <div class="category"> 
                                                    <div class="rateit small" data-rateit-value="{{$product->review_rating->avg('rating')}}" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-mode="font"></div>
                                                    <div class="pull-right">{{$product->review_rating->count()}} ulasan</div>
                                                    
                                                </div>
                                            
                                                <a class="wrap" href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}">{{$product->name}}</a>
                                                @if($product->discount==null || $product->discount!=null && $product->discount->expire < date('Y-m-d H:i:s'))
                                                <strong>Rp{{number_format($product->price,0,',','.')}}</strong>
                                                @elseif($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                                                <strong class="discount">Rp{{number_format($product->price,0,',','.')}}</strong><strong>{{'Rp'.number_format($product->price - (round($product->price * ($product->discount->discount/100))),0,',','.')}}</strong>
                                                @endif
                                                
                                            </div>
                                            <div>
                                                <form action="{{url('cart')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                                    @if(Auth::check())
                                                        @if($product->quantity!=0 && Auth::id()!=$product->user_id && Auth::user()->role_id==2)
                                                        <input type="hidden" name="quantity" value=1>
                                                        <button class="btn btn-default form-control" role="button">Tambahkan ke keranjang</button>
                                                        @elseif(Auth::id()==$product->user_id || Auth::user()->role_id==1)
                                                        <a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}" class="btn btn-default form-control" role="button">Lihat Barang</a>
                                                        @else
                                                        <div class="alert alert-danger danger" role="alert">Stok habis</div>
                                                        @endif
                                                        @else
                                                        @if($product->quantity!=0)
                                                        <input type="hidden" name="quantity" value=1>
                                                        <button class="btn btn-default form-control" role="button">Tambahkan ke keranjang</button>
                                                        @else
                                                        <div class="alert alert-danger danger" role="alert">Stok habis</div>
                                                        @endif
                                                    @endif
                                                </form> 
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            
                            <!-- pagination -->
                            <div class="col-sm-12">
                                {{$products->links()}}
                            </div>
                            @endif
                            <div style="clear:both"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="note">
                            <p class="ltl">Catatan Pelapak tetap tunduk terhadap Aturan penggunaan Serbajualan.</p><br>
                            {!! $user->store->seller_note !!}
                        </div>
                    </div>
                    <!-- Nav tabs -->
                    
                    

                </div>
                
            </div>
            
        </div>

    </div>
</section>
@endsection

@section('jsfield')
<script src="{{URL::asset('assets/js/jquery.rateit.min.js')}}"></script>
@endsection