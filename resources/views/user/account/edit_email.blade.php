<div class="col-sm-12 setting-head2">
    <h3>E-mail</h3>

    </div>
    <div class="col-sm-12">
    @if($status=Session::get('status'))
        <div class="alert alert-success">
            <ul>
                <li>{{$status}}</li>
            </ul>
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="{{url('users/email/put')}}" method="post" class="form-horizontal">
    {{csrf_field()}}
    {{method_field('put')}}
        <div class="form-group">
            <label class="control-label col-sm-2">E-mail</label>
            <div class="col-sm-6">
                <input type="email" name="email" value="{{$data->email}}" class="form-control">
            </div>
            
        </div>
        <hr>
        <div class="form-group">
            <label class="control-label col-sm-2">Password Serbajualan</label>
            <div class="col-sm-6">
                <input type="password" name="password" class="form-control">
            </div>
            
        </div>
            <button class="btn btn-success pull-right">Simpan</button>
    </form>
</div>