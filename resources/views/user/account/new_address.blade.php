@extends('user.templates.t_index')
@section('title', 'Serbajualan')
@section('cssfield')
<link rel="stylesheet" href="{{URL::asset('assets/css/selectize.bootstrap3.css')}}">
@endsection
@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row"> 
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('account_settings')}}">Pengaturan</a></li>
                                    <li><a href="{{url('user_addresses')}}">Pengaturan Alamat</a></li>
                                    <li class="active"><a>Tambah Alamat</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                @include('user.templates.t_user_setting_nav')

                <div class="col-sm-12">
                    <div class="row">
                        <h4>Tambah Alamat</h4>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form action="{{url('user_addresses/new/post')}}" method="post" class="form-horizontal">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <p class="control-label col-sm-2">Nama Alamat</p>
                                        <div class="col-sm-9">
                                            <input type="text" name="name" class="form-control" value="{{old('name')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="control-label col-sm-2">Atas Nama</p>
                                        <div class="col-sm-9">
                                            <input type="text" name="pp" class="form-control" value="{{old('pp')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="control-label col-sm-2">Telepon</p>
                                        <div class="col-sm-3">
                                            <input type="text" name="phone" class="form-control" value="{{old('phone')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="control-label col-sm-2">Provinsi</p>
                                        <div class="col-sm-4">
                                            <select name="prov" placeholder="Pilih Provinsi" id="prov" class="form-control">
                                                <option value="">Pilih Provinsi</option>
                                                    @foreach ($provinsi as $prov)
                                                <option value="{{$prov['id']}}">{{$prov['nama']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="control-label col-sm-2">Kabupaten/Kota</p>
                                        <div class="col-sm-4">
                                            <select name="kota" placeholder="Pilih Kabupaten/Kota" id="kota" class="form-control">
                                                <option value="">Pilih Kabupaten/Kota</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="control-label col-sm-2">Kecamatan</p>
                                        <div class="col-sm-4">
                                            <select name="kec" placeholder="Pilih Kecamatan" id="kec" class="form-control">
                                                <option value="">Pilih Kecamatan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="control-label col-sm-2">Alamat</p>
                                        <div class="col-sm-9">
                                            <textarea rows="10" cols="" name="address" class="form-control">{{old('address')}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="control-label col-sm-2">Kode Pos</p>
                                        <div class="col-sm-2">
                                            <input type="text" name="zip" class="form-control" value="{{old('zip')}}">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-success pull-right col-sm-5">Simpan</button>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                

                
            </div>
        </div>
    </div>
</div>

@endsection

@section('jsfield')
<script src="{{URL::asset('assets/js/selectize.min.js')}}"></script>
<script type="text/javascript">
    var xhr;
    var select_state, $select_state;
    var select_city, $select_city;
    var select_kec, $select_kec;

    $select_state = $('#prov').selectize({
        onChange: function(value) {
            if (!value.length) return;
            select_city.disable();
            select_city.clearOptions();
            select_kec.disable();
            select_kec.clearOptions();
            select_city.load(function(callback) {
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: '{{url("/show/kota")}}/' + value,
                    success: function(results) {
                        select_city.enable();
                        callback(results);
                    },
                    error: function() {
                        callback();
                    }
                })
            });
        }
    });

    $select_city = $('#kota').selectize({
        valueField: 'id',
        labelField: 'nama',
        searchField: ['nama'],
        onChange: function(value) {
            if (!value.length) return;
            select_kec.disable();
            select_kec.clearOptions();
            select_kec.load(function(callback) {
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: '{{url("/show/kecamatan")}}/' + value,
                    success: function(results) {
                        select_kec.enable();
                        callback(results);
                    },
                    error: function() {
                        callback();
                    }
                })
            });
        }
    });

    $select_kec = $('#kec').selectize({
        valueField: 'id',
        labelField: 'nama',
        searchField: ['nama']
    });
    select_city  = $select_city[0].selectize;
    select_state = $select_state[0].selectize;
    select_kec  = $select_kec[0].selectize;

    select_city.disable();
    select_kec.disable();
</script>
@endsection