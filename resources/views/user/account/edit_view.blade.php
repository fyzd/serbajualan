@extends('user.templates.t_index')
@section('title', 'Checkout')

@section('content')

<div class="setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('account_settings')}}">Pengaturan</a></li>
                                    @if(Request::get('section')=='general')
                                    <li class="active"><a>Edit Informasi Umum</a></li>
                                    @elseif(Request::get('section')=='email' || Request::get('section')=='password')
                                    <li class="active"><a>Edit Akun</a></li>
                                    @endif
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>

                @include('user.templates.t_user_setting_nav')
                
                <div class="setting-container">
                    <div class="setting-head">
                        <h4>Profil Akun</h4>
                        <span>Data Anda selalu rahasia dan tidak akan kami beritahukan kepada pihak ketiga.</span>
                    </div>

                    @if(Request::get('section')=='general')
                        @include('user.account.edit_general')
                    @elseif(Request::get('section')=='email')
                        @include('user.account.edit_email')
                    @elseif(Request::get('section')=='password')
                        @include('user.account.edit_password')
                    @endif
                    

                    <div style="clear:both"></div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection