<div class="col-sm-12 setting-head2">
    <h3>Informasi Umum</h3>
</div>
<div class="col-sm-12">
    <div class="row">

        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{url('users/general/put')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('put')}}
            <div class="form-group">
                <label class="control-label col-sm-2 col-xs-12">Foto Profil</label>
                <div class="col-sm-1 col-xs-2">
                    <img src="{{$data->image!=null ? url('assets/images/profile').'/'.$data->image : url('assets/images/pp-default.png')}}" height="52" width="52">
                </div>
                <div class="col-sm-6 col-xs-10">
                    <input type="file" name="image" class="form-control">
                </div>
                
            </div>
            <hr>
            <div class="form-group">
                <label class="control-label col-sm-2">Nama</label>
                <div class="col-sm-6">
                    <input type="text" name="name" class="form-control" value="{{$data->name}}">
                </div>
                
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Tanggal Lahir</label>
                <div class="col-sm-6">
                    <input type="date" name="birth" class="form-control" value="{{$data->birth}}">
                </div>
                
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Jenis Kelamin</label>
                <div class="col-sm-6">
                    <label class="radio-inline"><input type="radio" name="sex" value="L" {{$data->sex=='L' ? 'checked' : ''}}>Laki-laki</label>
                    <label class="radio-inline"><input type="radio" name="sex" value="P" {{$data->sex=='P' ? 'checked' : ''}}>Perempuan</label>
                </div>
                
            </div>
                <button class="btn btn-success pull-right">Simpan</button>
        </form>
    </div>
</div>