
<div class="col-sm-12">
    @if($status=Session::get('status'))
    <div class="alert alert-success">
        {{$status}}
    </div>
    @endif
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <h3>Catatan Penjual</h3>
            <form action="{{url('users/seller_note/put')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('put')}}
                
                <textarea name="note" rows="10" id="note" class="form-control">{{ $data->seller_note }}</textarea>
                <br>
                <button class="btn btn-success pull-right">Simpan</button>
            </form>
        </div>
    </div>
</div>

@section('jsfield')
<script src="{{URL::asset('assets/js/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
    CKEDITOR.replace('note');
</script>

@endsection