@extends('user.templates.t_index')
@section('title', 'Serbajualan')
@section('cssfield')
<style>
/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.75em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}

#ft:before{
    margin-left:0;
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  cursor:pointer} /* hover previous stars in list */

</style>
@endsection

@section('content')

<div class="setting-field">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('payment/invoices')}}">Transaksi</a></li>
                                    <li class="active"><a>Ulas Barang</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div class="col-sm-12">
                    <div class="row">
                        <h4>Ulas Barang</h4>
                        
                    </div>
                </div>
                
                
                <div class="col-sm-12" style="padding-left:0">
                    <div class="alert alert-warning">
                        Serbajualan berhak menghapus ulasan yang telah diberikan pembeli apabila mengandung konten yang tidak relevan atau bersifat memprovokasi
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                        @foreach($orders->order_item as $order)
                            <div class="row">
                                <div class="col-sm-1">
                                    <a href="{{url('p/'.$order->products->product_category->slug.'/'.$order->products->product_subcategory->slug.'/'.$order->products->id)}}"><img src="{{url('assets/images/products/'.$order->products->user_id.'/'.$order->products->product_image_first->name)}}" height="60" width="60"></a>
                                </div>
                                <div class="col-sm-9 review" style="padding-left:15px">
                                    <label class="pcart" style="display:block;margin:0;"><a href="{{url('p/'.$order->products->product_category->slug.'/'.$order->products->product_subcategory->slug.'/'.$order->products->id)}}">{{$order->products->name}}</a></label>
                                    @if($order->reviews!=null)
                                    <span class="reviewer">
                                    @for($i=1;$i<=5;$i++)
                                        <i class="fa fa-star {{$i<=$order->reviews->rating ? 'checked' : ''}}"></i>
                                    @endfor
                                    <span style="padding-left:5px">
                                    {{date('d F Y', strtotime($order->reviews->updated_at))}}, pukul {{date('H:i', strtotime($order->reviews->updated_at))}} WIB</span></span>
                                    <p style="color:#333;margin-top:5px">{{$order->reviews->comment}}</p>
                                    @else
                                    <p class="reviewer">Belum ada ulasan</p>
                                    @endif
                                </div>
                                <div class="col-sm-2">
                                    @if($order->reviews!=null)
                                    <a href="{{url('payment/change-review/'.$orders->id.'/'.$order->id)}}" class="btn btn-default pull-right" style="margin-left:5px" data-toggle="modal" data-target="#ubah-ulasan">Ubah Ulasan</a>
                                    @else
                                    <a href="{{url('payment/add-review/'.$orders->id.'/'.$order->id)}}" class="btn btn-default pull-right" style="margin-left:5px" data-toggle="modal" data-target="#ulas">Ulas Barang</a>
                                    @endif
                                </div>
                            </div>
                            
                        @endforeach
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="ulas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
            </div>
        </div>
    </div>


    <div class="modal fade" id="ubah-ulasan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
            </div>
        </div>
    </div>
</div>


@endsection

@section('jsfield')
<script type="text/javascript">
    $(document).ajaxStart(function() { Pace.restart(); });

    $('body').on('click', '[data-toggle="modal"]', function(){ $($(this).data("target")+' .modal-content').load($(this).attr('href')); });

    $("#ulas").on("hidden.bs.modal", function(){
        $("#ulas .modal-content").html("");
    });

    $("#ubah-ulasan").on("hidden.bs.modal", function(){
        $("#ubah-ulasan .modal-content").html("");
    });
    
    $(document).on('click', '#add', function(e){
        e.preventDefault();
        $("#error1").css('display', 'none');
        $('#error1').find('ul').html('');
        $('#add').attr('disabled', true);
        $.ajax({
            url : $('#add-review').attr('action'),
            type: 'post',
            data: $('#add-review').serialize(),
            success : function(data){
                if($.isEmptyObject(data.error)){
                    $("#error1").css('display', 'none');
                    $(location).attr('href', '{{url("payment/transaction")."/"}}'+data+'/product_reviews');
                }else{
                    $('#add').attr('disabled', false);
                    $("#error1").css('display', 'block');
                    $.each( data.error, function( key, value ) {
                        $("#error1").find("ul").append('<li>'+value+'</li>');
                    });
                }
            }
        });
    });

    $(document).on('click', '#review', function(e){
        e.preventDefault();
        $("#error2").css('display', 'none');
        $('#error2').find('ul').html('');
        $('#review').attr('disabled', true);
        $.ajax({
            url : $('#change-review').attr('action'),
            type: 'post',
            data: $('#change-review').serialize(),
            success : function(data){
                if($.isEmptyObject(data.error)){
                    $("#error2").css('display', 'none');
                    $(location).attr('href', '{{url("payment/transaction")."/"}}'+data+'/product_reviews');
                }else{
                    $('#review').attr('disabled', false);
                    $("#error2").css('display', 'block');
                    $.each( data.error, function( key, value ) {
                        $("#error2").find("ul").append('<li>'+value+'</li>');
                    });
                }
            }
        });
    });
</script>
@endsection