@extends('user.templates.t_index')
@section('title', 'Serbajualan')

@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('account_settings')}}">Pengaturan</a></li>
                                    <li class="active"><a>Pengaturan Alamat</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                @include('user.templates.t_user_setting_nav')
                <div class="row" style="padding:15px 0">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <div class="col-sm-6 col-xs-6">
                        <h4>Daftar Alamat</h4>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <a href="{{url('user_addresses/new')}}" class="btn btn-success pull-right">Tambah Alamat</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu" >
                                <td>Nama Alamat</td>
                                <td>Alamat</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($addresses as $address)
                            <tr>
                                <td>
                                    <label id="nama-{{$address->id}}">{{$address->name}}</label>
                                </td>
                                <td>
                                    <p>
                                    <label id="pp-{{$address->id}}">{{$address->pp}}</label><br>
                                    <span id="alamat-{{$address->id}}">{{$address->address}}</span><br>
                                    <span id="keckota-{{$address->id}}">Kec. {{$address->kecamatan->nama}}, 
                                    {{ucwords(strtolower(str_replace(strpos($address->kota->nama, 'KOTA ') === false ? 'KAB. ' : 'KOTA. ','',$address->kota->nama)))}}</span><br>
                                    <span id="prov-{{$address->id}}">{{$address->provinsi->nama.', '.$address->zip_code}}</span><br>
                                    <span id="hp-{{$address->id}}">Telepon/Handphone: {{$address->phone}}</span>
                                    </p>
                                </td>
                                <td>
                                    {!!$address->primary == 1 ? 'Alamat Utama <a href="'.url('user_addresses/'.$address->id.'/update').'" class="btn btn-default">Ubah</a>' : '
                                    <a class="btn btn-success" id="'.$address->id.'" data-toggle="modal" data-target="#set-utama" onclick="setUtama(this);return false;">Set Utama</a> 
                                    <a href="'.url('user_addresses/'.$address->id.'/update').'" class="btn btn-default">Ubah</a>
                                    <!-- Harusnya pake soft delete, ngaruhnya ke detail transaksi
                                    <form action="'.url('user_addresses/'.$address->id.'/delete').'" method="post" style="display: inline">
                                    '.csrf_field().method_field('delete').'
                                    <button type="submit" class="btn btn-danger">Hapus</button>'!!}
                                    </form  -->
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="set-utama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Set Alamat Utama</h2>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning" role="alert">
                        Alamat Utama adalah alamat toko yang digunakan untuk menghitung estimasi biaya pengiriman saat pembeli berbelanja di toko Anda
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>
                            <label id="namamodal"></label><br>
                            <label id="ppmodal"></label><br>
                            <span id="alamatmodal"></span><br>
                            <span id="keckotamodal"></span><br>
                            <span id="provmodal"></span><br>
                            <span id="hpmodal"></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    <form action="{{url('user_addresses/set_primary')}}" method="post">
                        {{csrf_field()}}
                        {{method_field('put')}}
                        <input type="hidden" id="idalamat" name="id">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                        <button type="submit" id="submit" class="btn btn-success success">Set Alamat Utama</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsfield')
<script type="text/javascript">
    function setUtama(idnya)
    {
        var id;
        id=$(idnya).attr('id');
        $('#idalamat').val(id);
        $('#namamodal').text($('#nama-'+id).html());
        $('#ppmodal').text($('#pp-'+id).html());
        $('#alamatmodal').text($('#alamat-'+id).html());
        $('#keckotamodal').text($('#keckota-'+id).html());
        $('#provmodal').text($('#prov-'+id).html());
        $('#hpmodal').text($('#hp-'+id).html());
    }
</script>
@endsection