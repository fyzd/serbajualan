@extends('user.templates.t_index')
@section('title', 'Checkout')
@section('cssfield')
    <link href="{{url('assets/css2/bootstrap-switch.min.css')}}" rel="stylesheet">
@endsection

@section('content')

<div class="setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('account_settings')}}">Pengaturan</a></li>
                                    <li class="active"><a href="#">Pengaturan Toko</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>

                @include('user.templates.t_user_setting_nav')
                
                <div class="setting-container">
                    <div class="setting-head">
                        <span>Pengaturan Informasi Toko, Catatan Penjual, dan Tutup Toko.</span>
                    </div>
                    <div class="setting-head2">
                        <div class="row">
                            <div class="col-sm-6 col-xs-8">
                                <h3>Informasi Toko</h3>
                            </div>
                            <div class="col-sm-6 col-xs-4">
                                <a href="{{url('users/edit_store?section=store_general')}}" class="btn btn-success pull-right">Edit</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Foto Header Toko</label>
                                <div class="col-sm-6" style="height:80px;overflow:hidden">
                                    <img src="{{$data->image!=null ? url('assets/images/store').'/'.$data->image: url('assets/images/header-store-default.jpg')}}" width="100%">
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-xs-3">Deskripsi Toko</label>
                                <span class="col-sm-10 col-xs-9 row">{{$data->description}}</span>
                            </div>
                        </form>
                    </div>

                    <div class="col-sm-12 setting-head2">
                        <div class="row">
                            <div class="col-sm-6 col-xs-8">
                                <h3>Catatan Penjual</h3>
                            </div>
                            <div class="col-sm-6 col-xs-4">
                                <a href="{{url('users/edit_store?section=seller_note')}}" class="btn btn-success pull-right">Edit</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="setting-head">
                        <p>Catatan Penjual diperuntukkan bagi pelapak yang ingin memberikan catatan tambahan yang tidak terkait dengan deskripsi barang kepada calon pembeli. Catatan Penjual tetap tunduk terhadap Aturan penggunaan Serbajualan.</p>
                    </div>
                    <div class="setting-head2">
                        {!! $data->seller_note !!}
                    </div>
                    
                    <div class="setting-head2">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <h3>Tutup Toko</h3>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <input type="checkbox" name="close" style="display:none" {{$data->closed==1 ? 'checked' : ''}}>
                            </div>
                        </div>
                        
                    </div>
                    <div class="setting-head">
                        <p>Anda dapat menggunakan fitur tutup toko untuk menonaktifkan toko Anda selama waktu yang dapat Anda tentukan sendiri. Manfaatkan fitur ini ketika Anda akan berlibur atau ketika Anda sedang tidak bisa menangani transaksi di Serbajualan.</p>
                    </div>
                    <div style="clear:both"></div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsfield')
<script src="{{url('assets/js/bootstrap-switch.min.js')}}"></script>
<script type="text/javascript">
    $("[name='close']").bootstrapSwitch();
    $("[name='close']").on('switchChange.bootstrapSwitch', function(event, state) {
        $.ajax({
            url : "{{url('users/close_store/put')}}",
            type : 'post',
            data : {_token:'{{csrf_token()}}',_method:'put',status:state},
            success: function(data){
            }
        });
    });
</script>
@endsection