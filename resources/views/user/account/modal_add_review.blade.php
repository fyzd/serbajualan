<div class="modal-header">
<h2>Ulas Barang</h2>
</div>
<div class="modal-body">
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-danger" id="error1" style="display:none">
            <ul></ul>
        </div>
        <div class="row form-group">
            <div class="col-sm-2">
                <img width="60" height="60" src="{{url('assets/images/products').'/'.$product->products->user_id.'/'.$product->products->product_image_first->name}}">
            </div>
            <div class="col-sm-10">
                <label><a href="{{url('p/'.$product->products->product_category->slug.'/'.$product->products->product_subcategory->slug.'/'.$product->products->id)}}">{{$product->products->name}}</a></label>
            </div>
        </div>
        <form action="{{url('payment/add-review')}}" method="post" id="add-review">
            {{csrf_field()}}
            <input type="hidden" name="order_id" value="{{$order_id}}">
            <input type="hidden" name="order_item_id" value="{{$product->id}}">
            <input type="hidden" name="product_id" value="{{$product->products->id}}">
        <div class="row form-group">
            <div class="col-sm-12">
                Penilaianmu untuk barang ini
            </div>
            <div class="col-sm-12">
                <fieldset class="rating">
                    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Keren!"></label>
                    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Bagus"></label>
                    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Biasa saja"></label>
                    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Jelek"></label>
                    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" id="ft" title="Kecewa!"></label>
                </fieldset>
            </div>
            
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                Ulasan
            </div>
            <div class="col-sm-12">
                <textarea class="form-control" rows="5" placeholder="Berikan ulasanmu" name="comment"></textarea>
            </div>
        </div>
    </div>
</div>

</div>
<div class="modal-footer">
    
    <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
    <input type="submit" value="Simpan" class="btn btn-success" id="add">
</form>

</div>