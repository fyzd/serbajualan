@extends('user.templates.t_index')
@section('title', 'Serbajualan')
@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li class="active"><a>Transaksi Penjualan</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div class="col-sm-12">
                    <div class="row">
                        <h3 style="margin-top:10px">Transaksi</h3>
                        <ul class="nav nav-tabs nav-justified">
                            <li><a href="{{url('payment/invoices')}}" >Pembelian <span class="badge">{{$cart->getTransaction()->getData()->purchase}} belum dibayar</span></a></li>
                            <li class="active"><a href="{{url('sale')}}" >Penjualan <span class="badge">{{$cart->getTransaction()->getData()->sale}} belum diproses</span></a></li>
                        </ul><br>
                        @foreach($orders as $order)
                        <div class="panel panel-default">
                            <div class="panel-body">
                            
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="trx">
                                            <tr>
                                                <th>No. Transaksi</th>
                                                <th style="width:400px">Status Barang</th>
                                                <th>Pembeli</th>
                                                <th>Dibeli pada</th>
                                                <th style="width:150px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><a href="{{url('sale/'.$order->id)}}" class="link">{{$order->id}}</a></td>
                                                <td><span class="label label-@if($order->order_status_id==2)warning @elseif($order->order_status_id==3)primary @elseif($order->order_status_id==4)success @elseif($order->order_status_id==5 || $order->order_status_id==6 || $order->order_status_id==7)danger @endif">{{$order->order_status->description}}</span></td>
                                                <td><a class="link" href="{{url('u/'.$order->shipments->buyer_address->user->username)}}">{{$order->shipments->buyer_address->user->user_name->name}}</a></td>
                                                <td>{{$order->created_at}}</td>
                                                <td class="text-center"><a href="{{url('sale/'.$order->id)}}" class="btn btn-success pull-right">Lihat Detail</a></td>
                                            </tr>
                                        </tbody>
                                        <thead class="trx">
                                        <tr>
                                                <th>Gambar</th>
                                                <th>Barang</th>
                                                <th></th>
                                                <th>Kuantitas</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->order_item as $sale)
                                            <tr>
                                                <td><a href="{{url('p/'.$sale->products->product_category->slug.'/'.$sale->products->product_subcategory->slug.'/'.$sale->products->id)}}"><img src="{{url('assets/images/products').'/'.$sale->products->user_id.'/'.$sale->products->product_image_first->name}}" height="30" width="30"></a></td>
                                                <td><a href="{{url('p/'.$sale->products->product_category->slug.'/'.$sale->products->product_subcategory->slug.'/'.$sale->products->id)}}">{{ strlen($sale->products->name) > 50 ? substr($sale->products->name,0,47).'...' : $sale->products->name }}</a></td>
                                                <td></td>
                                                <td>{{$sale->quantity}}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach  
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                        @endforeach  
                        
                        <!-- pagination -->
                        {{$orders->links()}}  

                    </div>
                </div>
                
                

                
            </div>
        </div>
    </div>
</div>

@endsection