@extends('user.templates.t_index')
@section('title', 'Account')
@section('cssfield')
<link rel="stylesheet" href="{{URL::asset('assets/css/selectize.bootstrap3.css')}}">
@endsection
@section('content')

<div class="setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('account_settings')}}">Pengaturan</a></li>
                                    <li class="active"><a>Pengaturan Pengiriman</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                @include('user.templates.t_user_setting_nav')
                
                <div class="setting-container">
                    <div class="setting-head">
                        <span>Pilih paket jasa pengiriman untuk semua barang Anda di bawah ini.</span>
                    </div>
                
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="col-sm-12">
                        <form action="{{url('users/courier_settings/post')}}" method="post" >
                            {{csrf_field()}}
                            {{method_field('put')}}
                            <div class="checkbox">
                                <label><input type="checkbox" name="jne" value=1 {{$kurir->jne == 1 ? 'checked' : ''}}>JNE</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="tiki" value=1 {{$kurir->tiki == 1 ? 'checked' : ''}}>TIKI</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="pos" value=1 {{$kurir->pos == 1 ? 'checked' : ''}}>POS Indonesia</label>
                            </div>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                        
                    </div>
                    <div style="clear:both"></div>
                </div>
                

                
            </div>
        </div>
    </div>
</div>
@endsection
