@extends('user.templates.t_index')
@section('title', 'Serbajualan')

@section('content')
<div class="setting-field">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li class="active"><a>Transaksi Pembelian</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div class="col-sm-12">
                    <div class="row">
                        <h3 style="margin-top:10px">Transaksi</h3>
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="{{url('payment/invoices')}}">Pembelian <span class="badge">{{$cart->getTransaction()->getData()->purchase}} belum dibayar</span></a></li>
                            <li><a href="{{url('sale')}}" >Penjualan <span class="badge">{{$cart->getTransaction()->getData()->sale}} belum diproses</span></a></li>
                        </ul><br>
                        @foreach($invoices as $invoice)
                        <div class="panel panel-default">
                            <div class="panel-body">
                            
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="trx">
                                            <tr>
                                                <th>No. Tagihan</th>
                                                <th>Total Pembayaran</th>
                                                <th style="width:250px">Status Tagihan</th>
                                                <th style="width:230px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><a class="link" href="{{url('payment/invoices/'.$invoice->invoice_number)}}">{{$invoice->invoice_number}}</a></td>
                                                <td>Rp{{number_format($invoice->amount, 0,',','.')}}</td>
                                                <td><span style="margin-right:10px" class="label label-@if($invoice->invoice_status_id==1 || $invoice->invoice_status_id==2)warning  @elseif($invoice->invoice_status_id==3)success @elseif($invoice->invoice_status_id==4 || $invoice->invoice_status_id==5 || $invoice->invoice_status_id==6 || $invoice->invoice_status_id==7)danger @endif">{{$invoice->invoice_status->description}}</span> ({{$invoice->updated_at}})
                                                {!!$invoice->invoice_status_id==1 ? '<br> <a class="link" href="'.url('payment/purchases/'.$invoice->invoice_number).'">Pilih Metode Bayar</a>' : '' !!}</td>
                                                <td>
                                                    <a href="{{url('payment/invoices/'.$invoice->invoice_number)}}" class="btn btn-success pull-right">Lihat Detail</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <thead class="trx">
                                            <tr>
                                                <th>Penjual/No. Transaksi</th>
                                                <th>Barang</th>
                                                <th>Status Pembelian</th>
                                                <th style="width:230px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                        @foreach($invoice->order as $order)
                                            <tr>
                                                <td>
                                                <a class="link" href="{{url('payment/invoices/'.$invoice->invoice_number.'#'.$order->id)}}"> {{ strtoupper(strlen($order->order_item[0]->products->stores->users->user_name->name) > 10 ? substr($order->order_item[0]->products->stores->users->user_name->name,0,7).'.../'.$order->id : $order->order_item[0]->products->stores->users->user_name->name.'/'.$order->id) }}</a>
                                                </td>
                                                <td>
                                                <a style="display:inline-block" href="{{url('p/'.$order->order_item[0]->products->product_category->slug.'/'.$order->order_item[0]->products->product_subcategory->slug.'/'.$order->order_item[0]->products->id)}}">
                                                    <img src="{{url('assets/images/products').'/'.$order->order_item[0]->products->user_id.'/'.$order->order_item[0]->products->product_image_first->name}}" height="30" width="30" id="image-{{$order->order_item[0]->id}}">
                                                </a>
                                                {!!$order->order_item->count()>1 ? '<a href="'.url('payment/invoices/'.$invoice->invoice_number).'">+ '.intval($order->order_item->count()-1).'</a>' : ''!!}
                                                <a href="{{url('p/'.$order->order_item[0]->products->product_category->slug.'/'.$order->order_item[0]->products->product_subcategory->slug.'/'.$order->order_item[0]->products->id)}}" id="product-{{$order->order_item[0]->id}}" style="display:none">{{$order->order_item[0]->products->name}}</a><p id="pid-{{$order->order_item[0]->id}}" style="display:none">{{$order->order_item[0]->products->id}}</p>
                                                </td>
                                                <td>
                                                    <ul class="steps">
                                                        @if($order->order_status_id==2 || $order->order_status_id==3 || $order->order_status_id==4)
                                                        <li title="Diproses Penjual"><div class="sprite icon-processed" ></div></li>
                                                        @endif
                                                        @if($order->order_status_id==3 || $order->order_status_id==4)
                                                        <li title="Dikirim"><div class="sprite icon-sent"></div></li>
                                                        @endif
                                                        @if($order->order_status_id==4)
                                                        <li title="{{$order->order_status->description}}"><div class="sprite icon-received"></div></li>
                                                        @endif
                                                        @if($order->order_status->id==5)
                                                        <li title="{{$order->order_status->description}}"><div class="sprite icon-refund"></div></li>
                                                        @endif
                                                    </ul>  
                                                    {{$order->order_status->description}}
                                                </td>
                                                <td>
                                                
                                                @if($order->order_item[0]->reviews==null && $order->order_status_id==4)
                                                    <a href="{{url('payment/transaction/'.$order->id.'/product_reviews')}}" class="btn btn-default pull-right" style="margin-left:5px" >Ulas Barang</a>
                                                @elseif($order->order_item[0]->reviews!=null && $order->order_status_id==4)
                                                    <a href="{{url('payment/transaction/'.$order->id.'/product_reviews')}}" class="btn btn-default pull-right" style="margin-left:5px" >Ubah Ulasan</a>
                                                @endif
                                                @if($order->order_status_id==3)
                                                    <a style="cursor:pointer" class="form-control btn btn-warning pull-right" id="{{$order->id}}" data-toggle="modal" data-target="#konfir-diterima" onclick="getOrderId({{$order->id}})">Konfirmasi Barang Diterima</a>
                                                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                
                                
                                
                                
                            </div>
                        </div>
                        @endforeach

                        <!-- pagination -->
                        {{$invoices->links()}}
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="konfir-diterima" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Konfirmasi Barang Diterima</h2>
                    </div>
                    <div class="modal-body">
                    <div class="alert alert-success" role="alert">
                        Dengan ini saya menyatakan barang sudah diterima.
                    </div>

                    </div>
                    <div class="modal-footer">
                        <form action="{{url('payment/received')}}" method="post" id="received">
                            {{csrf_field()}}
                            <input type="hidden" name="order_id" id="orderid">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                            <input type="submit" value="Konfirmasi" class="btn btn-success" id="confirm">
                        </form>

                    </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@section('jsfield')
<script type="text/javascript">

    function getOrderId(id)
    {
        $('#orderid').val(id);
    }
</script>
@endsection