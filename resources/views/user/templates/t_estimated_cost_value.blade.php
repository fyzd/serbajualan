<div class="col-sm-12">
<hr>
</div>
<div class="ecost-jumlah col-sm-12">
    <div class="row">
        <label class="control-label col-sm-3">Estimasi biaya ke {{$namakota}}</label>
        <div class="col-sm-9">
            <table class="table table-responsive">
                <tr>
                    <td>PENGIRIMAN</td>
                    <td>SERVIS</td>
                    <td>WAKTU PENGANTARAN</td>
                    <td>BIAYA KIRIM</td>
                    <td>HARGA + BIAYA KIRIM</td>
                </tr>
                @foreach($data as $d)
                @foreach($d as $a)
                @foreach($a['costs'] as $k)
                @foreach($k['cost'] as $d)
                <tr>
                    <td>{{strtoupper($a['code'])}}</td>
                    <td>{{strtoupper($a['code'].' '.$k['service'])}}</td>
                    <td>{{$d['etd']!=null ? $d['etd'].' hari' : 'Sampai di hari yang sama'}}</td>
                    <td>Rp{{$d['value']}}</td>
                    <td>Rp{{$price+$d['value']}}</td>
                </tr>
                @endforeach
                @endforeach
                @endforeach
                @endforeach
            </table>
        </div>
        
        
        
    </div>
</div>