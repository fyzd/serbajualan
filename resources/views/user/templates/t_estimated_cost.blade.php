<div class="ecost">
        <div class="ecost-jumlah col-sm-12">
            <div class="row">
                <label class="control-label col-sm-3">Masukkan Jumlah</label>
                <div class="col-sm-3">
                    <input type="number" name="qty" class="form-control" value="1">
                </div>
                
                
            </div>
        </div>
        <div class="col-sm-12">
        <hr>
        </div>
        <div class="ecost-tujuan col-sm-12">
            <div class="row">
                <label class="control-label col-sm-3">Masukkan Tujuan</label>
                <div class="col-sm-4">
                    <select class="form-control" id="prov">
                        <option disabled selected>Pilih Provinsi</option>
                        @foreach($provinsi as $p)
                        <option value="{{$p['province_id']}}">{{$p['province']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4">
                    <select class="form-control" id="kota" disabled>
                        <option disabled selected>Pilih Kota/Kabupaten</option>
                    </select>
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-success" id="cek" disabled>Cek</button>
                </div>
                
            </div>
        </div>

</div>

<div class="ecost-total">

</div>
<div style="clear:both"></div>

<script type="text/javascript">
    $('#prov').on('change', function(){
        var prov=$(this).val();
        var token='{{csrf_token()}}';
        $('#cek').prop('disabled',true);
        $('#kota').prop('disabled',true);
        $('#kota').html('<option>Pilih Kota/Kabupaten</option>');

        $.ajax({
            url : "{{url('ro/get_kota')}}",
            type: 'post',
            data: {_token:token, prov_id:prov},

            success: function(data){
                $('#cek').prop('disabled',false);
                $('#kota').prop('disabled',false);
                $('#kota').html(data);
            }
        });
    });

    $('#cek').on('click', function(){
        var prov=$('#prov').val();
        var kota=$('#kota').val();
        var quantity=$('input[name=qty]').val();
        var namakota=$('#kota').children(':selected').text();
        var product='{{$product_id}}';
        var weight='{{$weight}}';
        var asal='{{$kota_asal}}';
        var kurir={!! json_encode($kurir) !!};
        var price={{$price}};
        var token='{{csrf_token()}}';
        $('.ecost-total').html('');

        $.ajax({
            url : "{{url('ro/total')}}",
            type: 'post',
            data: {_token:token,prov_id:prov,kota_id:kota,product_id:product,weight:weight,kota_asal:asal,kurir:kurir,namakota:namakota,quantity:quantity,price:price},

            success: function(data){
                $('.ecost-total').html(data);
            }

        });
    });
</script>