<!DOCTYPE html>
<html lang="en">

<head>
	<title>@yield('title')</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="{{URL::asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css2/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css2/style.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css2/responsive.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css2/pace-theme-minimal.css')}}" rel="stylesheet"> 
	@yield('cssfield')

	<script src="{{URL::asset('assets/js/pace.min.js')}}"></script>
	<script type="text/javascript">
		//Pace js
		paceOptions = {
			elements: true
		};
	</script>

</head>

<body>

	<div class="navbar-fixed-top">
		<div id="flipkart-navbar">
			<div class="nav1">
				<div class="container">
					<div class="row row1">
						<ul class="largenav col-sm-6">
						</ul>
						<ul class="largenav col-sm-6 ta-right">
							<li class="upper-links" style="padding-left:0"><a class="links" href="#">DOWNLOAD APP</a></li>
							@if(Auth::check())
							<li class="upper-links">
								<div class="account">
								@if(Auth::user()->role_id==2)
									<a class="links" type="button" data-toggle="dropdown">{{strtoupper(Auth::user()->user_name->name)}} 
									@if($cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale!=0)
									({{$cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale}})
									@endif
									<span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right drop-user">
										<li><a href="{{url('users/account')}}">Panel Akun</a></li>
										<li><a href="{{url('payment/invoices')}}">Transaksi ({{$cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale}})</a></li>
										<li><a href="{{url('logout')}}">Logout</a></li>
									</ul>
								@else
								@endif
								</div>
								
							</li>
							@else

							<li class="upper-links"><a class="register" href="{{url('login')}}">Login</a></li>
							<li class="upper-links"><a class="register" href="{{url('register')}}">Daftar</a></li>
							@endif


						</ul>
					</div>
				</div>
			</div>
			
			<div class="atas">
				<div class="container ">
					<div class="row row2">
						<div class="col-sm-2">
							<h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ Serbajualan</span></h2>
							<h2 class="judul"><span class="largenav"><a href="{{url('')}}">Serbajualan</a></span></h2>
							<h2 class="kat"><span class="largenav">☰ Kategori</span></h2>
						</div>
						
						<div class="col-sm-{{Auth::check() ? Auth::user()->role_id==2 ? '7' : '8' : '8'}} col-xs-12">
							<div class="flipkart-navbar-search smallsearch">
								<div class="row">
									<div class="col-sm-11 col-xs-10" style="padding:0">
										<div class="input-group">
											<div class="input-group-btn search-panel">
												<button type="button" class="btn btn-default dropdown-toggle filter-nav" data-toggle="dropdown">
													<span id="search_concept">{{Request::is('c/*') ? 'Kategori saat ini' : 'Semua Kategori'}}</span> <span class="caret"></span>
												</button>
												<ul class="dropdown-menu d-search" role="menu">
													{!!Request::is('c/*') ? '<li><a href="#current">Kategori saat ini</a>' : '<li><a href="#all">Semua Kategori</a>'!!}</li>
												@foreach($categories as $category)
													<li><a href="#{{$category->slug}}">{{$category->description}}</a></li>
												@endforeach
												</ul>
											</div>
											<form action="{{Request::is('c/*') ? url()->current() : url('products')}}" type="get" id="search">
												<input class="flipkart-navbar-input" type="" placeholder="Cari produk, merk, atau yang lainnya" name="search" value="{{Request::get('search')}}">
										</div>
									</div>
											<button class="flipkart-navbar-button col-sm-1 col-xs-2">
												<i class="fa fa-search"></i>
											</button>
										</form>
									
								</div>
							</div>
						</div>			
						
						@if(Auth::check())
							@if(Auth::user()->role_id==2)
						<div class="cart largenav col-sm-1" style="padding-right:0">
							<a class="cart-button" href="{{url('dompet')}}">
							<i class="fa fa-credit-card cart-svg" style=" margin-right: 4px;"></i> Dompet
							</a>
						</div>
						<div class="cart largenav col-sm-2">
							<a class="cart-button cart-btn" href="{{url('cart')}}">
								<i class="fa fa-shopping-cart cart-svg"></i> <span class="keranjang">Keranjang</span>
								<span class="item-number">{{ $cart->totalProduct() > 0 ? $cart->totalProduct() : '0'}}</span>
							</a>
							<a class="user-button" data-toggle="dropdown">
								<img class="img-responsive" src="{{Auth::user()->user_image->image!=null ? url('assets/images/profile/'.Auth::user()->user_image->image) : url('assets/images/pp-default.png')}}"><span class="ind">{{$cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale!=0 ? $cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale : ''}}</span><span class="caret" {!! $cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale!=0 ? 'style="margin-top:15px"' : '' !!}></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right drop-user" style="margin-top:16px">
								<li><span>Halo,</span><br><a href="{{url('users/account')}}"><b>{{Auth::user()->user_name->name}}</b></a><hr style="margin:5px 0;"></li>
								<li><a href="{{url('payment/invoices')}}">Transaksi 
								@if($cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale!=0)
								({{$cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale}})
								@endif</a></li>
								<li><a href="{{url('logout')}}">Logout</a></li>
							</ul>
						</div>
							@else
						<div class="cart largenav col-sm-2 adm2">
							<a class="admin-button" data-toggle="dropdown">
								<img class="img-responsive" src="{{Auth::user()->user_image->image!=null ? url('assets/images/profile/'.Auth::user()->user_image->image) : url('assets/images/pp-default.png')}}"><span class="adm">{{Auth::user()->user_name->name}}</span><span class="caret" ></span>
							</a>
							<ul class="dropdown-menu dropdown-menu drop-user" style="margin-top:16px">
								<li><span>Halo,</span><br><a href="{{url('admin')}}"><b>{{Auth::user()->user_name->name}}</b></a><hr style="margin:5px 0;"></li>
								
								<li><a href="{{url('logout')}}">Logout</a></li>
							</ul>
						</div>
							@endif
						@else
						<div class="cart largenav col-sm-2">
							<a class="cart-button cart-btn" href="{{url('cart')}}">
								<i class="fa fa-shopping-cart cart-svg"></i> <span class="keranjang">Keranjang</span>
								<span class="item-number">{{ $cart->totalProduct() > 0 ? $cart->totalProduct() : '0'}}</span>
							</a>
							<a class="cart-button user-button cart-setengah" style="background-color:#8ec63f"href="{{url('login')}}">
								<span>Login</span>
							</a>
						</div>
						@endif
					</div>
					<div class="row row3">
						<nav class="navbar navbar-default navbar-static">

							<div class="collapse navbar-collapse js-navbar-collapse">
								<ul class="nav navbar-nav">
								@foreach($categories as $category)
									<li class="dropdown dropdown-large">
										<a style="cursor:pointer" class="dropdown-toggle" data-toggle="dropdown">{{$category->description}} <b class="caret"></b></a>

										<ul class="dropdown-menu dropdown-menu-large row">
											<li class="col-sm-12">

												<ul>
													<li class="dropdown-header">
														<a class="nav-kategori" href="{{url('c').'/'.$category->slug}}">{{$category->description}}
														</a>
													</li>
													@foreach($category->product_subcategory as $subcategory)
													<li>
														<a href="{{url('c').'/'.$category->slug.'/'.$subcategory->slug}}">{{$subcategory->description}}
														</a>
													</li>
													@endforeach
													
												</ul>

											</li>
											
										</ul>

									</li>
									@endforeach
								</ul>
								

							</div>
							<!-- /.nav-collapse -->
						</nav>

					</div>
				</div>
			</div>
		</div>
	</div>

	
	<div id="mySidenav" class="sidenav">
		<div class="container" style="background-color: #2874f0; padding-top: 10px;">
			<span class="sidenav-heading">Home</span>
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
		</div>
		<a href="http://clashhacks.in/">Link</a>
		<a href="http://clashhacks.in/">Link</a>
		<a href="http://clashhacks.in/">Link</a>
		<a href="http://clashhacks.in/">Link</a>
	</div>
	
	<div class="content">
		@yield('content')
	</div>
	
	<footer>
		<div class="footer-top">
			<div class="container">
				<div class="row">
					
					<div class="col-sm-3">
						<h2>Layanan Pelanggan</h2>
						<ul class="list-unstyled">
							<a href=""><li>Pusat Bantuan</li></a>
							<a href=""><li>Pembayaran</li></a>
							<a href=""><li>Cara Pembelian</li></a>
							<a href=""><li>Pengiriman</li></a>
							<a href=""><li>Jaminan Aman</li></a>
						</ul>
						
					</div>
					<div class="col-sm-3">
						<h2>Serbajualan</h2>
						<ul class="list-unstyled">
							<a href=""><li>Tentang Serbajualan</li></a>
							<a href=""><li>Aturan Penggunaan</li></a>
							<a href=""><li>Kebijakan Privasi</li></a>
							<a href=""><li>Berita & Pengumuman</li></a>
							<a href=""><li>Jual di Serbajualan</li></a>
						</ul>
					</div>
					<div class="col-sm-6">
						<h2>Newsletter</h2>
						
						<form class="navbar-for" role="email">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Alamat E-mail Anda">
								<span class="nav-search"><a href="#"><i class="fa fa-envelope"></i></a></span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<p>&copy 2017 Hak Cipta Terpelihara - Serbajualan</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<!-- ALL JAVASCRIPT -->
	<script src="{{URL::asset('assets/js/jquery.js')}}"></script>
	<script src="{{URL::asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>

	<script type="text/javascript">
		var navOffset = 150;

		// Calculating the navbar height, and store it in a variable
		var navbarHeight = $('.atas').outerHeight();
		var kelewatNav=false;

		$(window).on('scroll', function () {
			if ($(window).width() > 768) {
				
				if ($(window).scrollTop() >= navOffset) { // if the navbar offset equal or bigger than user scrolling, make it fixed
					kelewatNav = true;
					$('.judul').removeClass('animated2 fadeIn');
					$('.judul').addClass('animated fadeOutUp');
					
					$('.kat').addClass('animated2 fadeIn');

					$('.row3').slideUp();
					$('.row1').slideUp();

					$('.cart-btn').removeClass('cart-full');
					$('.cart-btn').addClass('cart-setengah');
					$('.keranjang').css('display','none');

					$('.user-button').addClass('animated-delay fade-in');
					

					$('[data-toggle="dropdown"]').parent().removeClass('open');


				} else { // if not, return it to its initial state
					$('.atas').removeClass('navbar-fixed-top');
					$('.row3').slideDown();
					if(kelewatNav){
						$('.row1').slideDown();

						$('.judul').removeClass('animated fadeOutUp');
					$('.judul').addClass('animated2 fadeIn');
						$('.kat').removeClass('animated2 fadeIn');

						$('.kat').css('display','block');
					$('.kat').addClass('animated fadeOutUp');
					$('.kat').fadeOut();
					

					kelewatNav=false;
					}
					
					$('.user-button').removeClass('animated-delay fade-in');
					

					$('.cart-btn').removeClass('cart-setengah');
					$('.keranjang').css('display','inline');
					
					
					$('[data-toggle="dropdown"]').parent().removeClass('open');
					
					$('body').css('padding-top', '0');
				}
			}
		});

		function openNav() {
			document.getElementById("mySidenav").style.width = "70%";
			// document.getElementById("flipkart-navbar").style.width = "50%";
			document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.body.style.backgroundColor = "rgba(0,0,0,0)";
		}

		$('.kat').on('click', function () {
			$('.row3').slideToggle('fast');
		});

		$('.d-search').find('a').click(function(e) {
			e.preventDefault();
			var param = $(this).attr("href").replace("#","");
			var concept = $(this).text();
			$('.search-panel span#search_concept').text(concept);

			if(param=='all'){
				$('#search').attr('action', '{{url("products")}}');
			}
			else if(param!='current'){
				$('#search').attr('action', '{{url("c")}}/'+param);
			} else{
				$('#search').attr('action', '{{url()->current()}}/'+param);
			}
		});
	</script>
	@yield('jsfield')

</body>

</html>