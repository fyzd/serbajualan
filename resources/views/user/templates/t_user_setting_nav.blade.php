<h3 style="margin-top:10px">Pengaturan</h3>
<ul class="nav nav-tabs nav-justified">
    <li class="{{Request::is('account_settings') || Request::is('users/edit') ? 'active' : ''}}"><a href="{{url('account_settings')}}" >Akun</a></li>
    <li class="{{Request::is('user_addresses') || Request::is('user_addresses/*') ? 'active' : ''}}"><a href="{{url('user_addresses')}}" >Alamat</a></li>
    <li class="{{Request::is('store_settings') || Request::is('users/edit_store') ? 'active' : ''}}"><a href="{{url('store_settings')}}" >Toko</a></li>
    <li class="{{Request::is('users/courier_settings') ? 'active' : ''}}"><a href="{{url('users/courier_settings')}}" >Pengiriman</a></li>
</ul>