<div class="left-sidebar">
    <div class="brands_products" ><!--brands_products-->
        
        <a href="{{url('my_products/new')}}" class="btn jual">
            Jual Barang
        </a>

        <div class="list-group">
            
            <a href="{{url('users/account')}}" class="list-group-item">Panel Akun</a>
            <a href="{{url('my_products')}}" class="list-group-item">Daftar Barang <span class="badge">{{$cart->getTransaction()->getData()->purchase+$cart->getProduct()}}</span></a>
            <a href="{{url('dompet')}}" class="list-group-item">Dompet Virtual</a>
            <a href="{{url('payment/invoices')}}" class="list-group-item">Transaksi <span class="badge">{{$cart->getTransaction()->getData()->purchase+$cart->getTransaction()->getData()->sale}}</span></a>
            <a href="{{url('account_settings')}}" class="list-group-item">Pengaturan</a>
        </div>

        <a href="{{url('u').'/'.Auth::user()->username}}" class="btn jual">
            Preview Toko
        </a>
    </div>
</div>