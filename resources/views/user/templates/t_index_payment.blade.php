<!DOCTYPE html>
<html lang="en">

<head>
	<title>@yield('title')</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="{{URL::asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css2/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css2/style.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css2/responsive.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css2/pace-theme-minimal.css')}}" rel="stylesheet"> 
	@yield('cssfield')

	<script src="{{URL::asset('assets/js/pace.min.js')}}"></script>
	<script type="text/javascript">
		//Pace js
		paceOptions = {
			elements: true
		};
	</script>

</head>

<body>

	<div id="flipkart-navbar">
		<div class="atas" style="background-color:#fff; padding:12px 0; height:48px; border-bottom: 1px solid #ddd;">
			<div class="container ">
				<div class="row">
					<div class="col-sm-12">
						<h2 style="font-size: 14pt; margin: 0"><span class="largenav"><a href="{{url('')}}">Serbajualan</a></span></h2>
                    </div>
					
				</div>
			</div>
		</div>
	</div>

	@yield('content')

	<!-- ALL JAVASCRIPT -->
	<script src="{{URL::asset('assets/js/jquery.js')}}"></script>
	<script src="{{URL::asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>

	@yield('jsfield')

</body>

</html>