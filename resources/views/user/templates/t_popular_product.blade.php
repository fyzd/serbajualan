<div class="row slick-slider popular">
@foreach($popular as $product)
    <div class="col-sm-2">
        <a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}">
            <div class="thumbnail">
                <span class="service-link text-center">
                @if($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                    <div class="disc-badge">
                        <div class="disc-content">
                            {{$product->discount->discount}}%
                        </div>
                    </div>
                @endif
                        <img class="show-byk" data-lazy="{{ url('assets/images/products').'/'.$product->user_id.'/'.$product->product_image_first->name }}" alt="">
                    
                </span>
                <div class="caption">
                    <div class="category">
                        <div class="rateit small" data-rateit-value="{{$product->review_rating->avg('rating')}}" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-mode="font"></div>
                        <div class="pull-right">{{$product->review_rating->count()}} ulasan</div>
                    </div>
                    <a class="wrap" href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}">{{$product->name}}</a>
                    @if($product->discount==null || $product->discount!=null && $product->discount->expire < date('Y-m-d H:i:s'))
                    <strong>Rp{{number_format($product->price,0,',','.')}}</strong>
                    @elseif($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                    <strong class="discount">Rp{{number_format($product->price,0,',','.')}}</strong><strong>{{'Rp'.number_format($product->price - (round($product->price * ($product->discount->discount/100))),0,',','.')}}</strong>
                    @endif
                    <div class="store-name">
                        <a href="{{url('u/'.$product->users->username)}}">{{$product->users->user_name->name}}</a>
                        <p><i class="fa fa-map-marker"></i>{{ucwords(strtolower($product->users->address->kota->nama))}}</p>
                    </div>
                    <div>
                        <form action="{{url('cart')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="product_id" value="{{$product->id}}">
                            @if(Auth::check())
                                @if($product->quantity!=0 && Auth::id()!=$product->user_id && Auth::user()->role_id==2)
                                <input type="hidden" name="quantity" value=1>
                                <button class="btn btn-default form-control" role="button">Tambahkan ke keranjang</button>
                                @elseif(Auth::id()==$product->user_id || Auth::user()->role_id==1)
                                <a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}" class="btn btn-default form-control" role="button">Lihat Barang</a>
                                @else
                                <div class="alert alert-danger danger" role="alert">Stok habis</div>
                                @endif
                            @else
                                @if($product->quantity!=0)
                                <input type="hidden" name="quantity" value=1>
                                <button class="btn btn-default form-control" role="button">Tambahkan ke keranjang</button>
                                @else
                                <div class="alert alert-danger danger" role="alert">Stok habis</div>
                                @endif
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </a>
    </div>
@endforeach
</div>