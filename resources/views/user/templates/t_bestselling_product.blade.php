<div class="row slick-slider bestselling">
@foreach($bestselling as $bs)
    <div class="col-sm-2">
        <a href="{{url('p').'/'.$bs->products->product_category->slug.'/'.$bs->products->product_subcategory->slug.'/'.$bs->products->id}}">
            <div class="thumbnail">
                <span class="service-link text-center">
                @if($bs->products->discount!=null && $bs->products->discount->expire > date('Y-m-d H:i:s'))
                    <div class="disc-badge">
                        <div class="disc-content">
                            {{$bs->products->discount->discount}}%
                        </div>
                    </div>
                @endif
                        <img class="show-byk" data-lazy="{{ url('assets/images/products').'/'.$bs->products->user_id.'/'.$bs->products->product_image_first->name }}" alt="">
                    
                </span>
                <div class="caption">
                    <div class="category">
                        <div class="rateit small" data-rateit-value="{{$bs->products->review_rating->avg('rating')}}" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-mode="font"></div>
                        <div class="pull-right">{{$bs->products->review_rating->count()}} ulasan</div>
                    </div>
                    <a class="wrap" href="{{url('p').'/'.$bs->products->product_category->slug.'/'.$bs->products->product_subcategory->slug.'/'.$bs->products->id}}">{{$bs->products->name}}</a>
                    @if($bs->products->discount==null || $bs->products->discount!=null && $bs->products->discount->expire < date('Y-m-d H:i:s'))
                    <strong>Rp{{number_format($bs->products->price,0,',','.')}}</strong>
                    @elseif($bs->products->discount!=null && $bs->products->discount->expire > date('Y-m-d H:i:s'))
                    <strong class="discount">Rp{{number_format($bs->products->price,0,',','.')}}</strong><strong>{{'Rp'.number_format($bs->products->price - (round($bs->products->price * ($bs->products->discount->discount/100))),0,',','.')}}</strong>
                    @endif
                    <div class="store-name">
                        <a href="{{url('u/'.$bs->products->users->username)}}">{{$bs->products->users->user_name->name}}</a>
                        <p><i class="fa fa-map-marker"></i>{{ucwords(strtolower($bs->products->users->address->kota->nama))}}</p>
                    </div>
                    <div>
                        <form action="{{url('cart')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="product_id" value="{{$bs->products->id}}">
                            @if(Auth::check())
                                @if($bs->products->quantity!=0 && Auth::id()!=$bs->products->user_id && Auth::user()->role_id!=1)
                                <input type="hidden" name="quantity" value=1>
                                <button class="btn btn-default form-control" role="button">Tambahkan ke keranjang</button>
                                @elseif(Auth::id()==$bs->products->user_id || Auth::user()->role_id==1)
                                <a href="{{url('p').'/'.$bs->products->product_category->slug.'/'.$bs->products->product_subcategory->slug.'/'.$bs->products->id}}" class="btn btn-default form-control" role="button">Lihat Barang</a>
                                @else
                                <div class="alert alert-danger danger" role="alert">Stok habis</div>
                                @endif
                            @else
                                @if($bs->products->quantity!=0)
                                <input type="hidden" name="quantity" value=1>
                                <button class="btn btn-default form-control" role="button">Tambahkan ke keranjang</button>
                                @else
                                <div class="alert alert-danger danger" role="alert">Stok habis</div>
                                @endif
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </a>
    </div>
@endforeach
</div>