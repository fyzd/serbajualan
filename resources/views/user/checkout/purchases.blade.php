@extends('user.templates.t_index_payment')
@section('title', 'Checkout')

@section('content')

<div class="container" style="padding-top:20px">
    <div class="row">

        <div class="col-sm-12">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    Ringkasan Belanja
                </div>
                <div class="panel panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Total Harga Barang</td>
                                <td>Rp{{number_format($barang,0,',','.')}}</td>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Biaya Kirim</td>
                                <td>Rp{{number_format($ongkir,0,',','.')}}</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total Belanja</th>
                                <th>Rp<label id="total">{{number_format($total,0,',','.')}}</label></th>
                            </tr>
                        </tfoot>
                    </table>
                    <button class="btn btn-success form-control" data-toggle="modal" data-target="#pilih-metode" id="pay-button">Pilih Metode Pembayaran</button>
                </div>
            </div>
        </div>
        
        <div class="col-sm-12">
            <a href="{{url('payment/invoices')}}"><i style="margin-right:5px" class="fa fa-angle-left"></i> Kembali ke data transaksi</a>
        </div>
        <form id="payment-form" method="post" action="snapfinish">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <input type="hidden" name="result_type" id="result-type" value=""></div>
            <input type="hidden" name="result_data" id="result-data" value=""></div>
        </form>
    </div>

    <div class="modal fade" id="pilih-metode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Pilih Metode Pembayaran</h2>
                </div>
                <div class="modal-body">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <form action="{{url('purchase/wallet')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="number" value={{$invoice_number}}>
                            <div class="form-group">
                                <label class="radio-inline">
                                    <input type="radio" name="method" value="dompet">Dompet Virtual
                                </label>
                            </div>
                            <div class="form-group" id="dompet" style="display:none">
                            @if($saldo==null || $saldo->balance<$total)
                            <p>Saldo Dompet Virtual Anda <b>Rp{{$saldo!=null ? number_format($saldo->balance,0,',','.') : '0'}}</b></p>
                            <p>Saldo Dompet Virtual Anda tidak mencukupi untuk membayar transaksi ini. Silakan pilih metode pembayaran lainnya.</p>
                            @else
                            <p>Saldo Dompet Virtual Anda <b>Rp{{number_format($saldo->balance,0,',','.')}}</b></p>
                            <p>Setelah melakukan pembayaran, saldo Dompet Virtual Anda akan menjadi <b>Rp{{number_format($saldo->balance-$total,0,',','.')}}</b></p>
                            @endif
                            </div>
                            <div class="form-group">
                                <label class="radio-inline">
                                    <input type="radio" name="method" value="other">Metode pembayaran lain
                                </label>
                            </div>
                            
                </div>
                <div class="modal-footer">
                    
                            <input type="hidden" name="order_id" value="">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                            <input type="submit" value="Bayar" id="bayar" class="btn btn-success">
                            </form>
                        </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('jsfield')
<script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="VT-client-x6Gqg0ddmgwraCe-"></script>
<script type="text/javascript">

$('#bayar').attr('disabled', true);
$('#pilih-metode').on('hidden.bs.modal', function(){
    $('input[name=method]:checked').attr('checked', false);
    $('#dompet').css('display', 'none');
    $('#bayar').attr('disabled', true);
});

$('input[name=method]').change(function (event) {
    if($(this).val()=='other')
    {
        $('input[name=method]').attr('disabled', true);
        $('#bayar').attr('disabled', true);
        $('#dompet').slideUp();
        $.ajax({
        
        url: '{{url("snaptoken/".$invoice_number)}}',
        cache: false,

        success: function(data) {
            $('#pilih-metode').modal('hide');
            $('input[name=method]').attr('disabled', false);

            //location = data;
            console.log('token = '+data);
            
            var resultType = document.getElementById('result-type');
            var resultData = document.getElementById('result-data');

            function changeResult(type,data){
            $("#result-type").val(type);
            $("#result-data").val(JSON.stringify(data));
            //resultType.innerHTML = type;
            //resultData.innerHTML = JSON.stringify(data);
            }

            snap.pay(data, {
            
            onSuccess: function(result){
                changeResult('success', result);
                console.log(result.status_message);
                console.log(result);
                $("#payment-form").submit();
            },
            onPending: function(result){
                changeResult('pending', result);
                console.log(result.status_message);
                $("#payment-form").submit();
            },
            onError: function(result){
                changeResult('error', result);
                console.log(result.status_message);
                $("#payment-form").submit();
            }
            });
        }
        });
    }
    else if($(this).val()=='dompet')
    {
        $('#dompet').slideDown();
        <?php
        if($saldo!=null){
            if($saldo->balance>=$total){
            ?>
                $('#bayar').attr('disabled', false);
        <?php
        }
    }
        ?>
    }
});
</script>
@endsection