@extends('user.templates.t_index_payment')
@section('title', 'Checkout')

@section('content')

<div class="container" style="padding-top:20px">
    <div class="row">

        <div class="col-sm-8">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <div class="alert alert-danger" id="error"></div>
            
            <div class="panel panel-default">
                <div class="panel-heading">
                @if(Auth::check())
                    <h3 class="panel-title">Detail Pembeli</h3>
                @else
                    Login atau Beli tanpa daftar
                @endif
                </div>
                <div class="panel-body">
                @if(Auth::check())
                

                <div class="panel-group" style="margin:0">

                    <div class="panel panel-default address">
                        <div class="panel-heading">
                            <div class="btn-group pull-right" style="margin-top:-8px;">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                    Kirim ke alamat lain <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <form action="{{url('payment/purchases/new')}}" method="post" id="pur">
                                    @php ($i=1)
                                    @foreach($alamat as $address)
                                        <li class="radio">
                                            <label class="radio-inline" style="padding-left:20px">
                                            <input type="radio" name="address" value="{{$address->id}}" class="{{$address->id}}" {{$i==1 ? 'checked' : ''}}> {{$address->name}}
                                            </label>
                                        </li>
                                    @php ($i++)
                                    @endforeach
                                    </form>
                                </ul>
                            </div>
                            <b>Alamat Pengiriman</b>
                        </div>
                        @php ($a=1)
                        @foreach($alamat as $address)
                        <div class="panel-collapse collapse {{$address->id}} {{$a==1 ? 'in' : ''}}">
                            <div class="panel-body">
                                {{$address->name}}<br>
                                {{$address->pp}}<br>
                                {{$address->address}} <br>
                                Kecamatan {{$address['kecamatan']->nama}}, {{ucwords(strtolower($address['kota']->nama))}}<br>
                                {{$address['provinsi']->nama}}, {{$address->zip_code}}<br>
                                {{$address->phone}} 
                            </div>
                        </div>
                        @php ($a++)
                        @endforeach
                    </div>
                </div>
                @else
                    <form action="{{url('checkout/login')}}" method="post">
                        {{csrf_field()}}
                        Email <input type="email" name="email" class="form-control"><br>
                        Password <input type="password" name="password" class="form-control">

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                @endif
                    
                </div>
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail Belanja</h3>
                </div>
                <div class="panel-body">
                
                @foreach($ongkir as $order)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>{{$order->users->user_name->name}}</b>
                    </div>
                    <div class="panel-body">
                        @foreach($order->productsOnCart as $product)
                        <div class="row">
                            <div class="col-sm-2">
                                <a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}"><img style="border: 1px solid #eee;"src="{{url('assets/images/products').'/'.$order->user_id.'/'.$product->product_image_first->name}}" alt="" width="60" height="60"></a>
                            </div>
                            <div class="col-sm-7">
                                <label class="pcart"><a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}">{{$product->name}}</a></label>
                                <form action="{{url('cart').'/'.$product->id}}" method="post" id="qty-{{$product->id}}">
                                    {{csrf_field()}}
                                    {{method_field('put')}}
                                    <div class="col-sm-4 row">
                                        <input type="number" class="form-control" min="1" max="{{$product->quantity}}" name="quantity" id="{{$product->id}}" value="{{$product->cart->quantity}}">
                                    </div>
                                    
                                </form>

                            </div>
                            <div class="col-sm-3">
                                <label class="pull-right">Rp{{number_format($product->price_after_disc*$product->cart->quantity,0,',','.')}}</label>
                            </div>
                        </div>
                        @endforeach
                        <hr>
                        <div class="row">
                            <div class="form-group form-horizontal">
                                <p class="control-label col-sm-2">Kurir</p>
                                <div class="col-sm-7">
                                    <select name="kurir" class="kurir form-control" id="kurir-{{$order->id}}">
                                    @foreach($order->ongkir as $o)
                                        @foreach($o as $a)
                                            @if(count($a['costs']) == 0)
                                                <option value="">Maaf, kurir {{$a['code']}} tidak tersedia di daerah Anda</option>
                                            @else
                                                @foreach($a['costs'] as $k)
                                                    @foreach($k['cost'] as $d)
                                                    <option value="{{$order->id}}-{{strtoupper($a['code'])}} {{$k['service']}}-{{$d['value']}}">{{strtoupper($a['code'])}} {{$k['service']}} {{$d['etd']}} hari Rp{{number_format($d['value'],0,',','.')}} </option>
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endforeach
                                    
                                    </select>
                                </div>
                                <div class="col-sm-3" style="text-align:right">
                                    <p style="display:inline">Rp<p style="display:inline" id="{{$order->id}}ongkir">{{number_format($order->costongkir,0,',','.')}}</p></p>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                       
                    </div>
                        
                    </div>
                @endforeach

                

                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ringkasan Belanja
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Total Harga Barang</td>
                                <td></td>
                                <td>Rp{{number_format($total['belanja'],0,',','.')}}</td>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Biaya Kirim</td>
                                <td></td>
                                <td>Rp<span id="tongkir">{{number_format($total['ongkir'],0,',','.')}}</span></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total Belanja</th>
                                <th></th>
                                <th>Rp<label id="total">{{number_format($total['total'],0,',','.')}}</label></th>
                            </tr>
                        </tfoot>
                    </table>
                    <button id="purchases" class="btn btn-success form-control">Pilih Metode Pembayaran</button>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection

@section('jsfield')

<script type="text/javascript">

$(document).ready(function () {

    $('.kurir').on("change", function(event){
        
        var kurir=$(this).val();
        var token='{{csrf_token()}}';
        $.ajax({
            url  : '{{url("/change/courier")}}',
            type : 'POST',
            data : {kurir:kurir, _token:token},

            success: function(data) { 
                console.log(data);
                $('#'+data[0]+'ongkir').html(data[2].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                $('#tongkir').html(data[3].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                $('#total').html(data[4].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            }
        });    
    });

    $(window).on("beforeunload", function() {
        $('#purchases').attr('disabled', true);
        $('select[name=kurir]').attr('disabled', true);
        $.ajax({
            url : "{{url('checkout/delete_session')}}",
            type: 'POST',
            async: false,
            data: {_token:'{{csrf_token()}}'},
            success: function(event) {
            }
        
        });
    });
        

    $('#purchases').on("click", function(event){
        event.preventDefault();
        $(this).prop('disabled', true);
        
        $.ajax({
            url : $('#pur').attr('action'),
            type : 'post',
            data : {_token:'{{csrf_token()}}', address:$('input[name=address]:checked').val()},
            success : function(data){
                
                if($.isEmptyObject(data.error)){
                    $("#error").css('display', 'none');
                    $(location).attr('href','{{url("payment/purchases")}}'+'/'+data);
                }else{
                    $('#purchases').prop('disabled', false);
                    $("#error").css('display', 'block');
                    $("#error").text(data.error.address); 
                }
            }
        });
        //$(window).unbind('beforeunload');
    });
    
    $('input[type=radio][name=address]').on('change', function () {
        if (!this.checked) return
        radio=$(this);
        $('.kurir').attr('disabled', true);
        $('#purchases').attr('disabled', true);
        $('.collapse').not($('div.' + $(this).attr('class'))).slideUp();

        $.ajax({
            url : "{{url('change/address')}}",
            type: "post",
            data: {_token:'{{csrf_token()}}', address:$('input[name=address]:checked').val()},
            dataType: 'json',
            success: function(data){
                $('.kurir').attr('disabled', false);
                $('#purchases').attr('disabled', false);

                $('.collapse.' + radio.attr('class')).slideDown();

                $.each(data[0], function(k1, o) { 
                    select=$('#kurir-'+o.id);
                    select.find('option').remove();
                    $.each(o.ongkir, function(k2, a) { 
                        if(a.costs=='null'){
                            $('<option>').val('').text('Maaf, kurir '+a.value+' tidak tersedia di daerah Anda').appendTo(select);
                        }
                        else
                        {
                            $.each(a[0].costs, function(k3, k) { 
                                $.each(k.cost, function(k4, d) { 
                                    $('<option>').val(o.id+'-'+a[0].code.toUpperCase()+' '+k.service+'-'+parseInt(d.value)).text(a[0].code.toUpperCase()+' '+k.service+' '+d.etd+' hari Rp'+parseInt(d.value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")).appendTo(select);

                                    
                                });
                            });
                        }
                    });
                    $('#'+o.id+'ongkir').html(parseInt(o.ongkir[0][0].costs[0].cost[0].value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                });

                
                $('#tongkir').html(data[1].ongkir.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                $('#total').html(data[1].total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
 
            }
        });
    });

    $('input[name=quantity]').on('change', function() {
        id = $(this).attr('id');

        $('#qty-'+id).attr('action', '{{url("cart")}}/'+id).submit();
    });
});
</script>
@endsection