@extends('user.templates.t_index') @section('title', 'Cart') 
@section('content')
<section class="desktop-bar bar-show">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('')}}">Halaman depan</a></li>
                    <li class="active"><a>Keranjang</a>
                    </li>
                </ol>
                <nav class="navbar navbar-default desk-nav">
                        <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <span class="navbar-brand nav-kat">Keranjang Belanja</span>
                    </div>

                    <div class="collapse navbar-collapse" id="desktop"></div>
                </nav>
                
            </div>
        </div>
    </div>
</section>

<section id="cart_items">
    <div class="container">
        <div class="row">
        @if ($cart->isEmpty())
        <div class="col-sm-12" style="margin-bottom:40px">
            <h1 class="text-center">Belum ada barang di keranjang belanja Anda</h1>
        </div>
        @else
            <div class="col-sm-8">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @foreach($cart->details() as $order)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$order->users->user_name->name}}</h3>
                    </div>
                
                    <div class="panel-body" style="border-bottom: 1px solid #eee !important;">
                    @if(Auth::check())
                    @php($each=$order->productsOnCart)
                    @else
                    @php($each=$order->products)
                    @endif
                    @foreach($each as $product)
                        <div class="row keranjang-cart">
                            <div class="col-sm-2 col-xs-2">
                                <a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}"><img style="border: 1px solid #eee;"src="{{url('assets/images/products').'/'.$order->user_id.'/'.$product->product_image_first->name}}" alt="" width="60" height="60"></a>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <label class="pcart"><a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}">{{$product->name}}</a></label>
                                <form action="{{url('cart').'/'.$product->id}}" method="post" id="qty-{{$product->id}}">
                                    {{csrf_field()}}
                                    {{method_field('put')}}
                                    <div class="col-sm-4 row">
                                        <input type="number" class="form-control" name="quantity" min="1" max="{{$product->quantity}}" id="{{$product->id}}" value="{{Auth::check() ? $product->cart->quantity : $product->cookie_qty}}">
                                    </div>
                                    
                                </form>

                            </div>
                            <div class="col-sm-3 col-xs-4">
                                <label class="pull-right">Rp{{Auth::check() ? number_format($product->price_after_disc*$product->cart->quantity,0,',','.') : number_format($product->price_after_disc*$product->cookie_qty,0,',','.')}}</label>
                            </div>
                            <div class="col-sm-1 col-xs-4">
                            <form action="{{url('cart').'/'.$product->id}}" method="post" style="display: inline">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="delete">
                                <button class="btn btn-danger" href=""><i class="fa fa-times"></i></button>
                            </form>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="panel-body">
                        <span class="ltl">Belum termasuk biaya kirim</span>
                    </div>
                </div>
                
                @endforeach
            
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pembayaran</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                Total Harga Barang
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <label class="pull-right">Rp{{number_format($cart->totalPrice(),0,',','.')}}</label>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="{{url('payment/purchases/new')}}" class="btn btn-success form-control">Bayar</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        @endif
        </div>
    </div>
</section>
<!--/#cart_items-->


@endsection

@section('jsfield')
<script type="text/javascript">
    $('input[name=quantity]').on('change', function() {
        id = $(this).attr('id');

        $('#qty-'+id).attr('action', '{{url("cart")}}/'+id).submit();
    });
</script>
@endsection