@extends('user.templates.t_index')
@section('title', 'My Product')
@section('cssfield')
<link href="{{URL::asset('assets/css2/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/css2/dataTables.checkboxes.css')}}" rel="stylesheet">
@endsection

@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li class="active"><a>Daftar Barang</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="col-sm-12 row">
                    <h4>Daftar Barang</h4>
                    <div class="panel panel-default">
                        <div class="panel-body">
                                
                            <table class="table table-condensed" id="users-table">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                                        <th width="80px">Dijual pada</th>
                                        <th width="380px">Nama Barang</th>
                                        <th>Subkategori</th>
                                        <th>Stok</th>
                                        <th>Gambar</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                            </table>
                            <button class="btn btn-danger" id="hapus"><i class="fa fa-trash"></i> Hapus Barang</button>
                        </div>
                    </div>
                </div>
                
            </div>
                <div id="panel"></div>

        </div>
    </div>
</div>


@endsection

@section('jsfield')
<script src="{{URL::asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('assets/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('assets/js/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/js/dataTables.checkboxes.min.js')}}"></script>
<script>
$(function() {
    var table=$('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('datatables.data') !!}',
            type: 'POST',
            data: { _token: '{{csrf_token()}}'}
        },
        columns: [
            { data: 'id', name: 'id', 'searchable': false },
            { data: 'created_at', name: 'created_at' },
            { data: 'name', name: 'name' },
            { data: 'subcat', name: 'subcategory', 'searchable': false },
            { data: 'qty', name: 'quantity' },
            { data: 'image', name: 'image', 'searchable': false, 'sortable': false, render:  
                function ( data, url, type, full) {
                    var url='{{asset("assets/images/products")}}/{{Auth::id()}}/'+data;
                    return '<img src="'+url+'" width=50 >';
                },
            },
            { 'searchable': false, 'sortable': false, data:  
                function ( data, url, type, full) {
                    var url='{{url("p")}}/'+data.cat+'/'+data.subslug+'/'+data.id;
                    var url2='{{url("my_products/edit")}}/'+data.id;
                    return '<a class="btn btn-primary" href="'+url+'" data-toggle="tooltip" title="Lihat Barang"><i class="fa fa-eye"></i></a> <a class="btn btn-success" href="'+url2+'" data-toggle="tooltip" title="Edit Barang"><i class="fa fa-edit"></i></a>';
                }  
            },
        ],
        columnDefs: [
                {
                    targets: 0,
                    checkboxes: {
                        selectRow: true
                    }
                }
            ],
            select:{
                style: 'multi'
            },
            order: [[1, 'desc']]
        
    });
    $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.api().rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

    $('#users-table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;

            
         }
      }
   });
   
   $('#hapus').on("click", function(event){
       var data = table.rows(' .selected ').data();
       if(data.count()>0)
       {
        confirm=confirm('Anda yakin ingin menghapus barang yang telah dipilih?');
        if(confirm==true)
        {
            $.each(data, function() {
                var key = Object.keys(this)[0];
                var value = this[key];
                var user_id={{Auth::id()}};
    
                $.ajax({
                    type: "POST",
                    url: "{{ url('my_products/delete') }}",
                    data: {
                        "_method": "DELETE",
                        "_token": "{{ csrf_token() }}",
                        "id": value,
                        "user_id": user_id
                    },
                    success: function(result) {
                        $('#users-table').DataTable().ajax.reload();
                        alert('Barang berhasil dihapus.')
                    }
                });
            });
        }
        
       }
       else
       {
           alert('Pilih terlebih dahulu barang yang ingin dihapus.');
       }
       
    });

});
</script>
@endsection