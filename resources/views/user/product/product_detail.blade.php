@extends('user.templates.t_index')
@section('title', 'Product Details') 
@section('cssfield')
<link href="{{URL::asset('assets/css/smoothproducts.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/css2/style2.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/css2/xzoom.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet"> 
<link href="{{URL::asset('assets/css2/rateit.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/css2/slick.css')}}" rel="stylesheet">
@endsection 
@section('content')

<!-- desktop bar -->
<section class="desktop-bar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb" style="padding:1em 0">
                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                    <li><a href="{{url('c').'/'.strtolower($product->product_category->slug)}}">{{$product->product_category->description}}</a></li>
                    <li><a href="{{url('c').'/'.strtolower($product->product_category->slug.'/'.$product->product_subcategory->slug)}}">{{$product->product_subcategory->description}}</a></li>
                    <li class="active" style="text-transform:capitalize"><a>{{$product->name}}</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="desk-com">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <div class="row d-product">
                            <div class="col-sm-5">
                                <div class="xzoom-container">
                                @if($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                                    <div class="disc-badge" style="margin-right:15px">
                                        <div class="disc-content">
                                            {{$product->discount->discount}}%
                                        </div>
                                    </div>
                                @endif
                                    <img class="xzoom" id="xzoom-fancy" src="{{url('assets/images/products').'/'.$product->user_id.'/'.$product->product_image[0]->name}}"
                                        xoriginal="{{url('assets/images/products').'/'.$product->user_id.'/'.$product->product_image[0]->name}}"
                                        width="100%" />
                                    <div class="xzoom-thumbs" style="padding-top:10px">
                                        <a href="{{url('assets/images/products').'/'.$product->user_id.'/'.$product->product_image[0]->name}}"><img class="xzoom-gallery" width="67" height="67" src="{{url('assets/images/products').'/'.$product->user_id.'/'.$product->product_image[0]->name}}"  xpreview="{{url('assets/images/products').'/'.$product->user_id.'/'.$product->product_image[0]->name}}"></a>
                                        @foreach ($product->product_image->slice(1) as $image)
                                        <a href="{{url('assets/images/products').'/'.$product->user_id.'/'.$image->name}}"><img class="xzoom-gallery" width="67" height="67" src="{{url('assets/images/products').'/'.$product->user_id.'/'.$image->name}}"></a>
                                        @endforeach
                                    </div>
                                </div>


                            </div>
                            <div class="col-sm-7">

                                <div class="pro-details">
                                    <h2><b>{{$product->name}}</b></h2>
                                    <div class="list-inline rate-stock">
                                        <div class="rateit" data-rateit-value="{{$product->reviews->avg('rating')}}" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-mode="font"></div>
                                        <span class="ulasan">{{$product->reviews->count()}} Ulasan</span>
                                    </div>
                                    
                                    <div class="currency">
                                        @if($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                                        <strong class="discountdet">Rp{{number_format($product->price,0,',','.')}}</strong>
                                        <h1 style="font-size:34px; color:black;margin-top:0"><b style="color:#d71149">Rp{{number_format($product->price_after_disc,0,',','.')}}</b></h1>
                                        @else
                                        <h1 style="font-size:34px; color:black;margin-top:0"><b>Rp{{number_format($product->price,0,',','.')}}</b></h1>
                                        @endif
                                    </div>
                                    <br>

                                    <div class="to-cart">
                                    @if(Auth::check())
                                        @if($product->quantity!=0 && Auth::id()!=$product->user_id && Auth::user()->role_id==2)
                                            <label>Tersedia <span style="color:#8ec63f">{{$product->quantity}}</span> stok barang</label>
                                            <p>Masukkan jumlah yang diinginkan</p>
                                            <form action="{{url('cart')}}" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <div class="col-sm-4 row">
                                                    <div class="form-group">
                                                        <input type="number" name="quantity" min="1" max="{{$product->quantity}}" value="1" class="form-control text-center"/>
                                                    </div>
                                                    
                                                </div>
                                                
                                                <div class="add-cart"><button type="submit" class="btn btn-default form-control">Tambahkan ke keranjang</button></div>
                                            </form>
                                        @elseif(Auth::id()==$product->user_id)
                                            <p>Uang hasil penjualan yang akan diterima (belum termasuk biaya pengiriman) setelah dipotong komisi penjualan. <a href="" class="link">Apa itu komisi penjualan?</a></p>
                                            <h3>Rp{{number_format($harga,0,',','.')}}</h3>
                                            <div class="add-cart"><a href="{{url('my_products/edit').'/'.$product->id}}" class="btn btn-default form-control">Edit Barang</a></div>
                                        @elseif(Auth::user()->role_id==1)
                                            <form method="post" action="{{url('admin/products/delete')}}">
                                                {{csrf_field()}}
                                                {{method_field('delete')}}
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <input type="hidden" name="user_id" value="{{$product->user_id}}">
                                                <input type="submit" style="background-color:#d9534f"class="btn btn-danger form-control" value="Hapus Barang">
                                            </form>
                                        @else
                                            <h2>Stok habis</h2>
                                        @endif
                                    @else
                                        @if($product->quantity!=0)
                                            <label>Tersedia <span style="color:#8ec63f">{{$product->quantity}}</span> stok barang</label>
                                            <p>Masukkan jumlah yang diinginkan</p>
                                            <form action="{{url('cart')}}" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <div class="col-sm-4 row">
                                                    <div class="form-group">
                                                        <input type="number" name="quantity" min="1" max="{{$product->quantity}}" value="1" class="form-control text-center"/>
                                                    </div>
                                                    
                                                </div>
                                                
                                                <div class="add-cart"><button type="submit" class="btn btn-default form-control">Tambahkan ke keranjang</button></div>
                                            </form>
                                        @else
                                            <h2>Stok habis</h2>
                                        @endif
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                        <div class="row description detail">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#description" aria-controls="home" role="tab" data-toggle="tab">Detail Barang</a></li>
                                <li role="presentation"><a href="#estimated-cost" aria-controls="profile" role="tab" data-toggle="tab">Estimasi Biaya Kirim</a></li>
                                <li role="presentation"><a href="#reviews" aria-controls="messages" role="tab" data-toggle="tab">{{$product->reviews->count()}} Ulasan Barang</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="description">
                                    <div class="i-produk">
                                        <div class="i-produk-judul">
                                            Informasi
                                        </div>
                                        <div class="i-produk-kiri" style="margin-right:130px">
                                            <ul class="ul-samping">
                                                <li>
                                                    <div class="awl-desc"><i class="fa fa-exchange"></i>Kondisi</div>{{$product->condition}}
                                                </li>
                                                <li>
                                                    <div class="awl-desc"><i class="fa fa-cube"></i>Berat</div>{{$product->weight}}gr
                                                </li>
                                                <li>
                                                    <div class="awl-desc"><i class="fa fa-eye"></i>Dilihat</div>{{$product->visitor_count}}
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="i-produk-kiri">
                                            <ul class="ul-samping">
                                                <li>
                                                    <div class="awl-desc"><i class="fa fa-shopping-cart"></i>Terjual</div>{{$product->sold_count}}
                                                </li>
                                                <li>
                                                    <div class="awl-desc"><i class="fa fa-clock-o"></i>Diperbarui</div>{{$product->updated_at->diffForHumans()}}
                                                </li>
                                            </ul>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div><hr>
                                    <div class="i-produk">
                                        <div class="i-produk-judul">
                                            Deskripsi
                                        </div>
                                        <div class="i-produk-kiri">
                                            <ul class="ul-samping">
                                                <li>
                                                    {!!$product->description!!}
                                                </li>
                                            </ul>
                                            
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                    @if($product->stores->seller_note!=null)
                                    <hr>
                                    <div class="i-produk">
                                        <div class="i-produk-judul">
                                            Catatan
                                        </div>
                                        <div class="i-produk-kiri">
                                            <ul class="ul-samping">
                                                <li>
                                                    <p class="ltl">Catatan Pelapak tetap tunduk terhadap Aturan penggunaan Serbajualan.</p><br>
                                                    {!!$product->stores->seller_note!!}
                                                </li>
                                            </ul>
                                            
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="estimated-cost">
                                    Loading...
                                </div>
                                <div role="tabpanel" class="tab-pane" id="reviews">
                                @foreach($product->reviews as $review)
                                    <div class="row">
                                        <div class="col-sm-1 col-xs-2">
                                            <div class="img-rvw">
                                                
                                                <img class="img-responsive" src="{{$review->users->user_image->image!=null ? url('assets/images/profile').'/'.$review->users->user_image->image : url('assets/images/pp-default.png')}}">
                                            </div>
                                        </div>
                                        <div class="col-sm-11 col-xs-10 review">
                                            
                                            @for($i=1;$i<=5;$i++)
                                                <i class="fa fa-star {{$i<=$review->rating ? 'checked' : ''}}"></i>
                                            @endfor
                                            <p class="reviewer">Oleh {{$review->users->user_name->name}}, <span style="margin-left:5px">{{date('d F Y', strtotime($review->updated_at))}}, pukul {{date('H:i', strtotime($review->updated_at))}} WIB</span></p>
                                            <p style="color:#333">{{$review->comment}}</p>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                                </div>
                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 info-seller">
                <div class="p-info">
                    <p class="big">Penjual</p>
                    <div class="p-pic">
                        <img src="{{$product->users->user_info->image!=null ? url('assets/images/profile').'/'.$product->users->user_info->image : url('assets/images/pp-default.png')}}" width="60" height="60" style="border-radius:50%">
                    </div>
                    <div class="p-name">
                        <label style="font-size:16px"><a href="{{url('u').'/'.$product->users->username}}">{{$product->users->user_info->name}}</a></label>
                        <p class="ltl ltl2"><i class="fa fa-map-marker" style="margin-right:5px"></i> {{ucwords(strtolower($product->users->address->kota->nama))}}</p>
                    </div>
                    
                </div>
                <hr>
                <div class="p-detail">
                    <table class="table borderless">
                        <tr>
                            <td>Pelanggan</td>
                            <td>{{$customer}} orang</td>
                        </tr>
                        <tr>
                            <td>Pesanan diterima</td>
                            <td>
                                @if($semua>0)
                                    Menerima {{$terima}} dari {{$semua}} ({{intval($terima/$semua*100)}}%)
                                @else
                                    Tidak pernah menolak pesanan
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Bergabung</td>
                            <td>{{date('d F Y', strtotime($product->users->created_at))}}</td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="p-shipping">
                    <p class="big">Pengiriman</p>
                    <table class="table borderless text-center">
                        <tbody>
                            <tr>
                                <td><span class="sp-courier icon-jne" title="JNE"></span></td>
                                <td>{!!$product->users->courier->jne==1 ? '<i class="fa fa-check">' : '<i class="fa fa-close">'!!}</i></td>
                            </tr>
                            <tr>
                                <td><span class="sp-courier icon-tiki" title="TIKI"></span></td>
                                <td>{!!$product->users->courier->tiki==1 ? '<i class="fa fa-check">' : '<i class="fa fa-close">'!!}</td>
                            </tr>
                            <tr>
                                <td><span class="sp-courier icon-pos" title="POS"></span></td>
                                <td>{!!$product->users->courier->pos==1 ? '<i class="fa fa-check">' : '<i class="fa fa-close">'!!}</td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
                <div class="p-look">
                    <a href="{{url('u').'/'.$product->users->username}}" class="btn btn-default form-control">Semua Barang</a>
                </div>
            </div>
        </div>

    </div>

    <!-- related product  -->
    <section class="related-product sect">
            <div class="container">
                <div class="row" >
                    <div class="col-sm-12">
                        <div class="">
                            <h1><span class="t-color-3">Barang</span> Terkait</h1>
                            <div class="heading-border b-color-3"></div>
                        </div>
                    </div>
                </div> <!-- section title -->
                <div class="carousel slide">
                    <ol class="carousel-indicators">
                            <li class="active" id="rprev"><i class="fa fa-angle-left"></i></li>
                            <li id="rnext"><i class="fa fa-angle-right"></i></li>
                    </ol>
                    <div class="outer-related">
                        @include('user.templates.t_related_product')
                    </div>
                </div>
            </div>
        </section>

</section>

@endsection 

@section('jsfield')
<script src="{{URL::asset('assets/js/xzoom.min.js')}}"></script>
<script src="{{URL::asset('assets/fancybox/source/jquery.fancybox.js')}}"></script>
<script src="{{URL::asset('assets/js/hammer.js/1.0.5/jquery.hammer.min.js')}}"></script>
<script src="{{URL::asset('assets/js/slick.min.js')}}"></script>

<script type="text/javascript">
    $('.xzoom, .xzoom-gallery').xzoom({
        tint: '#006699',
        Xoffset: 15
    });

    //Integration with hammer.js
    var isTouchSupported = 'ontouchstart' in window;

    if (isTouchSupported) {
        //If touch device
        $('.xzoom').each(function () {
            var xzoom = $(this).data('xzoom');
            xzoom.eventunbind();
        });

        $('.xzoom').each(function () {
            var xzoom = $(this).data('xzoom');
            $(this).hammer().on("tap", function (event) {
                event.pageX = event.gesture.center.pageX;
                event.pageY = event.gesture.center.pageY;
                var s = 1,
                    ls;

                xzoom.eventmove = function (element) {
                    element.hammer().on('drag', function (event) {
                        event.pageX = event.gesture.center.pageX;
                        event.pageY = event.gesture.center.pageY;
                        xzoom.movezoom(event);
                        event.gesture.preventDefault();
                    });
                }

                var counter = 0;
                xzoom.eventclick = function (element) {
                    element.hammer().on('tap', function () {
                        counter++;
                        if (counter == 1) setTimeout(openfancy, 300);
                        event.gesture.preventDefault();
                    });
                }

                function openfancy() {
                    if (counter == 2) {
                        xzoom.closezoom();
                        $.fancybox.open(xzoom.gallery().cgallery);
                    } else {
                        xzoom.closezoom();
                    }
                    counter = 0;
                }
                xzoom.openzoom(event);
            });
        });

    } else {
        //If not touch device

        //Integration with fancybox plugin
        $('#xzoom-fancy').bind('click', function (event) {
            var xzoom = $(this).data('xzoom');
            xzoom.closezoom();
            $.fancybox.open(xzoom.gallery().cgallery, {
                padding: 0,
                helpers: {
                    overlay: {
                        locked: false
                    }
                }
            });
            event.preventDefault();
        });
    }

    $("a[href='#estimated-cost']").on('shown.bs.tab', function(){
        var id={{$product->id}};
        var price={{$product->price_after_disc}};
        var weight={{$product->weight}};
        var asal='{{$product->users->address->kota->nama}}';
        var kurir={jne:{{$product->users->courier->jne}},tiki:{{$product->users->courier->tiki}},pos:{{$product->users->courier->pos}}};
        var token='{{csrf_token()}}';
        $.ajax({
            url : '{{url("estimated_cost")}}',
            type : 'post',
            data: {_token:token, product_id:id, weight:weight, kota_asal:asal, kurir:kurir, price:price},
            error: function(data){
                alert("There was a problem");
            },
            success: function(data){
                $('#estimated-cost').html(data);
            }
        });
    });

    $('.slick-slider').slick({
        arrows:false,
        slidesToShow: 6,
        slidesToScroll: 2,
        lazyLoad: 'ondemand',
        responsive: [
            {
            breakpoint: 481,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            }
        ]
    });

    $('#rnext').click(function(){
        $('.related').slick('slickNext');
        $('#rnext').attr('class', 'active');
        $('#rprev').attr('class', '');
    });
    $('#rprev').click(function(){
        $('.related').slick('slickPrev');
        $('#rprev').attr('class', 'active');
        $('#rnext').attr('class', '');
    });
</script>
<script src="{{URL::asset('assets/js/jquery.rateit.min.js')}}"></script>

@endsection