@extends('user.templates.t_index')
@section('title', 'Update Product')
@section('cssfield')
<link href="{{URL::asset('assets/css2/dropzone.min.css')}}" rel="stylesheet">
@endsection
@section('content')


<div class="setting-field">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('user.templates.t_user_setting_category')
            </div>
            
            <div class="col-sm-9 padding-right set">
                <section class="desktop-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 row">
                                <ol class="breadcrumb">
                                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                                    <li><a href="{{url('my_products')}}">Daftar Barang</a></li>
                                    <li class="active"><a>Update Barang</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="col-sm-12 row">
                    <h4>Update Barang</h4>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{url('my_products/update/').'/'.$item->id }}" method="post" id="product" class="form-horizontal">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tentukan Barang Jualan</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Nama Barang</p>
                                        
                                    <div class="col-sm-9">
                                        <input type="text" name="name" value="{{$item->name}}" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Kategori</p>
                                        
                                    <div class="col-sm-9">
                                        <select name="category" class="form-control" id="kategori">
                                            @foreach ($categories as $category)
                                                <option value="{{$category->id}}" {{$category->id==$item->product_category->id ?'selected' : ''}}>{{$category->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Subkategori</p>
                                    <div class="col-sm-9">
                                        <select name="subcategory" class="form-control" id="subkategori">
                                            @foreach ($subcategories as $subcategory)
                                                <option value="{{$subcategory->id}}" {{$subcategory->id==$item->product_subcategory->id ?'selected' : ''}}>{{$subcategory->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tentukan Deskripsi Barang</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Kondisi</p>
                                    <div class="col-sm-9">
                                        <label class="radio-inline"><input type="radio" name="condition" value="baru" {{$item->condition=='baru' ? 'checked' : ''}}>Baru</label>
                                        <label class="radio-inline"><input type="radio" name="condition" value="bekas" {{$item->condition=='bekas' ? 'checked' : ''}}>Bekas</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Perkiraan berat</p>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" name="weight" value="{{$item->weight}}" class="form-control"> 
                                            <span class="input-group-addon" id="addon">gr</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Stok</p>
                                    <div class="col-sm-2">
                                        <input type="number" name="quantity" value="{{$item->quantity}}" class="form-control"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Harga satuan</p>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="addon">Rp</span>
                                            <input type="text" name="price" value="{{$item->price}}" class="form-control" aria-describedby="addon">
                                        </div> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Uang yang akan diterima *)</p>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="addon">Rp</span>
                                            <input type="text" id="potong" class="form-control" aria-describedby="addon" disabled>
                                        </div> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Diskon</p>
                                    <div class="col-sm-6">
                                        
                                        <div class="panel-group" style="margin:0">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <p>Tambahkan diskon?</p>
                                                        <label class="radio-inline"><input name="collapseGroup" type="radio" value="ya" data-toggle="collapse" data-target="#discount:not(.in)" {{$item->discount_expire!=null ? 'checked' : ''}}/> Ya</label>

                                                        <label class="radio-inline"><input name="collapseGroup" type="radio" value="tidak" data-toggle="collapse"  data-target="#discount.in" {{$item->discount_expire==null ? 'checked' : ''}}/> Tidak</label>
                                                        
                                                    </h4>
                                                </div>
                                                <div id="discount" class="panel-collapse collapse {{$item->discount_expire!=null ? 'in' : ''}}">
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <p class="control-label col-sm-3">Diskon</p>
                                                        <div class="col-sm-5">
                                                            <div class="input-group">
                                                                <input type="text" name="discount" value="{{$item->discount_expire!=null ? $item->discount_expire->discount : old('discount')}}" class="form-control" aria-describedby="addon" {{$item->discount_expire==null ? 'disabled' : ''}}>
                                                                <span class="input-group-addon" id="addon">%</span>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <p class="control-label col-sm-3">Hingga</p>
                                                        <div class="col-sm-9">
                                                            <div class="input-group">
                                                                <input type="datetime-local" name="expire" value="{{$item->discount_expire!=null ? date('Y-m-d\TH:i:s', strtotime($item->discount_expire->expire)) : old('discount')}}" class="form-control" {{$item->discount_expire==null ? 'disabled' : ''}}>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Gambar</p>
                                    <div class="col-sm-9">
                                        <div class="dropzone" id="dropzoneFileUpload">
                                        </div> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="control-label col-sm-2">Deskripsi</p>
                                    <div class="col-sm-9">
                                        <textarea rows="10" class="form-control" name="description">{{$item->description}}</textarea>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <p>*) Uang hasil penjualan yang akan diterima (belum termasuk biaya pengiriman dan diskon jika ada) setelah dipotong komisi penjualan. <a href="" class="link">Apa itu komisi penjualan?</a></p> 
                                </div>
                                <button type="submit" id="submit" class="btn btn-success pull-right col-sm-5">Simpan</button>
                                
                                
                            </div>
                            
                        </div>
                        
                    </form>
                </div>
                
                
            </div>
        </div>
    </div>
</div>


@endsection

@section('jsfield')
<script src="{{URL::asset('assets/js/dropzone.min.js')}}"></script>
<script src="{{URL::asset('assets/js/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
    var baseUrl = "{{ url('/') }}";
    var token = "{{ Session::token() }}";
    var user_id="{{Auth::user()->id}}";
    var product_id="{{request()->route()->product_id}}";
    var maxImageWidth = 1000;
    var maxImageHeight = 1000;
    
    Dropzone.autoDiscover = false;

    var myDropzone = new Dropzone("div#dropzoneFileUpload", { 
        
        url: baseUrl+"/dropzone/store",
        params: {
        _token: token
        },
        addRemoveLinks: true,
        maxFilesize: 2,
        init:function() {


            $.get('{{url("my_products/server-images")}}/'+product_id, function(data) {

                $.each(data.images, function (key, value) {

                    var mockFile = {name: value.name, size: value.size};
                    myDropzone.emit("addedfile", mockFile);
                    myDropzone.files.push(mockFile);
                    myDropzone.createThumbnailFromUrl(mockFile, '{{url("assets/images/products/")}}/' +user_id+ '/' + value.name);
                    myDropzone.emit("complete", mockFile);
                });
            });

            this.on("thumbnail", function(file) {
                if (file.rejectDimensions !== undefined || file.acceptDimensions !== undefined) {
                    // Do the dimension checks you want to do
                    if (file.width > maxImageWidth || file.height > maxImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                }
            });

        
        },
        accept: function(file, done) {
            file.acceptDimensions = done;
            file.rejectDimensions = function() { done("Invalid dimension."); };
            // Of course you could also just put the `done` function in the file
            // and call it either with or without error in the `thumbnail` event
            // callback, but I think that this is cleaner.
        }

    });

    myDropzone.on("addedfile", function(file) {
        if (this.files[5]!=null){
            this.removeFile(this.files[4]);
        }
    });

    myDropzone.on("removedfile", function(file) {
        var name = file.previewElement.id;
        $.ajax({
            type: 'POST',
            url: "{{url('dropzone/delete')}}",
            data: {name: name, oldImage: file.name, product_id:product_id, _token:token},
            dataType: 'html',
            success: function (data) {
                
            }
        })
        
    });
    
    myDropzone.on("success", function(file,response){
        var fileName = response.newName //server return fileName
        var aa = file.previewElement.querySelector("[data-dz-name]");
        aa.dataset.dzName = fileName;

        aa.innerHTML = fileName;

        file.previewElement.id = fileName;
        console.log(fileName);
    });

    $(window).on("beforeunload", function() {
        
        $.ajax("{{url('dropzone/delete')}}", {
            type: 'POST',
                    data: {_token:token},
                    dataType: 'html',
            async: false,
            success: function(event) {
                console.log("Ajax request executed");
            }
        });
        return "This is a jQuery version";
        
    });
        
    $('#product').submit(function(e) {
        if(myDropzone.files.length < 1) {
            e.preventDefault();
            e.stopPropagation();

            alert('Barang harus mempunyai minimal 1 gambar');
        }
        else
        $(window).unbind('beforeunload');
    });

    $('#kategori').on('change', function(){
        var kat=$(this).val();
        $('#subkategori').prop('disabled', true);
        $('#subkategori').html('<option disabled selected>Pilih Subkategori</option>');
        $.ajax({
            url : '{{url("my_products/get_subcategory")}}',
            type: 'get',
            data: {id:kat},
            success: function(data){
                $('#subkategori').prop('disabled', false);
                $('#subkategori').html(data);
            }
        });
    });

    $('input[type=radio][name=collapseGroup]').on('change', function(){
        if($(this).val()==='ya'){
            $('input[name=discount]').attr('disabled', false);
            $('input[name=expire]').attr('disabled', false);
        } else{
            $('input[name=discount]').attr('disabled', true);
            $('input[name=expire]').attr('disabled', true);
        }
    });

    CKEDITOR.replace('description');

    $('input[name=price]').on('change', function() {
        harga=$(this).val();
        potongan(harga);
    });

    harga=$('input[name=price]').val();
    potongan(harga);

    function potongan(harga){
        var total;
        if(harga<{!!$commission[0]->higherorequalto !!})
            total=harga
        else{
            $.each({!!$commission!!}, function(k, v){
                if(harga>=v.higherorequalto)
                    total=harga-v.cost
            })
        }
        $('#potong').val(total);
    }

</script>

@endsection