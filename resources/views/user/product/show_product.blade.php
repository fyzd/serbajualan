@extends('user.templates.t_index')
@section('title', 'Marketplace')
@section('cssfield')
<link href="{{URL::asset('assets/css2/rateit.css')}}" rel="stylesheet">
@endsection
@section('content')

<!-- desktop bar -->
<section class="desktop-bar bar-show">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('')}}">Halaman Depan</a></li>
                    @if (Request::is('c/*'))
                        @if(count($subcategory)!=0)
                        <li><a href="{{url('c').'/'.$category->slug}}">{{$category->description}}</a></li>
                        <li><a href="{{url('c').'/'.$category->slug.'/'.$subcategory->slug}}">{{$subcategory->description}}</a></li>
                        @else
                        <li><a href="{{url('c').'/'.$category->slug}}">{{$category->description}}</a></li>
                        @endif
                    @else

                    @endif
                </ol>
                <nav class="navbar navbar-default desk-nav">
                        <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <span class="navbar-brand nav-kat">
                            @if(Request::is('c/*'))
                            Kategori {{count($subcategory)!=0 ? $subcategory->description : $category->description}}
                            @else
                            Semua hasil pencarian
                            @endif
                        </span>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="desktop">
                        <div class="navbar-form nav navbar-nav navbar-left">
                            <div class="selection">
                                <form action="{{url()->current()}}" method="get" style="display: inline" id="sort">
                                    <input name="sort" type="hidden" value="{{Request::get('sort')}}">
                                </form>
                                
                            </div>	
                        </div>	
                            <ul class="nav nav-pills navbar-right">
                                <li role="presentation" {!!Request::get('sort')=='new' ? 'class="active"' : ''!!}><a id="new" href="#new">Terbaru</a></li>
                                <li role="presentation" {!!Request::get('sort')=='cheap' ? 'class="active"' : ''!!}><a id="cheap" href="#cheap">Termurah</a></li>
                                <li role="presentation" {!!Request::get('sort')=='expensive' ? 'class="active"' : ''!!}><a id="expensive" href="#expensive">Termahal</a></li>
                                <li role="presentation" {!!Request::get('sort')=='popular' ? 'class="active"' : ''!!}><a id="popular" href="#popular">Terpopuler</a></li>
                                <li role="presentation" {!!Request::get('sort')=='bestselling' ? 'class="active"' : ''!!}><a id="bestselling" href="#bestselling">Terlaris</a></li>
                            </ul>
                            
                        <div class="sort navbar-right">
                            <div class="urut">
                            <span>Urutkan</span>
                            </div>
                        </div>
                            
                        
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>

<!-- recommented products -->
<section class="product-list">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-2">
                <div class="sidebar">
                    <div class="well wedget">
                        <label>Kategori</label>
                        <div class="heading-border"></div>
                        <ul class="list-unstyled kate">
                            <li><a href="{{url('products')}}" style="content:''">Semua Kategori</a></li>
                            @foreach($kategori as $c)
                            <li>
                                <span>+</span><a href="{{url('c').'/'.$c->slug}}">{{$c->description}} ({{$c->product->count()}})</a>
                                @foreach($c->product_subcategory as $s)
                                <ul>
                                    <li class="list-unstyled"><span>-</span><a href="{{url('c').'/'.$c->slug.'/'.$s->slug}}">{{$s->description}} ({{$s->product->count()}})</a></li>
                                </ul>
                                @endforeach
                            @endforeach
                            </li>
                        </ul>
                    </div>

                    <div class="well wedget">
                        <label>Harga</label>
                        <div class="heading-border"></div>
                        
                        <form action="{{url()->current()}}" method="get" id="price" class="form-group">
                            <div class="form-group">
                                <input type="number" name="min" class="form-control" placeholder="Min"value="{{Request::get('min')}}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="max" class="form-control" placeholder="Max"value="{{Request::get('max')}}">
                            </div>

                            <button type="submit" class="btn btn-default btn-block">Tampilkan</button>
                        </form>
                        <form action="{{url()->current()}}" method="get" id="discount" class="form-group">
                            <div class="checkbox">
                                <label><input type="checkbox" name="discount" value="true" {{Request::get('discount')=='true' ? 'checked' : ''}}>Diskon</label>
                            </div>
                        </form>
                        
                    </div>

                    <!-- Sort by condition -->
                    <div class="well wedget">
                        <label>Kondisi Barang</label>
                        <div class="heading-border"></div>
                        <form action="{{url()->current()}}" method="get" id="condition" class="form-group">
                            <select name="condition" class="form-control">
                                <option value="">Semua Kondisi</option>
                                <option value="baru" {{Request::get('condition')=='baru' ? 'selected' : ''}}>Baru</option>
                                <option value="bekas" {{Request::get('condition')=='bekas' ? 'selected' : ''}}>Bekas</option>
                            </select>
                        </form>
                    </div>
                    
                    <div class="well wedget">
                        <label>Rating Barang</label>
                        <div class="heading-border"></div>
                        <form action="{{url()->current()}}" method="get" id="rating" class="form-group">
                        @for($i=5;$i>=1;$i--)
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rating" value="{{$i.'-5'}}" {{Request::get('rating')==$i.'-5' ? 'checked' : ''}}>
                                    @php 
                                    Request::get('rating')!=null ? $rating=explode('-', Request::get('rating')) : $rating[0]=6
                                    @endphp
                                    @for($j=1;$j<=5;$j++)
                                        @if($i>=$j)<span class="fa fa-star {{$rating[0]<=$i && $i>=$j ? 'checked' : ''}}"></span>
                                        @endif
                                    @endfor
                                    
                                </label>
                            </div>
                         @endfor 
                         <div class="radio">
                            <label><input type="radio" name="rating" value="all" >Lihat semua</label>
                        </div>
                        </form>
                    </div>
                    
                </div> <!-- sidebar -->
            </div>
            <div class="col-sm-10">
                
                <!-- desktop -->
                <div class="sect desktop">
                    <div class="row">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <div class="col-sm-12">
                            
                            <div class="row">
                            @foreach($products as $product)
                                <div class="col-sm-3 col-xs-6">
                                    <a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}">
                                        <div class="thumbnail paddingless">
                                            <!--span class="e-label"><div>Sale</div></span-->

                                            <span class="service-link text-center">
                                                @if($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                                                <div class="disc-badge">
                                                    <div class="disc-content">
                                                        {{$product->discount->discount}}%
                                                    </div>
                                                </div>
                                                @endif
                                                <img class="img-responsive show-byk" src="{{url('assets/images/products').'/'.$product->user_id.'/'.$product->product_image_first->name}}" alt="{{$product->name}}">
                                            </span>
                                            <div class="caption">
                                                <div class="p-det">
                                                    <div class="category"> 
                                                        <div class="rateit small" data-rateit-value="{{$product->review_rating->avg('rating')}}" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-mode="font"></div>
                                                        <div class="pull-right">{{$product->review_rating->count()}} ulasan</div>
                                                        
                                                    </div>
                                                
                                                    <a class="wrap" href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}">{{$product->name}}</a>
                                                    @if($product->discount==null || $product->discount!=null && $product->discount->expire < date('Y-m-d H:i:s'))
                                                    <strong>Rp{{number_format($product->price,0,',','.')}}</strong>
                                                    @elseif($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                                                    <strong class="discount">Rp{{number_format($product->price,0,',','.')}}</strong><strong>{{'Rp'.number_format($product->price - (round($product->price * ($product->discount->discount/100))),0,',','.')}}</strong>
                                                    @endif

                                                    <div class="store-name">
                                                    <a href="{{url('u/'.$product->users->username)}}">{{$product->users->user_name->name}}</a>
                                                    <p><i class="fa fa-map-marker"></i>{{ucwords(strtolower($product->users->address->kota->nama))}}</p>
                                                </div>
                                                </div>
                                                
                                                <div>
                                                    <form action="{{url('cart')}}" method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                                        @if(Auth::check())
                                                            @if($product->quantity!=0 && Auth::id()!=$product->user_id && Auth::user()->role_id==2)
                                                            <input type="hidden" name="quantity" value=1>
                                                            <button class="btn btn-default form-control" role="button">Tambahkan ke keranjang</button>
                                                            @elseif(Auth::id()==$product->user_id || Auth::user()->role_id==1)
                                                            <a href="{{url('p').'/'.$product->product_category->slug.'/'.$product->product_subcategory->slug.'/'.$product->id}}" class="btn btn-default form-control" role="button">Lihat Barang</a>
                                                            @else
                                                            <div class="alert alert-danger danger" role="alert">Stok habis</div>
                                                            @endif
                                                        @else
                                                            @if($product->quantity!=0)
                                                            <input type="hidden" name="quantity" value=1>
                                                            <button class="btn btn-default form-control" role="button">Tambahkan ke keranjang</button>
                                                            @else
                                                            <div class="alert alert-danger danger" role="alert">Stok habis</div>
                                                            @endif
                                                        @endif
                                                    </form>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div> <!-- desktop -->

                <!-- pagination -->
                {{$products->appends(Request::except('page'))->links()}}

            </div><!--div 9 -->
        </div>
    </div>
</section>

@endsection

@section('jsfield')
<script src="{{URL::asset('assets/js/jquery.rateit.min.js')}}"></script>
<script type="text/javascript">
    $('select[name=condition]').on('change', function(){
        $('#sort :input').not(':submit').clone().hide().appendTo('#condition');
        $('#search :input').not(':submit').clone().hide().appendTo('#condition');
        $('#price :input').not(':submit').clone().hide().appendTo('#condition');
        $('#discount :input').not(':submit').clone().hide().appendTo('#condition');
        $('#rating :input').not(':submit').clone().hide().appendTo('#condition');

        $('#condition').submit();
    });
    $('input[name=discount]').on('change', function(){
        if($(this).is(':checked'))
        {
            $('#sort :input').not(':submit').clone().hide().appendTo('#discount');
            $('#search :input').not(':submit').clone().hide().appendTo('#discount');
            $('#price :input').not(':submit').clone().hide().appendTo('#discount');
            $('#condition :input').not(':submit').clone().hide().appendTo('#discount');
            $('#rating :input').not(':submit').clone().hide().appendTo('#discount');

            $('#discount').submit();
        } 
        else
        {
            $(this).val('');
            $('#sort :input').not(':submit').clone().hide().appendTo('#discount');
            $('#search :input').not(':submit').clone().hide().appendTo('#discount');
            $('#price :input').not(':submit').clone().hide().appendTo('#discount');
            $('#condition :input').not(':submit').clone().hide().appendTo('#discount');
            $('#rating :input').not(':submit').clone().hide().appendTo('#discount');

            $('#discount').submit();
        }
        
    });
    $('#new, #cheap, #expensive, #popular, #bestselling').on('click', function(e){
        e.preventDefault();
        var param = $(this).attr("href").replace("#","");

        $('#condition :input').not(':submit').clone().hide().appendTo('#sort');
        $('#search :input').not(':submit').clone().hide().appendTo('#sort');
        $('#price :input').not(':submit').clone().hide().appendTo('#sort');
        $('#discount :input').not(':submit').clone().hide().appendTo('#sort');
        $('#rating :input').not(':submit').clone().hide().appendTo('#sort');

        $('input[name=sort]').attr('value', param);
        $('#sort').submit();

    });
    $('#price').on('submit', function(){
        $('#sort :input').not(':submit').clone().hide().appendTo('#price');
        $('#search :input').not(':submit').clone().hide().appendTo('#price');
        $('#condition :input').not(':submit').clone().hide().appendTo('#price');
        $('#discount :input').not(':submit').clone().hide().appendTo('#price');
        $('#rating :input').not(':submit').clone().hide().appendTo('#price');
    });
    $('input[name=rating]').on('change', function(){
        if($(this).val()!='all')
        {
            $('#sort :input').not(':submit').clone().hide().appendTo('#rating');
            $('#search :input').not(':submit').clone().hide().appendTo('#rating');
            $('#condition :input').not(':submit').clone().hide().appendTo('#rating');
            $('#discount :input').not(':submit').clone().hide().appendTo('#rating');
            $('#price :input').not(':submit').clone().hide().appendTo('#rating');

            $('#rating').submit();
        }
        else
        {
            $(this).val('');
            $('#sort :input').not(':submit').clone().hide().appendTo('#rating');
            $('#search :input').not(':submit').clone().hide().appendTo('#rating');
            $('#condition :input').not(':submit').clone().hide().appendTo('#rating');
            $('#discount :input').not(':submit').clone().hide().appendTo('#rating');
            $('#price :input').not(':submit').clone().hide().appendTo('#rating');

            $('#rating').submit();
        }
        
    });

    $('.kate li span').on('click', function() {
        $(this).parent().find('ul').toggle();
    });
</script>
@endsection