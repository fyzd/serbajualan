@extends('user.templates.t_index')
@section('title', 'Serbajualan: Situs Belanja Online dan Jual Beli Terbaru')
@section('cssfield')
<link href="{{URL::asset('assets/css2/rateit.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/css2/slick.css')}}" rel="stylesheet">
@endsection 
@section('content')

<!-- home -->
<section class="home">
    <div class="intro">
        <div id="home" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#home" data-slide-to="0" class="active"></li>
                <li data-target="#home" data-slide-to="1"></li>
                <li data-target="#home" data-slide-to="2"></li>
                <li data-target="#home" data-slide-to="3"></li>
                <li data-target="#home" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item slider active">
                    <div class="row">
                        <img class="img-responsive"src="{{url('assets/images/slider/1.jpg')}}">
                    </div>
                </div>
                <div class="item slider">
                    <div class="row">
                        <img class="img-responsive"src="{{url('assets/images/slider/2.jpg')}}">
                    </div>
                </div>
                <div class="item slider">
                    <div class="row">
                        <img class="img-responsive"src="{{url('assets/images/slider/3.jpg')}}">
                    </div>
                </div>
                <div class="item slider">
                    <div class="row">
                        <img class="img-responsive"src="{{url('assets/images/slider/4.jpg')}}">
                    </div>		
                </div>
                <div class="item slider">
                    <div class="row">
                        <img class="img-responsive"src="{{url('assets/images/slider/5.jpg')}}">
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#home" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="right carousel-control" href="#home" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</section>	


<!-- featured product -->
<section class="featured-product sect">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><span class="t-color-3">Baru </span> Dijual
                    <small class="btn-group hidden-xs">
                        
                        <select name="featured" class="form-control">
                            <option value="all">Semua Kategori</option>
                            @foreach($categories as $c)
                            
                            <option value="{{$c->slug}}">{{$c->description}}</option>
                            @endforeach
                        </select>
                    </small>
                    <small class="btn-group hidden-xs">
                        <a id="see-featured" class=" btn btn-success btn-lg see-more" href="{{url('products')}}">Selengkapnya <i class="fa fa-angle-double-right"></i></a>
                    </small>
                </h1>
                <div class="heading-border b-color-3"></div>
            </div>
        </div> <!-- section title -->
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li class="active" id="fprev"><i class="fa fa-angle-left"></i></li>
                <li id="fnext"><i class="fa fa-angle-right"></i></li>
            </ol>
            <div class="outer-featured">
                @include('user.templates.t_featured_product')
            </div>
        </div>    
        
    </div>
</section>		

<!-- emarket adds -->
<section class="sponsor">
    <div class="container">
        <div id="sponsor" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-4">
                                <img class="img-responsive" src="{{ asset('assets/images/slider/6.jpg') }}" alt="" />
                            </div>
                            <div class="col-sm-4">
                                <img class="img-responsive" src="{{ asset('assets/images/slider/7.jpg') }}" alt="" />
                            </div>
                            <div class="col-sm-4">
                                <img class="img-responsive" src="{{ asset('assets/images/slider/8.jpg') }}" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-4">
                                <img class="img-responsive" src="{{ asset('assets/images/slider/9.jpg') }}" alt="" />
                            </div>
                            <div class="col-sm-4">
                                <img class="img-responsive" src="{{ asset('assets/images/slider/10.jpg') }}" alt="" />
                            </div>
                            <div class="col-sm-4">
                                <img class="img-responsive" src="{{ asset('assets/images/slider/11.jpg') }}" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- Controls -->
            <a class="control-left" href="#sponsor" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="control-right" href="#sponsor" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</section>

<!-- discount product -->
<section class="discount-product sect">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><span class="t-color-3">Gebyar</span> Diskon
                <small class="btn-group hidden-xs">
                <select name="discount" class="form-control">
                        <option value="all">Semua Kategori</option>
                        @foreach($categories as $c)
                        
                        <option value="{{$c->slug}}">{{$c->description}}</option>
                        @endforeach
                    </select>
                    </small>
                    <small class="btn-group hidden-xs">
                        <a id="see-discount" class=" btn btn-success btn-lg see-more" href="{{url('products?discount=true')}}">Selengkapnya <i class="fa fa-angle-double-right"></i></a>
                    </small>
                </h1>
                <div class="heading-border b-color-3"></div>
            </div>
        </div> <!-- section title -->
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li class="active" id="dprev"><i class="fa fa-angle-left"></i></li>
                <li id="dnext"><i class="fa fa-angle-right"></i></li>
            </ol>
            <div class="outer-discount">
                @include('user.templates.t_discount_product')
            </div>
        </div>
        
    </div>
</section>	

<!-- popular product -->
<section class="popular-product sect">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><span class="t-color-3">Paling Banyak Dilihat</span> Minggu Ini
                <small class="btn-group hidden-xs">
                <select name="popular" class="form-control">
                        <option value="all">Semua Kategori</option>
                        @foreach($categories as $c)
                        
                        <option value="{{$c->slug}}">{{$c->description}}</option>
                        @endforeach
                    </select>
                    </small>
                    <small class="btn-group hidden-xs">
                        <a id="see-popular" class=" btn btn-success btn-lg see-more" href="{{url('products')}}">Selengkapnya <i class="fa fa-angle-double-right"></i></a>
                    </small>
                </h1>
                <div class="heading-border b-color-3"></div>
            </div>
        </div> <!-- section title -->
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li class="active" id="pprev"><i class="fa fa-angle-left"></i></li>
                <li id="pnext"><i class="fa fa-angle-right"></i></li>
            </ol>
            <div class="outer-popular">
                @include('user.templates.t_popular_product')
            </div>
        </div>
        
    </div>
</section>	

<!-- best selling product -->
<section class="bestselling-product sect">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><span class="t-color-3">Paling Laris</span> Minggu Ini
                <small class="btn-group hidden-xs">
                <select name="bestselling" class="form-control">
                        <option value="all">Semua Kategori</option>
                        @foreach($categories as $c)
                        
                        <option value="{{$c->slug}}">{{$c->description}}</option>
                        @endforeach
                    </select>
                    </small>
                    <small class="btn-group hidden-xs">
                        <a id="see-bestselling" class=" btn btn-success btn-lg see-more" href="{{url('products')}}">Selengkapnya <i class="fa fa-angle-double-right"></i></a>
                    </small>
                </h1>
                <div class="heading-border b-color-3"></div>
            </div>
        </div> <!-- section title -->
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li class="active" id="bprev"><i class="fa fa-angle-left"></i></li>
                <li id="bnext"><i class="fa fa-angle-right"></i></li>
            </ol>
            <div class="outer-bestselling">
                @include('user.templates.t_bestselling_product')
            </div>
        </div>
        
    </div>
</section>	

<!-- much reviewed product -->
<section class="muchreviewed-product sect">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><span class="t-color-3">Paling Sering Diulas</span> Minggu Ini
                <small class="btn-group hidden-xs">
                <select name="muchreviewed" class="form-control">
                        <option value="all">Semua Kategori</option>
                        @foreach($categories as $c)
                        
                        <option value="{{$c->slug}}">{{$c->description}}</option>
                        @endforeach
                    </select>
                    </small>
                    <small class="btn-group hidden-xs">
                        <a id="see-muchreviewed" class=" btn btn-success btn-lg see-more" href="{{url('products')}}">Selengkapnya <i class="fa fa-angle-double-right"></i></a>
                    </small>
                </h1>
                <div class="heading-border b-color-3"></div>
            </div>
        </div> <!-- section title -->
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li class="active" id="mprev"><i class="fa fa-angle-left"></i></li>
                <li id="mnext"><i class="fa fa-angle-right"></i></li>
            </ol>
            <div class="outer-muchreviewed">
                @include('user.templates.t_muchreviewed_product')
            </div>
        </div>
        
    </div>
</section>

        
<section class="promote">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-12 promote-left">
                <img class="img-responsive pull-right" src="{{url('assets/images/web/appss.png')}}">
            </div>
            <div class="col-sm-6 col-xs-12 promote-right">
                <div class="promote-header">
                    <p>Download Aplikasi Serbajualan Sekarang</p>
                </div>
                <p>Cepat, simpel dan mudah digunakan.</p>
                <p>Hanya membutuhkan waktu 30 detik untuk mendownload.</p>
                <div class="promote-download">
                    <div class="row">
                        <div class="col-xs-6">
                            <img class="img-responsive" width="200" src="{{url('assets/images/web/play_app.png')}}">
                        </div>
                        <div class="col-xs-6 apple">
                            <img class="img-responsive" width="200" src="{{url('assets/images/web/store_app.png')}}">
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>



@endsection

@section('jsfield')
<script src="{{URL::asset('assets/js/slick.min.js')}}"></script>

<script type="text/javascript">
    var token='{{csrf_token()}}';

    $('select[name=featured]').on('change', function(){
        $('.featured').html('<img class="loading" src="{{url('assets/images/web/loading.svg')}}">');
        $.ajax({
            url : '{{url("change/featured")}}',
            type: 'post',
            data: {id: $('select[name=featured]').val(), _token:token},
            dataType: 'html',
            success: function(data){
                
                setTimeout(function () {
                    
                    $('.featured').slick('unslick');
                    $('.outer-featured').html(data);
                    $('.featured').slick({
                        arrows:false,
                        slidesToShow: 6,
                        slidesToScroll: 2,
                        lazyLoad: 'ondemand',
                        responsive: [
                            {
                            breakpoint: 480,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                }
                            }
                        ]
                    });
                    $.getScript("{{URL::asset('assets/js/jquery.rateit.min.js')}}");
                }, 1000);
                
                if($('select[name=featured]').val()!='all')
                    $('#see-featured').attr('href', '{{url("c")}}/'+$('select[name=featured]').val());
                    
                else
                    $('#see-featured').attr('href', '{{url("products")}}');
            }
        });
    });

    $('select[name=popular]').on('change', function(){
        $('.popular').html('<img class="loading" src="{{url('assets/images/web/loading.svg')}}">');
        $.ajax({
            url : '{{url("change/popular")}}',
            type: 'post',
            data: {id: $('select[name=popular]').val(), _token:token},
            dataType: 'html',
            success: function(data){
                setTimeout(function () {
                    $('.popular').slick('unslick');
                    $('.outer-popular').html(data);
                    $('.popular').slick({
                        arrows:false,
                        slidesToShow: 6,
                        slidesToScroll: 2,
                        lazyLoad: 'ondemand',
                        responsive: [
                            {
                            breakpoint: 480,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                }
                            }
                        ]
                    });
                    $.getScript("{{URL::asset('assets/js/jquery.rateit.min.js')}}")
                }, 1000);
                if($('select[name=popular]').val()!='all')
                    $('#see-popular').attr('href', '{{url("c")}}/'+$('select[name=popular]').val());
                else
                    $('#see-popular').attr('href', '{{url("products")}}');
            }
        });
    });

    $('select[name=bestselling]').on('change', function(){
        $('.bestselling').html('<img class="loading" src="{{url('assets/images/web/loading.svg')}}">');
        $.ajax({
            url : '{{url("change/bestselling")}}',
            type: 'post',
            data: {id: $('select[name=bestselling]').val(), _token:token},
            dataType: 'html',
            success: function(data){
                setTimeout(function () {
                    $('.bestselling').slick('unslick');
                    $('.outer-bestselling').html(data);
                    $('.bestselling').slick({
                        arrows:false,
                        slidesToShow: 6,
                        slidesToScroll: 2,
                        lazyLoad: 'ondemand',
                        responsive: [
                            {
                            breakpoint: 480,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                }
                            }
                        ]
                    });
                    $.getScript("{{URL::asset('assets/js/jquery.rateit.min.js')}}")
                }, 1000);
                if($('select[name=bestselling]').val()!='all')
                    $('#see-bestselling').attr('href', '{{url("c")}}/'+$('select[name=bestselling]').val());
                else
                    $('#see-bestselling').attr('href', '{{url("products")}}');
            }
        });
    });

    $('select[name=discount]').on('change', function(){
        $('.discountslick').html('<img class="loading" src="{{url('assets/images/web/loading.svg')}}">');
        $.ajax({
            url : '{{url("change/discount")}}',
            type: 'post',
            data: {id: $('select[name=discount]').val(), _token:token},
            dataType: 'html',
            success: function(data){
                setTimeout(function () {
                    $('.discountslick').slick('unslick');
                    $('.outer-discount').html(data);
                    $('.discountslick').slick({
                        arrows:false,
                        slidesToShow: 6,
                        slidesToScroll: 2,
                        lazyLoad: 'ondemand',
                        responsive: [
                            {
                            breakpoint: 480,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                }
                            }
                        ]
                    });
                    $.getScript("{{URL::asset('assets/js/jquery.rateit.min.js')}}")
                }, 1000);
                if($('select[name=discount]').val()!='all')
                    $('#see-discount').attr('href', '{{url("c")}}/'+$('select[name=discount]').val()+'?discount=true');
                else
                    $('#see-discount').attr('href', '{{url("products")}}?discount=true');
            }
        });
    });

    $('select[name=muchreviewed]').on('change', function(){
        $('.muchreviewed').html('<img class="loading" src="{{url('assets/images/web/loading.svg')}}">');
        $.ajax({
            url : '{{url("change/muchreviewed")}}',
            type: 'post',
            data: {id: $('select[name=muchreviewed]').val(), _token:token},
            dataType: 'html',
            success: function(data){
                setTimeout(function () {
                    $('.muchreviewed').slick('unslick');
                    $('.outer-muchreviewed').html(data);
                    $('.muchreviewed').slick({
                        arrows:false,
                        slidesToShow: 6,
                        slidesToScroll: 2,
                        lazyLoad: 'ondemand',
                        responsive: [
                            {
                            breakpoint: 480,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                }
                            }
                        ]
                    });
                    $.getScript("{{URL::asset('assets/js/jquery.rateit.min.js')}}")
                }, 1000);
                if($('select[name=muchreviewed]').val()!='all')
                    $('#see-muchreviewed').attr('href', '{{url("c")}}/'+$('select[name=muchreviewed]').val());
                else
                    $('#see-muchreviewed').attr('href', '{{url("products")}}');
            }
        });
    });

    $('.slick-slider').slick({
        arrows:false,
        slidesToShow: 6,
        slidesToScroll: 2,
        lazyLoad: 'ondemand',
        responsive: [
            {
            breakpoint: 481,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            }
        ]
    });

    $('#fnext').click(function(){
        $('.featured').slick('slickNext');
        $('#fnext').attr('class', 'active');
        $('#fprev').attr('class', '');
    });
    $('#fprev').click(function(){
        $('.featured').slick('slickPrev');
        $('#fprev').attr('class', 'active');
        $('#fnext').attr('class', '');
    });

    $('#pnext').click(function(){
        $('.popular').slick('slickNext');
        $('#pnext').attr('class', 'active');
        $('#pprev').attr('class', '');
    });
    $('#pprev').click(function(){
        $('.popular').slick('slickPrev');
        $('#pprev').attr('class', 'active');
        $('#pnext').attr('class', '');
    });

    $('#bnext').click(function(){
        $('.bestselling').slick('slickNext');
        $('#bnext').attr('class', 'active');
        $('#bprev').attr('class', '');
    });
    $('#bprev').click(function(){
        $('.bestselling').slick('slickPrev');
        $('#bprev').attr('class', 'active');
        $('#bnext').attr('class', '');
    });

    $('#dnext').click(function(){
        $('.discountslick').slick('slickNext');
        $('#dnext').attr('class', 'active');
        $('#dprev').attr('class', '');
    });
    $('#dprev').click(function(){
        $('.discountslick').slick('slickPrev');
        $('#dprev').attr('class', 'active');
        $('#dnext').attr('class', '');
    });

    $('#mnext').click(function(){
        $('.muchreviewed').slick('slickNext');
        $('#mnext').attr('class', 'active');
        $('#mprev').attr('class', '');
    });
    $('#mprev').click(function(){
        $('.muchreviewed').slick('slickPrev');
        $('#mprev').attr('class', 'active');
        $('#mnext').attr('class', '');
    });

    window.paceOptions = {
        ajax: false,
        restartOnRequestAfter: false,
    };
</script>

<script src="{{URL::asset('assets/js/jquery.rateit.min.js')}}"></script>

@endsection