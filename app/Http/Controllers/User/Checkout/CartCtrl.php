<?php

namespace App\Http\Controllers\User\Checkout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Auth;
use Cookie;
use DB;

use App\Http\Models\Cart;
use App\Http\Models\Product;
use App\Http\Models\Product_Image;
use App\Http\Models\Store;

class CartCtrl extends Controller
{
    protected $cart;

    public function __construct()
    {
        $this->middleware('user.only');
        $this->cart=Cookie::get('cart');
    }

    public function index()
    {
        return view ('user/checkout/cart');
    }

    public function addProduct(Request $request)
    {
        $product_id=$request->product_id;
        $quantity = $request->quantity;
        $qty=Product::select('name','quantity')->where(['id' => $product_id])->first();
        if(Auth::check()) 
        {
            
            if($qty->quantity < $request->quantity || $request->quantity<=0)
                return back()->withErrors('Pembelian '.$qty->name.' tidak boleh lebih dari '.$qty->quantity);
            else
            {
                $cart = Cart::firstOrCreate([
                    'product_id'=> $product_id,
                    'user_id'=>Auth::id()
                ]);
                $cart->quantity += $quantity;
                $cart->save();
                return back();
            }
            
        } 
        else 
        {
            $cart=$request->cookie('cart', []);
            if (array_key_exists($product_id, $cart)){
                
                    $quantity+=$cart[$product_id];
            }
            $cart[$product_id]=$quantity;
            if($qty->quantity < $cart[$product_id] || $cart[$product_id]<=0)
                return back()->withErrors('Pembelian '.$qty->name.' tidak boleh lebih dari '.$qty->quantity);
            else
                return back()->withCookie(cookie()->forever('cart', $cart));
            
        }
    }

    public function removeProduct(Request $request, $product_id)
    {
        if (Auth::check()) {
            $cart = Cart::firstOrCreate([
                'product_id'=>$product_id,
                'user_id'=>Auth::id()
            ]);

            $cart->delete();
            return redirect('cart');
        }else{

            $cart = $request->cookie('cart', []);
            unset($cart[$product_id]);
                
            return redirect('cart')->withCookie(cookie()->forever('cart', $cart));
        }
    }

    public function updateQty(Request $request, $product_id)
    {
        $quantity=$request->quantity;
        $qty=Product::select('name','quantity')->where(['id' => $product_id])->first();
        if($qty->quantity < $quantity || $quantity<=0)
            return back()->withErrors('Pembelian '.$qty->name.' tidak boleh lebih dari '.$qty->quantity);
        else
        {
            if(Auth::check())
            {
                
                $cart= Cart::firstOrCreate(['user_id'=>Auth::id(), 'product_id'=>$product_id]);
                $cart->quantity = $quantity;;
                $cart->save();
    
                return back();
            } 
            else
            {
                $cart= $request->cookie('cart',[]);
                $cart[$product_id] = $quantity;
    
                return back()->withCookie(cookie()->forever('cart', $cart));
            }
        }
        
    }

    public function details()
    {
        $result=[];

        if($this->totalProduct()>0){
            if(Auth::check())
            {
                $result=Store::select('id', 'user_id')->with('productsOnCart.cart', 'users.user_name', 'users.address')
                ->whereHas('productsOnCart.cart', function($q){
                    $q->where('user_id', Auth::id());
                })->get();

                foreach($result as $r){
                    foreach($r->productsOnCart as $product){
                        if($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                            $product['price_after_disc']=$product->price - (round($product->price * ($product->discount->discount/100)));
                        else
                            $product['price_after_disc']=$product->price;
                        
                    }
                }
            
            }
            else
            {
                foreach($this->lists() as $product_id=>$quantity){
                    $product[]=$product_id;
                }
                $result=Store::select('id', 'user_id')->with(['products'=>function($q)use($product){
                    $q->whereIn('id', $product);
                }])->whereHas('products',function($q)use($product){
                    $q->whereIn('id', $product);
                })->with('users.user_name', 'users.address')->get();

                foreach($result as $r){
                    foreach($r->products as $product){
                        foreach($this->lists() as $product_id=>$quantity){
                            $product['cookie_qty']=$quantity;
                        }
                        
                        if($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
                            $product['price_after_disc']=$product->price - (round($product->price * ($product->discount->discount/100)));
                        else
                            $product['price_after_disc']=$product->price;
                        
                    }
                }
            }
            
            
        }
        
        return $result; 
    }

    public function getKotaPenjual()
    {
        foreach($this->cart->details() as $product){
            $penjual[]=$product['detail']['user_id'];
        }
        foreach($penjual as $p){
            $kota[]=DB::table('users_addresses')
                        ->select('kota.nama')
                        ->join('kota', 'kota_id', '=', 'kota.id')
                        ->where('user_id', $p)
                        ->first();
        }
        return $kota;
    }

    public function totalPrice()
    {
        $total=0;
        foreach($this->details() as $order)
        {
            if(Auth::check())
            {
                foreach($order->productsOnCart as $product)
                {
                    $total += $product->price_after_disc*$product->cart->quantity;
                }
            }
            else
            {
                foreach($order->products as $product)
                {
                    $total += $product->price_after_disc*$product->cookie_qty;
                }
            }
            
        }
        return $total;
    }

    public function lists()
    {
        if(Auth::check()){
            return Cart::where('user_id', Auth::id())->pluck('quantity', 'product_id');
        } else{
            return $this->cart;
        }
        
    }

    public function totalProduct()
    {
        return count($this->lists());
    }

    public function isEmpty()
    {
        return $this->totalProduct() < 1;
    }

    public function merge()
    {
        if($this->cart != null)
        {
            $cart_cookie = Cookie::get('cart',[]);

            foreach ($cart_cookie as $product_id => $quantity) {
                $product=Product::select('id')->where('user_id', Auth::id())->where('id', $product_id)->first();
                
                if($product!=null) {

                }
                else {
                    $cart = Cart::firstOrCreate([
                        'user_id'=>Auth::id(),
                        'product_id'=>$product_id
                    ]);
                    $cart->quantity = $quantity;
                    $cart->save();
                }
                
            }
            return Cookie::queue(Cookie::forget('cart'));
        }
    }

    public function getTransaction()
    {
        $purchase=\App\Http\Models\Invoice::select('id')->where('buyer_id', Auth::id())->whereIn('invoice_status_id', ['1','2'])->count();
        $sale=\App\Http\Models\Order::select('id')->whereHas('order_item.products', function($q){
            $q->where('products.user_id', Auth::id());
        })->where('order_status_id', 2)->count();

        return response()->json(["purchase"=>$purchase, "sale"=>$sale]);
    }

    public function getProduct()
    {
        return Product::select('id')->where('user_id', Auth::id())->count();
    }

}
