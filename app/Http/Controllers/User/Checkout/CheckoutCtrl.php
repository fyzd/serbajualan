<?php

namespace App\Http\Controllers\User\Checkout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Auth;
use Cookie;
use App\Http\Controllers\User\Checkout\CartCtrl;
use RajaOngkir;
use Session;
use Validator;

use App\Http\Traits\OngkirTrait;

use App\Http\Models\Cart;
use App\Http\Models\User;
use App\Http\Models\Product;
use App\Http\Models\Seller_Courier;
use App\Http\Models\Invoice;
use App\Http\Models\Order;
use App\Http\Models\Order_Item;
use App\Http\Models\Shipment;
use App\Http\Models\Shipment_For_Create;
use App\Http\Models\Wallet;
use App\Http\Models\Wallet_Mutation;

class CheckoutCtrl extends Controller
{
    use OngkirTrait;

    protected $cart;

    public function __construct(CartCtrl $cart)
    {
        $this->cart=$cart;
        $this->middleware('user');
        $this->middleware('address.filled');
        $this->middleware('confirmed.email');
    }

    public function checkoutView()
    {
        
        $ongkir=$this->getOngkir();

        if($ongkir==false)
            return 'Maaf, api rajaongkir error';
        else{
            $total=$this->getTotal($ongkir);
            if(Auth::check())
            {
                return view ('user/checkout/checkout', [
                    'alamat' => $this->address, 
                    'ongkir'    => $ongkir,
                    'total'     => $total
                ]);
            }
            return view('user/checkout/checkout', ['provinsi' => $provinsi]);
        }
    }

    public function purchasesView($invoice_number)
    {
        $barang=0;
        $barangtmp=0;
        $barangtmp2=0;
        $barangtmp3=0;
        $kirim=0;

        $invoice=Invoice::select('id', 'invoice_number')->with('order.order_item', 'order.shipments', 'users.user_name')->where('invoice_number', $invoice_number)->first();
        $saldo=Wallet::select('balance')->where('user_id', Auth::id())->first();

        foreach($invoice->order as $o)
        {
            foreach($o->order_item as $order_item)
            {
                $barangtmp=$order_item->price;
                $barangtmp2+=$barangtmp;
                $barangtmp=0;
            }
            $barangtmp3=$barangtmp2;
            
            $kirim+=$o->shipments->cost;
            
        }
        $barang+=$barangtmp3;

        $total=$barang+$kirim;

        if(Auth::check())
        {
            return view ('user/checkout/purchases', ['total'=>$total, 'barang'=>$barang, 'ongkir'=>$kirim, 'invoice_number'=>$invoice->invoice_number, 'saldo'=>$saldo]);
        }
        return view('user/checkout/purchases');
        
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required'
        ]);

        $email      = $request->email;
        $password   = $request->password;
        
        if($request->has('password'))
            return $this->auth($email, $password);
        else
            return $this->checkEmail($email);
    }

    public function checkEmail($email)
    {
        if(User::where('email', $email)->first())
        {
            return back()->withErrors('Email exist');
        }
    }

    public function auth($email, $password)
    {
        if(Auth::attempt(['email' => $email, 'password' => $password]))
        {
            $this->cart->merge();
            return back();
        }
        else
            return back()->withErrors('Email/password salah');
    }

    public function getTotal($data)
    {
        $costbelanja=0;
        $costongkir=0;
        $costtotal=0;
        
        if(Session::has('total')){
            $sess=Session::get('total');
            $total=[
                'belanja'   => $sess['belanja'],
                'ongkir'    => $sess['ongkir'],
                'total'     => $sess['total']
            ];
        } else{
            foreach($data as $ongkir){
                $costbelanja += $ongkir->subtotal;
                $costongkir += $ongkir->costongkir;
                
            }
    
            $costtotal = $costbelanja + $costongkir;
    
            $total=[
                'belanja'   => $costbelanja,
                'ongkir'    => $costongkir,
                'total'     => $costtotal
            ];
    
            Session::put('total', $total);
        }

        return $total;
    }

    public function changeAddress(Request $request)
    {
        Session::forget('quantity');
        Session::forget('ongkir');
        Session::forget('total');
        Session::forget('service');

        $ongkir=$this->getOngkirIfAddressChanged($request->address);
        
        if($ongkir==false)
            return 'Maaf, api rajaongkir error';
        else{
            $total=$this->getTotal($ongkir);
            return json_encode(array($ongkir, $total));
        }
    }

    public function changeCourier(Request $request)
    {
        $costongkir=0;
        $costtotal=0;

        $ongkir=Session::get('ongkir');
        $data=explode('-', $request->kurir);
            if(array_key_exists($data[0], $ongkir)){
                $data[2]=intval($data[2]);
            }
        
        $ongkir[$data[0]] = intval($data[2]);
        Session::put('ongkir', $ongkir);

        //Insert nilai kurir ke database

        $service=Session::get('service');
            if(array_key_exists($data[1], $service)){
                $data[1]=$data[1];
            }
        
        $service[$data[0]] = $data[1];
        Session::put('service', $service);

        

        $ongkir2=Session::get('ongkir');
        $total=Session::get('total');

        foreach($ongkir2 as $o){
            $costongkir+=$o;
        }

        $costtotal=$total['belanja']+$costongkir;

        $totalupdate=[
            'belanja'   => $total['belanja'],
            'ongkir'    => $costongkir,
            'total'     => $costtotal
        ];
        
        Session::put('total', $totalupdate);

        $data[3]=$totalupdate['ongkir'];
        $data[4]=$totalupdate['total'];

        return $data;
    }

    public function snap()
    {
        dd(Session::get('ongkir'));
        return view('user/midtrans/snap_checkout');
    }

    public function deleteSession(Request $request)
    {
        /*foreach(Session::get('quantity') as $k => $v)
        {
            $qty=Product::select('quantity')->where('id', $k)->first();
            Product::where('id', $k)->update(['quantity' => $qty->quantity+$v]);
        }*/
        Session::forget('quantity');
        Session::forget('ongkir');
        Session::forget('total');
        Session::forget('service');
        
    }

    public function orderProduct(Request $request)
    {
        
        $v = Validator::make($request->all(), [
            'address' => 'required'
        ]);

        if ($v->fails())
        {
            return response()->json(['error'=>$v->errors()]);
        }

        $ongkir=$this->getOngkir();

        if($ongkir==false)
            return 'Maaf, api rajaongkir error';
        else{
            $total=$this->getTotal($ongkir);
            if(Auth::check())
            {
                $invoice_number=$this->invoice_number();
                $invoice=Invoice::create([
                    'invoice_number'    => $invoice_number,
                    'buyer_id'          => Auth::id(),
                    'amount'            => $total['total'],
                    'invoice_status_id' => 1
                ]);
                foreach($ongkir as $detail){
                    $order=Order::create([
                        'invoice_id'        => $invoice->id,
                        'order_status_id'   => 1,
                    ]);
                    Shipment_For_Create::create([
                        'order_id'          => $order->id,
                        'service'           => $detail->service,
                        'cost'              => $detail->costongkir,
                        'buyer_address_id'  => $request->address,
                        'store_address_id'  => $detail->store_address
                    ]);
                    foreach($detail->productsOnCart as $product){
                        $order_item=Order_Item::create([
                            'order_id'      => $order->id,
                            'product_id'    => $product->id,
                            'quantity'      => $product->cart->quantity,
                            'price'         => $product->price_after_disc * $product->cart->quantity,
                            'order_item_status_id' => 1
                        ]);
                        //Kurangi Quantity Produk
                        $qty=Product::select('quantity')->where('id', $product->id)->first();
                        Product::where('id', $product->id)->update(['quantity' => $qty->quantity-$product->cart->quantity]);
    
                        Cart::where('user_id', Auth::id())->where('product_id', $product->id)->delete();
                    } 
                }

                return $invoice_number;
            }
            return 'Error';
        }

    }

    public function purchaseWallet(Request $request)
    {
        $this->validate($request,[
            'method' => 'required'
        ]);

        $bal=Wallet::select('balance')->where('user_id', Auth::id())->first();
        $inv=Invoice::select('id', 'invoice_number', 'amount')->where('invoice_number', $request->number)->first();
        if($bal->balance < $inv->amount || $bal==null) 
            return back()->withErrors('Saldo dompet Anda tidak mencukupi');
        else{
            Invoice::where('invoice_number', $request->number)->update([
                'invoice_status_id' => 3
            ]);
            Order::where('invoice_id', $inv->id)->update([
                'order_status_id'   => 2
            ]);

            $order_id=Order::select('id')->where('invoice_id', $inv->id)->pluck('id');
    
            Order_Item::whereIn('order_id', $order_id)->update([
                'order_item_status_id'   => 2
            ]);

            $wallet=Wallet::firstOrCreate([
                'user_id'   => Auth::id()
            ]);
            $wallet->balance -= $inv->amount;
            $wallet->save();
            
            $mutation= new Wallet_Mutation();
            $mutation->wallet_id            = $wallet->id;
            $mutation->mutation             = -$inv->amount;
            $mutation->remaining_bal        -= $inv->amount;
            $mutation->mutation_detail_id   = 1;
            $mutation->invoice_id           = $inv->id;
            $mutation->save();

            return redirect('payment/invoices');
        }
    }

    public function invoice_number()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        
        // generate a pin based on 2 * 7 digits + a random character
        $id = 'SJ'
            . mt_rand(10000, 99999)
            . $characters[rand(0, strlen($characters) - 1)]
            . $characters[rand(0, strlen($characters) - 1)]
            . $characters[rand(0, strlen($characters) - 1)]
            . $characters[rand(0, strlen($characters) - 1)]
            . $characters[rand(0, strlen($characters) - 1)]
            . 'INV';
        
        // shuffle the result
        return $id;
    }
    
}
