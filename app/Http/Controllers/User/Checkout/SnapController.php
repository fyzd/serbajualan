<?php

namespace App\Http\Controllers\User\Checkout;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use App\Http\Traits\OngkirTrait;

use App\Midtrans\Midtrans;

use App\Http\Models\Invoice;
use App\Http\Models\Order;
use App\Http\Models\Order_Item;

class SnapController extends Controller
{
    use OngkirTrait;

    
    public function __construct()
    {   
        Midtrans::$serverKey = 'VT-server-pLx-jReQDr4Qg7VCuy6FD5_N';
        //set is production to true for production mode
        Midtrans::$isProduction = false;
    }

    public function snap()
    {
        return view('user/midtrans/snap_checkout');
    }

    public function token($invoice_number) 
    {
        $items=[];

        $invoice=Invoice::with('users.user_name', 'order.order_item.products.stores.users.user_name', 'order.shipments.buyer_address.kecamatan','order.shipments.buyer_address.kota','order.shipments.buyer_address.provinsi')->where('invoice_number', $invoice_number)->first();

        error_log('masuk ke snap token dri ajax');
        $midtrans = new Midtrans;

        foreach ($invoice->order as $o)
        {
            foreach($o->order_item as $order_item)
            {
                array_push($items, [
                    'id'        => $order_item->product_id,
                    'price'     => $order_item->price/$order_item->quantity,
                    'quantity'  => $order_item->quantity,
                    'name'      => substr($order_item->products->name, 0, 50)
                ]);
            }
            array_push($items, [
                'id'        => $o->id,
                'price'     => $o->shipments->cost,
                'quantity'  => 1,
                'name'      => 'Biaya Kirim '.$o->shipments->service
            ]);
        }

        $transaction_details = array(
            'order_id'      => $invoice->id,
            'gross_amount'  => $invoice->amount
        );

        
        // Populate customer's billing address
        $billing_address = array(
            'first_name'    => $invoice->order[0]->shipments->buyer_address->pp,
            'last_name'     => "",
            'address'       => $invoice->order[0]->shipments->buyer_address->address,
            'city'          => ucwords(strtolower($invoice->order[0]->shipments->buyer_address->kota->nama)),
            'postal_code'   => $invoice->order[0]->shipments->buyer_address->zip_code,
            'phone'         => $invoice->order[0]->shipments->buyer_address->phone,
            'country_code'  => 'IDN'
            );

        // Populate customer's shipping address
        $shipping_address = array(
            'first_name'    => $invoice->order[0]->shipments->buyer_address->pp,
            'last_name'     => "",
            'address'       => $invoice->order[0]->shipments->buyer_address->address.', Kec. '.ucwords(strtolower($invoice->order[0]->shipments->buyer_address->kecamatan->nama)),
            'city'          => ucwords(strtolower($invoice->order[0]->shipments->buyer_address->kota->nama)),
            'postal_code'   => $invoice->order[0]->shipments->buyer_address->zip_code,
            'phone'         => $invoice->order[0]->shipments->buyer_address->phone,
            'country_code'  => 'IDN'
            );

        // Populate customer's Info
        $customer_details = array(
            'first_name'      => $invoice->users->user_name->name,
            'last_name'       => "",
            'email'           => $invoice->users->email,
            'phone'           => $invoice->users->user_info->phone,
            'billing_address' => $billing_address,
            'shipping_address'=> $shipping_address
            );

        // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit'       => 'day', 
            'duration'   => 3
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $items,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
    
        try
        {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            //return redirect($vtweb_url);
            echo $snap_token;
        } 
        catch (Exception $e) 
        {   
            return $e->getMessage;
        }
    }

    public function finish(Request $request)
    {
        $result = $request->input('result_data');
        $result = json_decode($result);
        echo $result->status_message . '<br>';
        echo 'RESULT <br><pre>';
        echo '</pre>' ;

        
    }

    public function notification()
    {
        $midtrans = new Midtrans;
        echo 'test notification handler';
        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);

        if($result){
            $notif = $midtrans->status($result->order_id);
        }
        
        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status;

        if ($transaction == 'pending') {
            Invoice::where('id', $order_id)->update([
                'invoice_status_id' => 2
            ]);
        } else if ($transaction == 'expire') {
            Invoice::where('id', $order_id)->update([
                'invoice_status_id' => 4
            ]);
            Order::where('invoice_id', $order_id)->update([
                'order_status_id'   => 6
            ]);

            $order_db_id=Order::select('id')->where('invoice_id', $order_id)->pluck('id');
    
            Order_Item::whereIn('order_id', $order_db_id)->update([
                'order_item_status_id'   => 5
            ]);
        } else if ($transaction == 'settlement') {
            Invoice::where('id', $order_id)->update([
                'invoice_status_id' => 3
            ]);
            Order::where('invoice_id', $order_id)->update([
                'order_status_id'   => 2
            ]);

            $order_db_id=Order::select('id')->where('invoice_id', $order_id)->pluck('id');
    
            Order_Item::whereIn('order_id', $order_db_id)->update([
                'order_item_status_id'   => 2
            ]);
        } else if ($transaction == 'capture') {
            if ($type == 'credit_card'){
                if($fraud == 'challenge'){
                    Invoice::where('id', $order_id)->update([
                        'invoice_status_id' => 5
                    ]);
                    Order::where('invoice_id', $order_id)->update([
                        'order_status_id'   => 7
                    ]);
        
                    $order_db_id=Order::select('id')->where('invoice_id', $order_id)->pluck('id');
            
                    Order_Item::whereIn('order_id', $order_db_id)->update([
                        'order_item_status_id'   => 5
                    ]);
                } 
                else {
                    Invoice::where('id', $order_id)->update([
                        'invoice_status_id' => 3
                    ]);
                    Order::where('invoice_id', $order_id)->update([
                        'order_status_id'   => 2
                    ]);
        
                    $order_db_id=Order::select('id')->where('invoice_id', $order_id)->pluck('id');
            
                    Order_Item::whereIn('order_id', $order_db_id)->update([
                        'order_item_status_id'   => 2
                    ]);
                }
            }
        } else{
            $invoice=Invoice::where('id', $order_id)->update([
                'invoice_status_id' => 5
            ]);
            Order::where('invoice_id', $order_id)->update([
                'order_status_id'   => 7
            ]);

            $order_db_id=Order::select('id')->where('invoice_id', $order_id)->pluck('id');
    
            Order_Item::whereIn('order_id', $order_db_id)->update([
                'order_item_status_id'   => 5
            ]);
        }
        /*
        if ($transaction == 'capture') {
          // For credit card transaction, we need to check whether transaction is challenge by FDS or not
          if ($type == 'credit_card'){
            if($fraud == 'challenge'){
              // TODO set payment status in merchant's database to 'Challenge by FDS'
              // TODO merchant should decide whether this transaction is authorized or not in MAP
              echo "Transaction order_id: " . $order_id ." is challenged by FDS";
              } 
              else {
              // TODO set payment status in merchant's database to 'Success'
              echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
              }
            }
          }
        else if ($transaction == 'settlement'){
          // TODO set payment status in merchant's database to 'Settlement'
          echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
          } 
          else if($transaction == 'pending'){
          // TODO set payment status in merchant's database to 'Pending'
          echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
          } 
          else if ($transaction == 'deny') {
          // TODO set payment status in merchant's database to 'Denied'
          echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        }*/
   
    }

    public function invoice_number()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        
        // generate a pin based on 2 * 7 digits + a random character
        $id = 'SJ'
            . mt_rand(10000, 99999)
            . $characters[rand(0, strlen($characters) - 1)]
            . $characters[rand(0, strlen($characters) - 1)]
            . $characters[rand(0, strlen($characters) - 1)]
            . $characters[rand(0, strlen($characters) - 1)]
            . $characters[rand(0, strlen($characters) - 1)]
            . 'INV';
        
        // shuffle the result
        return $id;
    }
}    