<?php

namespace App\Http\Controllers\User\Product;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Http\Models\Temp_Product_Pic;
use App\Http\Models\Temp_Product_Pic_Deleted;
use Illuminate\Support\Facades\Storage;
use File;


class DropzoneCtrl extends Controller
{
    
    public function uniqueImageName($user_id, $imageName, $ext)
    {
        $imageUnique=md5($imageName . time());

        return $user_id.'_'.$imageUnique.'.'.$ext;
    }

    public function uploadFiles(Request $request) 
    {
        $user_id = Auth::user()->id;
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        
        $imageFinal=$this->uniqueImageName($user_id, $imageName, $image->getClientOriginalExtension());

        if($image->move(public_path('assets/images/temp'),$imageFinal)) {
            $temp = new Temp_Product_Pic([
                'user_id'=>$user_id,
                'name'=>$imageFinal
            ]);
            $temp->save();

            return response()->json(['newName'=>$imageFinal]);
        }
        /*$input = Input::all();
 
        $rules = array(
            'file' => 'image|max:3000',
        );
 
        $validation = Validator::make($input, $rules);
 
        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }
 
        $destinationPath = 'assets/images/uploads'; // upload path
        $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
        $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
        $upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
 
        if ($upload_success) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
        */
    }

    public function delete(Request $request)
    {
        if($request->has('name'))
        {
            $name=$request->name;
            
            
            $filename = $request->name;
            unlink('assets/images/temp/'.$filename);
            Temp_Product_Pic::where('name', $filename)->delete();
        } 
        else if($request->has('oldImage'))
        {
            $oldImage=$request->oldImage;
            if(File::exists(public_path('assets\images\products\\'.Auth::id().'\\'.$oldImage))) 
            {
                File::move(public_path('assets\images\products\\'.Auth::id().'\\'.$oldImage), public_path('assets\images\temp_deleted\\'.$oldImage));
                $temp_deleted= new Temp_Product_Pic_Deleted([
                    'product_id'=>$request->product_id,
                    'user_id'=>Auth::id(),
                    'name'=>$oldImage
                ]);
                $temp_deleted->save();
            } 
        }
        else
        {
            $user_id=Auth::user()->id;
            $filename=$user_id.'_*.*';
            array_map('unlink', glob('assets/images/temp/'.$filename));

            $files=glob('assets/images/temp_deleted/'.$user_id.'_*.*');
            foreach($files as $file){
                rename($file, 'assets/images/products/'.$user_id.'/'.basename($file));
            }
            Temp_Product_Pic::where('user_id', $user_id)->delete();
            Temp_Product_Pic_Deleted::where('user_id', $user_id)->delete();
        }
        
    }
    
}
