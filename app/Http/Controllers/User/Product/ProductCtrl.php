<?php

namespace App\Http\Controllers\User\Product;

use App\Http\Models\Product;
use App\Http\Models\Product_Image;
use App\Http\Models\Product_Category;
use App\Http\Models\Product_Subcategory;
use App\Http\Models\Discount;
use App\Http\Models\Commission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use File;
use Datatables;
use Illuminate\Support\Facades\DB;

class ProductCtrl extends Controller
{

    public function __construct()
    {
        $this->middleware('user');
    }

    //Product list
    public function index()
    {
        return view('user/product/product');
    }

    //Store Product
    public function createView()
    {
        $category=Product_Category::all();
        $commission=Commission::all();
        return view('user/product/new_product', ['categories'=>$category, 'commission'=>$commission]);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        if($this->moveImages() != false)
        {
            $product=$this->createProduct($request->all());
            
            if($request->has('discount') || $request->has('expire'))
                $this->createDiscount($product->id, $request->all());
    
    
            return redirect('my_products');
        }
        else
        return back()->withErrors('Barang harus mempunyai minimal 1 gambar');
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'name'=>'required|string|max:191',
            'category'=>'required',
            'subcategory'=>'required',
            'condition'=>'required',
            'weight'=>'required',
            'quantity'=>'required',
            'price'=>'required',
            'discount'  => 'sometimes|integer|max:90|min:1',
            'expire'    => 'sometimes|date',
            'description'=>'required|string|min:30',
        ]);
    }

    public function createProduct(array $data)
    {
        $user_id=Auth::user()->id;
        return Product::create([
            'name'=>$data['name'],
            'product_categories_id'=>$data['category'],
            'product_subcategories_id'=>$data['subcategory'],
            'condition'=>$data['condition'],
            'weight'=>$data['weight'],
            'quantity'=>$data['quantity'],
            'price'=>$data['price'],
            'description'=>$data['description'],
            'user_id'=>$user_id,
            'store_id'=>Auth::user()->store->id
        ]);
    }

    public function createDiscount($id, array $data)
    {
        return Discount::create([
            'product_id'    => $id,
            'discount'      => $data['discount'],
            'expire'        => $data['expire']
        ]);
    }

    public function updateDiscount($id, array $data)
    {
        return Discount::where('product_id', $id)->update([
            'discount'      => $data['discount'],
            'expire'        => $data['expire']
        ]);
    }

    public function moveImages()
    {
        $user_id=Auth::user()->id;

        $files=glob('assets/images/temp/'.$user_id.'_*.*');
        if(count($files)!=0)
        {
            if(file_exists('assets/images/products/'.$user_id.'/')){
                foreach($files as $file){ // iterate files
                    rename($file, 'assets/images/products/'.$user_id.'/' . basename($file)); // move file
                }
            } else{
                mkdir('assets/images/products/'.$user_id);
                foreach($files as $file){ // iterate files
                    rename($file, 'assets/images/products/'.$user_id.'/' . basename($file)); // move file
                }
            }
            return true;
        }
        else
            return false;
        
    }


    //Update Product
    public function editView($product_id)
    {
        $user_id=Auth::user()->id;
        

        $item=Product::where('id', $product_id)->where('user_id', $user_id)->with('product_category','product_subcategory','discount_expire')->with(['product_image' => function($query) {
            $query->select('product_id','name');
        }])->first();

        $categories=Product_Category::all();
        $subcategories=Product_Subcategory::where('parent_category_id', $item->product_category->id)->get();

        $commission=Commission::all();

        return view('user/product/edit_product', ['item'=>$item,'categories'=>$categories,'subcategories'=>$subcategories, 'commission'=>$commission]);
    }

    public function update(Request $request, $product_id)
    {
        $this->validator($request->all())->validate();

        Product::where('id', $product_id)->update([
            'product_categories_id'=> $request->category,
            'product_subcategories_id'=> $request->subcategory,
            'name' => $request->name,
            'quantity' => $request->quantity,
            'condition' => $request->condition,
            'price' => $request->price,
            'weight' => $request->weight,
            'description' => $request->description
        ]);

        if($request->has('discount') || $request->has('expire'))
        {
            if(Discount::select('product_id')->where('product_id', $product_id)->count()>0)
                $this->updateDiscount($product_id, $request->all());
            else
                $this->createDiscount($product_id, $request->all());
        }
        else
        {
            if(Discount::select('product_id')->where('product_id', $product_id)->count()>0)
                Discount::where('product_id', $product_id)->delete();
        }
        
        $this->moveImages();

        $user_id=Auth::id();
        $files=glob('assets/images/temp_deleted/'.$user_id.'_*.*');
        foreach($files as $file){
            unlink($file);
        }

        return redirect('my_products');

    }

    public function getServerImages($product_id)
    {
        $images = Product_Image::select('name')->where('product_id',$product_id)->get();

        $imageAnswer = [];

        foreach ($images as $image) {
            $imageAnswer[] = [
                'name' => $image->name,
                'size' => File::size(public_path('assets\images\products\\'.Auth::id().'\\'.$image->name))
            ];
        }

        return response()->json([
            'images' => $imageAnswer
        ]);
    }

    public function datatables()
    {
        /*$products=Product::select('products.id as id','products.name as name')->with('product_image_first')->whereHas('product_image_first', function($q){
            $q->select('name as image');
        })->where('products.user_id', Auth::id())->get();*/
        
        $products = DB::table('products')
        ->join('product_image','products.id','=','product_image.product_id')
        ->join('product_categories','products.product_categories_id','=','product_categories.id')
        ->join('product_subcategories','products.product_subcategories_id','=','product_subcategories.id')
        ->where('products.user_id',Auth::id())
        ->select('products.id as id','products.name as name','product_image.name as image', 'product_categories.slug as cat', 'product_subcategories.slug as subslug', 'product_subcategories.description as subcat', 'products.quantity as qty', 'products.condition as condition', 'created_at')
        ->groupBy('products.id');
        return Datatables::querybuilder($products)->make(true);
    }

    public function multiDelete(Request $request)
    {
        $data = $request->id;
        $user_id=$request->user_id;
        $image_name=Product_Image::select('name')->where('product_id',$data)->pluck('name')[0];
        unlink('assets/images/products/'.$user_id.'/'.$image_name);
        Product::destroy($data);
    }

    public function getSubcategory(Request $request)
    {
        $kat_id=$request->id;
        $data=Product_Subcategory::where('parent_category_id', $kat_id)->get();

        foreach($data as $d){
            echo '<option value="'.$d->id.'">'.$d->description.'</option>';
        }
    }

    
}
