<?php

namespace App\Http\Controllers\User\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

use App\Http\Models\Product;
use App\Http\Models\Product_Image;
use App\Http\Models\Product_Category;
use App\Http\Models\Product_Subcategory;

class SortProductCtrl extends Controller
{

    public function showByCategory(Request $request, $category)
    {

        $filters=collect($request->only(['condition', 'search', 'sort', 'min', 'max', 'discount', 'rating']));

        $products=Product::select('products.id', 'user_id', 'name', 'products.price', 'products.quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first','review_rating','discount', 'users.user_name', 'users.address')
        ->where(function($q) use($filters){
            if($filters->get('search')!=null)
                return $q->where('name', 'like', '%'.$filters->get('search').'%');
        })
        ->where(function($q) use($filters){
            if($filters->get('condition')!=null)
                return $q->where('condition', $filters->get('condition'));
        })
        ->where(function($q) use($filters){
            if($filters->get('min')!=null)
                return $q->where('products.price', '>=', $filters->get('min'));
        })
        ->where(function($q) use($filters){
            if($filters->get('max')!=null)
                return $q->where('products.price', '<=', $filters->get('max'));
        })
        ->when($filters->get('sort')!=null, function($q) use($filters){
            switch($filters->get('sort'))
            {
                case 'new':
                $field='created_at';
                $sort='desc';
                break;

                case 'cheap':
                $field='price';
                $sort='asc';
                break;

                case 'expensive':
                $field='price';
                $sort='desc';
                break;

                case 'popular':
                return $q->leftJoin('visitor_registry', 'products.id' ,'=', 'visitor_registry.product_id')
                ->groupBy('products.id')
                ->orderBy(DB::raw('count(ip)'), 'desc');
                break;

                case 'bestselling':
                return $q->join('order_items', 'products.id', 'order_items.product_id')
                ->groupBy('order_items.product_id')
                ->orderBy(DB::raw('count(order_items.product_id)'), 'desc');
                break;

                default:
                return 'Error 404';
                break;
            }
            return $q->orderBy($field, $sort);
        })
        ->when($filters->get('sort')==null, function($q) use($filters){
            return $q->orderBy('created_at', 'desc');
        })
        ->when($filters->get('discount')=='true', function($q) use($filters){
            return $q->whereHas('discount');
        })
        ->whereHas('product_category', function($query) use ($category) {
            $query->select('slug')->where('slug', $category);
        })
        ->when($filters->get('rating')!=null, function($q) use($filters){
            $rating=explode('-', $filters->get('rating'));
            $from=$rating[0];
            $to=$rating[1];
            return $q->whereHas('review_rating', function($q) use($from, $to){
                $q->havingRaw('avg(rating) >= '.$from.' && avg(rating) <= '.$to);
            });
        })
        ->whereHas('store_status', function($query) {
            $query->select('closed')->where('closed', 0);
        })->paginate(20);
        
        $categories=Product_Category::select('id','description','slug')->where('slug', $category)->get();
        
        $category=Product_Category::select('description','slug')->where('slug', $category)->first();

        return view('user/product/show_product', ['products'=>$products, 'kategori'=>$categories, 'category'=>$category, 'subcategory'=>null]);
    }

    public function showBySubcategory(Request $request, $category, $subcategory)
    {
        $filters=collect($request->only(['condition', 'search', 'sort', 'min', 'max', 'discount', 'rating']));
        
        $products=Product::select('products.id', 'user_id', 'name', 'products.price', 'products.quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first','review_rating','discount', 'users.user_name', 'users.address')
        ->where(function($q) use($filters){
            if($filters->get('search')!=null)
                return $q->where('name', 'like', '%'.$filters->get('search').'%');
        })
        ->where(function($q) use($filters){
            if($filters->get('condition')!=null)
                return $q->where('condition', $filters->get('condition'));
        })
        ->where(function($q) use($filters){
            if($filters->get('min')!=null)
                return $q->where('products.price', '>=', $filters->get('min'));
        })
        ->where(function($q) use($filters){
            if($filters->get('max')!=null)
                return $q->where('products.price', '<=', $filters->get('max'));
        })
        ->when($filters->get('sort')!=null, function($q) use($filters){
            switch($filters->get('sort'))
            {
                case 'new':
                $field='created_at';
                $sort='desc';
                break;

                case 'cheap':
                $field='price';
                $sort='asc';
                break;

                case 'expensive':
                $field='price';
                $sort='desc';
                break;

                case 'popular':
                return $q->leftJoin('visitor_registry', 'products.id' ,'=', 'visitor_registry.product_id')
                ->groupBy('products.id')
                ->orderBy(DB::raw('count(ip)'), 'desc');
                break;

                case 'bestselling':
                return $q->join('order_items', 'products.id', 'order_items.product_id')
                ->groupBy('order_items.product_id')
                ->orderBy(DB::raw('count(order_items.product_id)'), 'desc');
                break;

                default:
                return 'Error 404';
                break;
            }
            return $q->orderBy($field, $sort);
        })
        ->when($filters->get('sort')==null, function($q) use($filters){
            return $q->orderBy('created_at', 'desc');
        })
        ->when($filters->get('discount')=='true', function($q) use($filters){
            return $q->whereHas('discount');
        })
        ->whereHas('product_category', function($query) use ($category) {
            $query->select('slug')->where('slug', $category);
        })
        ->whereHas('product_subcategory', function($query) use ($subcategory) {
            $query->select('slug')->where('slug', $subcategory);
        })
        ->when($filters->get('rating')!=null, function($q) use($filters){
            $rating=explode('-', $filters->get('rating'));
            $from=$rating[0];
            $to=$rating[1];
            return $q->whereHas('review_rating', function($q) use($from, $to){
                $q->havingRaw('avg(rating) >= '.$from.' && avg(rating) <= '.$to);
            });
        })
        ->whereHas('store_status', function($query) {
            $query->select('closed')->where('closed', 0);
        })->paginate(20);

        $categories=Product_Category::select('id','description','slug')->with('product_subcategory')->where('slug', $category)->get();

        $category=Product_Category::select('description','slug')->where('slug', $category)->first();
        $subcategory=Product_Subcategory::select('description','slug')->where('slug', $subcategory)->first();
        
        return view('user/product/show_product', ['products'=>$products, 'kategori'=>$categories, 'category'=>$category, 'subcategory'=>$subcategory]);

    }
}
