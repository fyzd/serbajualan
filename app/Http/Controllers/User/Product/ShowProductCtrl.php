<?php

namespace App\Http\Controllers\User\Product;

use App\Http\Models\User;
use App\Http\Models\Product;
use App\Http\Models\Invoice;
use App\Http\Models\Order;
use App\Http\Models\Order_Item;
use App\Http\Models\Review;
use App\Http\Models\Product_Image;
use App\Http\Models\Product_Category;
use App\Http\Models\Product_Subcategory;
use App\Http\Models\Commission;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Visitor;
use RajaOngkir;
use Carbon\Carbon;

use App\Http\Traits\OngkirTrait;

class ShowProductCtrl extends Controller
{
    use OngkirTrait;

    protected $products;
    
    public function index()
    {
        $featured=Product::select('id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first', 'review_rating', 'discount', 'users.user_name', 'users.address')->whereHas('store_status', function($query) {
            $query->select('closed')->where('closed', 0);
        })->orderBy('created_at', 'desc')->limit(12)->get();
        
        $popular=Product::select('products.id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first', 'review_rating','discount', 'users.user_name', 'users.address')
        ->leftJoin('visitor_registry', 'products.id' ,'=', 'visitor_registry.product_id')
        ->groupBy('products.id')
        ->where('visitor_registry.created_at', '>=', Carbon::now()->subWeeks(1))
        ->orderBy(DB::raw('count(ip)'), 'desc')
        ->whereHas('store_status', function($query) {
            $query->select('closed')->where('closed', 0);
        })
        ->limit(8)->get();

        $bestselling=Order_Item::select('product_id')->with('products.product_category','products.product_subcategory','products.product_image_first', 'products.review_rating', 'products.discount', 'products.users.user_name', 'products.users.address')
        ->whereHas('orders', function($query) {
            $query->select('created_at')->where('created_at', '>=', Carbon::now()->subWeeks(1));
        })
        ->whereHas('products.store_status', function($query) {
            $query->select('closed')->where('closed', 0);
        })
        ->where('order_item_status_id', 3)
        ->groupBy('product_id')
        ->orderBy(DB::raw('count(product_id)'), 'desc')
        ->limit(12)->get();

        $discount=Product::select('id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first', 'review_rating', 'discount', 'users.user_name', 'users.address')
        ->whereHas('discount')
        ->whereHas('store_status', function($query) {
            $query->select('closed')->where('closed', 0);
        })->inRandomOrder()->orderBy('created_at', 'desc')->limit(12)->get();

        $muchreviewed=Review::select('product_id')->with('products.product_category','products.product_subcategory','products.product_image_first', 'products.review_rating', 'products.discount', 'products.users.user_name', 'products.users.address')
        ->whereHas('products.store_status', function($query) {
            $query->select('closed')->where('closed', 0);
        })
        ->groupBy('product_id')
        ->orderBy(DB::raw('count(product_id)'), 'desc')
        ->where('created_at', '>=', Carbon::now()->subWeeks(1))
        ->limit(12)->get();

        return view('user/home', [
            'featured'      => $featured, 
            'popular'       => $popular, 
            'bestselling'   => $bestselling, 
            'discount'      => $discount,
            'muchreviewed'  => $muchreviewed
        ]);
    }

    public function showProduct(Request $request)
    {
        $filters=collect($request->only(['condition', 'search', 'sort', 'min', 'max', 'discount','rating']));

        $products=Product::select('products.id', 'user_id', 'name', 'products.price', 'products.quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first','review_rating','discount', 'users.user_name', 'users.address')
        ->where(function($q) use($filters){
            if($filters->get('search')!=null)
                return $q->where('name', 'like', '%'.$filters->get('search').'%');
        })
        ->where(function($q) use($filters){
            if($filters->get('condition')!=null)
                return $q->where('condition', $filters->get('condition'));
        })
        ->where(function($q) use($filters){
            if($filters->get('min')!=null)
                return $q->where('products.price', '>=', $filters->get('min'));
        })
        ->where(function($q) use($filters){
            if($filters->get('max')!=null)
                return $q->where('products.price', '<=', $filters->get('max'));
        })
        ->when($filters->get('sort')!=null, function($q) use($filters){
            switch($filters->get('sort'))
            {
                case 'new':
                $field='created_at';
                $sort='desc';
                break;

                case 'cheap':
                $field='price';
                $sort='asc';
                break;

                case 'expensive':
                $field='price';
                $sort='desc';
                break;

                case 'popular':
                return $q->leftJoin('visitor_registry', 'products.id' ,'=', 'visitor_registry.product_id')
                ->groupBy('products.id')
                ->orderBy(DB::raw('count(ip)'), 'desc');
                break;

                case 'bestselling':
                return $q->join('order_items', 'products.id', 'order_items.product_id')
                ->groupBy('order_items.product_id')
                ->orderBy(DB::raw('count(order_items.product_id)'), 'desc');
                break;

                default:
                return 'Error 404';
                break;
            }
            return $q->orderBy($field, $sort);
        })
        ->when($filters->get('sort')==null, function($q) use($filters){
            return $q->orderBy('created_at', 'desc');
        })
        ->when($filters->get('discount')=='true', function($q) use($filters){
            return $q->whereHas('discount');
        })
        ->when($filters->get('rating')!=null, function($q) use($filters){
            $rating=explode('-', $filters->get('rating'));
            $from=$rating[0];
            $to=$rating[1];
            return $q->whereHas('review_rating', function($q) use($from, $to){
                $q->havingRaw('avg(rating) >= '.$from.' && avg(rating) <= '.$to);
            });
        })
        ->whereHas('store_status', function($query) {
            $query->select('closed')->where('closed', 0);
        })
        ->paginate(20);
        
        $categories=Product_Category::select('id','description','slug')->with('product_subcategory')->withCount('product')->get();

        return view('user/product/show_product', ['products'=>$products, 'kategori'=>$categories]);
    }

    public function detail($category, $subcategory, $product_id)
    {
        $product=Product::with('product_category','product_subcategory','product_image','discount','reviews.users.user_name','users.user_info','users.address','users.courier','reviews.users.user_image', 'stores')->withCount('sold')->withCount('visitor')->where('id', $product_id)->whereHas('product_category', function($q) use ($category){
            $q->where('slug', $category);
        })->whereHas('product_subcategory', function($q) use ($subcategory){
            $q->where('slug', $subcategory);
        })->first();

        if($product->discount!=null && $product->discount->expire > date('Y-m-d H:i:s'))
            $product['price_after_disc']=$product->price - (round($product->price * ($product->discount->discount/100)));
        else
            $product['price_after_disc']=$product->price;

        $customer=Invoice::select('buyer_id')->whereHas('order.order_item.products', function($q) use($product){
            $q->select('user_id')->where('user_id', $product->users->id);
        })->groupBy('buyer_id')->get()->count();

        $related=Product::select('id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first','review_rating','discount', 'users.user_name', 'users.address')->where('id', '!=', $product_id)->whereHas('product_subcategory', function($q) use ($subcategory){
            $q->where('slug', $subcategory);
        })->limit(8)->inRandomOrder()->get();

        $terima=Order::select('order_status_id')->whereHas('order_item.products', function($q)use($product){
            $q->where('user_id', $product->users->id);
        })->whereIn('order_status_id', [3,4])->count();
        $semua=Order::select('order_status_id')->whereHas('order_item.products', function($q)use($product){
            $q->where('user_id', $product->users->id);
        })->whereNotIn('order_status_id', [1,2,6,7])->count();

        $commission=Commission::all();
        $harga=$product['price_after_disc'];

        if($harga<$commission[0]->higherorequalto){}
        else{
            foreach($commission as $c){
                if($harga>=$c->higherorequalto)
                    $t=$harga-$c->cost;
            }
        }
        
        
        if($product==null)
            return 'Error 404';
            
        Visitor::log($product_id);
        return view('user/product/product_detail', ['product'=> $product, 'related'=>$related, 'customer'=>$customer, 'terima'=>$terima, 'semua'=>$semua, 'harga'=>$t]);
    }

    public function changeFeatured(Request $request)
    {
        if($request->id=='all')
        {
            $featured=Product::select('id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first', 'review_rating', 'discount', 'users.user_name', 'users.address')->whereHas('store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })->orderBy('created_at', 'desc')->limit(8)->inRandomOrder()->get();
        }
        else
        {
            $featured=Product::select('id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category', 'product_subcategory', 'product_image_first', 'review_rating', 'discount', 'users.user_name', 'users.address')
            ->whereHas('product_category', function($q) use ($request){
                return $q->where('slug', $request->id);
            })->whereHas('store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })->orderBy('created_at', 'desc')->limit(8)->get();
        }
        

        return view('user.templates.t_featured_product')->with(['featured' => $featured]);
    }

    public function changePopular(Request $request)
    {
        if($request->id=='all')
        {
            $popular=Product::select('products.id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first', 'review_rating', 'discount', 'users.user_name', 'users.address')
            ->leftJoin('visitor_registry', 'products.id' ,'=', 'visitor_registry.product_id')
            ->groupBy('products.id')
            ->where('visitor_registry.created_at', '>=', Carbon::now()->subWeeks(1))
            ->orderBy(DB::raw('count(ip)'), 'desc')
            ->whereHas('store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })
            ->limit(8)->get();
        }
        else
        {
            $popular=Product::select('products.id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first', 'review_rating', 'discount', 'users.user_name', 'users.address')
            ->leftJoin('visitor_registry', 'products.id' ,'=', 'visitor_registry.product_id')
            ->groupBy('products.id')
            ->where('visitor_registry.created_at', '>=', Carbon::now()->subWeeks(1))
            ->orderBy(DB::raw('count(ip)'), 'desc')
            ->whereHas('product_category', function($q) use ($request){
                return $q->where('slug', $request->id);
            })
            ->whereHas('store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })
            ->limit(8)->get();
        }
        

        return view('user.templates.t_popular_product')->with(['popular' => $popular]);
    }

    public function changeBestSelling(Request $request)
    {
        if($request->id=='all')
        {
            $bestselling=Order_Item::select('product_id')->with('products.product_category','products.product_subcategory','products.product_image_first', 'products.review_rating', 'products.discount', 'products.users.user_name', 'products.users.address')
            ->whereHas('orders', function($query) {
                $query->select('created_at')->where('created_at', '>=', Carbon::now()->subWeeks(1));
            })
            ->whereHas('products.store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })
            ->where('order_item_status_id', 3)
            ->groupBy('product_id')
            ->orderBy(DB::raw('count(product_id)'), 'desc')
            ->limit(12)->get();
        }
        else
        {
            $bestselling=Order_Item::select('product_id')->with('products.product_category','products.product_subcategory','products.product_image_first', 'products.review_rating', 'products.discount', 'products.users.user_name', 'products.users.address')
            ->whereHas('orders', function($query) {
                $query->select('created_at')->where('created_at', '>=', Carbon::now()->subWeeks(1));
            })
            ->whereHas('products.store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })
            ->where('order_item_status_id', 3)
            ->groupBy('product_id')
            ->orderBy(DB::raw('count(product_id)'), 'desc')
            ->whereHas('products.product_category', function($q) use ($request){
                return $q->where('slug', $request->id);
            })
            ->limit(12)->get();
        }

        return view('user.templates.t_bestselling_product')->with(['bestselling' => $bestselling]);
    }

    public function changeDiscount(Request $request)
    {
        if($request->id=='all')
        {
            $discount=Product::select('id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first', 'review_rating', 'discount', 'users.user_name', 'users.address')
            ->whereHas('discount')
            ->whereHas('store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })->inRandomOrder()->limit(12)->get();
        }
        else
        {
            $discount=Product::select('id', 'user_id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first', 'review_rating', 'discount', 'users.user_name', 'users.address')
            ->whereHas('discount')
            ->whereHas('store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })
            ->whereHas('product_category', function($q) use ($request){
                return $q->where('slug', $request->id);
            })->inRandomOrder()->limit(12)->get();
        }

        return view('user.templates.t_discount_product')->with(['discount' => $discount]);
    }

    public function changeMuchReviewed(Request $request)
    {
        if($request->id=='all')
        {
            $muchreviewed=Review::select('product_id')->with('products.product_category','products.product_subcategory','products.product_image_first', 'products.review_rating', 'products.discount', 'products.users.user_name', 'products.users.address')
            ->whereHas('products.store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })
            ->groupBy('product_id')
            ->orderBy(DB::raw('count(product_id)'), 'desc')
            ->where('created_at', '>=', Carbon::now()->subWeeks(1))
            ->limit(12)->get();
        }
        else
        {
            $muchreviewed=Review::select('product_id')->with('products.product_category','products.product_subcategory','products.product_image_first', 'products.review_rating', 'products.discount', 'products.users.user_name', 'products.users.address')
            ->whereHas('products.store_status', function($query) {
                $query->select('closed')->where('closed', 0);
            })
            ->groupBy('product_id')
            ->orderBy(DB::raw('count(product_id)'), 'desc')
            ->where('created_at', '>=', Carbon::now()->subWeeks(1))
            ->whereHas('products.product_category', function($q) use ($request){
                return $q->where('slug', $request->id);
            })
            ->limit(12)->get();
        }

        return view('user.templates.t_muchreviewed_product')->with(['muchreviewed' => $muchreviewed]);
    }

    public function estimatedCost(Request $request)
    {
        $product_id=$request->product_id;
        $weight=$request->weight;
        $kota_asal=$request->kota_asal;
        $kurir=$request->kurir;
        $price=$request->price;

        $provinsi=RajaOngkir::Provinsi()->all();
        return view('user.templates.t_estimated_cost', ['provinsi'=>$provinsi, 'product_id'=>$product_id, 'weight'=>$weight,'kota_asal'=>$kota_asal, 'kurir'=>$kurir, 'price'=>$price]);
    }
    
    public function roGetKota(Request $request)
    {
        $prov_id=$request->prov_id;
        $data = RajaOngkir::Kota()->byProvinsi($prov_id)->get();

        foreach($data as $d){
            echo '<option value="'.$d['city_id'].'">'.$d['city_name'].'</option>';
        }
        
    }

    public function roTotal(Request $request)
    {
        $kota_asal=$request->kota_asal;
        $product_id=$request->product_id;
        $weight=$request->weight;
        $prov_id=$request->prov_id;
        $kota_id=$request->kota_id;
        $courier=$request->kurir;
        $namakota=$request->namakota;
        $quantity=$request->quantity;
        $price=$request->price;

        $weight*=$quantity;
        $price*=$quantity;

        $api_kota=RajaOngkir::Kota()->all();
        
        $kurir=[];

        if($courier['jne']==1)
            array_push($kurir, 'jne');
        if($courier['tiki']==1)
            array_push($kurir, 'tiki');
        if($courier['pos']==1)
            array_push($kurir, 'pos');

        foreach ($kurir as $k)
        {
            $data[] = RajaOngkir::Cost([
                'origin' 		=> $this->getIdKota($kota_asal, $api_kota), // id kota asal
                'destination' 	=> $kota_id, // id kota tujuan
                'weight' 		=> $weight, // berat satuan gram
                'courier' 		=> $k, // kode kurir pengantar ( jne / tiki / pos )
            ])->get();
        }

        return view('user.templates.t_estimated_cost_value', ['data'=>$data, 'namakota'=>$namakota, 'price'=>$price]);
    }
}
