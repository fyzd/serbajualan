<?php

namespace App\Http\Controllers\User\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use File;
use Hash;
use Mail;
use Validator;

use App\Http\Models\User;
use App\Http\Models\Product;
use App\Http\Models\Provinsi;
use App\Http\Models\Kota;
use App\Http\Models\Kecamatan;
use App\Http\Models\User_Address;
use App\Http\Models\User_Information;
use App\Http\Models\Store;
use App\Http\Models\Invoice;
use App\Http\Models\Order;
use App\Http\Models\Order_Item;
use App\Http\Models\Shipment;
use App\Http\Models\Review;
use App\Http\Models\Wallet;
use App\Http\Models\Wallet_Mutation;

use App\Http\Models\Seller_Courier;

class AccountCtrl extends Controller
{
    protected $provinsi;

    public function __construct()
    {
        $this->middleware('user')->except('storeView');
    }
    public function index()
    {
        $invoices=Invoice::with('invoice_status')->where('buyer_id', Auth::id())->orderBy('created_at', 'desc')->limit(5)->get();
        $orders=Order::with('order_status')->whereHas('order_item.products', function($q){
            $q->where('user_id', Auth::id());
        })->orderBy('created_at', 'desc')->limit(5)->get();
        $address=User_Address::with('provinsi', 'kota', 'kecamatan')->where('user_id', Auth::id())->where('primary', 1)->first();

        return view('user.account.account', [
            'address'   => $address,
            'invoices'   => $invoices,
            'orders'    => $orders
        ]);
    }

    public function newAddressView()
    {
        $provinsi=Provinsi::all();
        return view('user.account.new_address', ['provinsi' => $provinsi]);
    }

    public function validator($request)
    {
        return $this->validate($request, [
            'name'      => 'required',
            'pp'        => 'required',
            'phone'     => 'required|numeric|digits_between:6,12',
            'prov'      => 'required',
            'kota'      => 'required',
            'kec'       => 'required',
            'address'   => 'required|min:20',
            'zip'       => 'required|numeric|digits:5'
        ]);
    }

    public function newAddress(Request $request)
    {
        $this->validator($request);
        
        $address= new User_Address;
        $address->user_id   = Auth::id();
        $address->name      = $request->name;
        $address->pp        = $request->pp;
        $address->phone     = $request->phone;
        $address->prov_id   = $request->prov;
        $address->kota_id   = $request->kota;
        $address->kec_id    = $request->kec;
        $address->address   = $request->address;
        $address->zip_code  = $request->zip;

        $cek=User_Address::select('user_id')->where('user_id', Auth::id())->first();
        
        count($cek) == 0 ? $address->primary = 1 : $address->primary = 0;
        
        $address->save();

        return redirect('user_addresses');
    }

    public function updateAddressView($id)
    {
        $provinsi=Provinsi::all();
        $address=User_Address::where('id', $id)
                    ->with(['provinsi', 'kota', 'kecamatan'])
                    ->first();
        $kota=Kota::where('provinsi_id', $address['kota']->provinsi_id)->get();
        $kecamatan=Kecamatan::where('kota_id', $address['kecamatan']->kota_id)->get();

        return view('user.account.update_address', [
            'provinsi' => $provinsi, 
            'address' => $address, 
            'kota' => $kota, 
            'kecamatan' => $kecamatan
        ]);
    }

    public function updateAddress(Request $request, $id)
    {
        $this->validator($request);

        User_Address::where('id', $id)->update([
            'name'      => $request->name,
            'pp'        => $request->pp,
            'phone'     => $request->phone,
            'prov_id'   => $request->prov,
            'kota_id'   => $request->kota,
            'kec_id'    => $request->kec,
            'address'   => $request->address,
            'zip_code'  => $request->zip
        ]);

        return redirect('user_addresses');
    }

    public function deleteAddress($id)
    {
        User_Address::where('id', $id)->delete();

        return redirect('user_addresses');
    }

    public function getKota($prov_id)
    {
        return Kota::where('provinsi_id', $prov_id)->get();
    }

    public function getKecamatan($kota_id)
    {
        return Kecamatan::where('kota_id', $kota_id)->get();
    }

    //Courier Settings
    public function courierSettingView()
    {
        $kurir=Seller_Courier::where('user_id', Auth::id())->first();
        return view('user.account.courier_setting', ['kurir' => $kurir]);
    }

    public function courierSetting(Request $request)
    {
        $this->validate($request, [
            'jne'   => 'required_without_all:pos,tiki',
            'pos'   => 'required_without_all:jne,tiki',
            'tiki'  => 'required_without_all:pos,jne'
        ]);
        
        Seller_Courier::where('user_id', Auth::id())->update([
            'jne'   => !empty($request->jne) ? $request->jne : '0',
            'pos'   => !empty($request->pos) ? $request->pos : '0',
            'tiki'  => !empty($request->tiki) ? $request->tiki : '0'
        ]);
        
        return redirect()->back();
    }

    public function settingHome()
    {
        $data=User_Information::where('user_id', Auth::id())->first();
        return view('user.account.setting_home')->with('data', $data);
    }

    public function settingAddress()
    {
        $addresses=User_Address::with(['kecamatan', 'kota', 'provinsi'])->where('user_id', Auth::id())->orderBy('primary', 'desc')->get();

        return view('user.account.setting_address', ['addresses' => $addresses]);
    }

    public function updatePrimaryAddress(Request $request)
    {
        $id=$request->id;

        User_Address::where('user_id', Auth::id())->update(['primary' => 0]);
        User_Address::where('id', $id)->update(['primary' => 1]);

        return redirect('user_addresses');
    }

    public function transactionView()
    {
        $invoice=Invoice::with('order.order_item.products.product_image_first', 'order.order_item.products.product_category', 'order.order_item.products.product_subcategory', 'invoice_status',  'order.order_item.reviews', 'order.order_item.products.stores.users.user_name')->where('buyer_id', Auth::id())->orderBy('created_at', 'desc')->paginate(5);
        
        return view('user.account.setting_transaction', ['invoices' => $invoice]);
    }

    public function transactionDetail($invoice_number)
    {
        $barang=0;
        $kirim=0;
        $barangtmp=0;
        $barangtmp2=0;
        $barangtmp3=0;

        $invoice=Invoice::with('users.user_info','order.order_item.products.product_image_first', 'order.order_item.products.product_category', 'order.order_item.products.product_subcategory', 'order.shipments.buyer_address.kecamatan','order.shipments.buyer_address.kota','order.shipments.buyer_address.provinsi', 'invoice_status', 'order.order_status')->where('invoice_number', $invoice_number)->first();
        
        foreach($invoice->order as $o)
        {
            foreach($o->order_item as $order_item)
            {
                $barangtmp=$order_item->price;
                $barangtmp2+=$barangtmp;
                $barangtmp=0;
            }
            $barangtmp3=$barangtmp2;
            
            $kirim+=$o->shipments->cost;
            
        }
        $barang+=$barangtmp3;
        $total=$barang+$kirim;
        
        return view('user.account.transaction_detail')->with(['invoice'=>$invoice, 'barang'=>$barang, 'kirim'=>$kirim, 'total'=>$total]);
    }

    public function receivedConfirmation(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required'
        ]);

        Order::where('id', $request->order_id)->update([
            'order_status_id'  => 4
        ]);

        Order_Item::where('order_id', $request->order_id)->update([
            'order_item_status_id'  => 3
        ]);

        return back();
    }

    public function addReviewView($order_id, $order_item_id)
    {
        $product=Order_Item::select('id', 'product_id')->with('products.product_image_first', 'products.product_category', 'products.product_subcategory')->where('id', $order_item_id)->first();

        return view('user.account.modal_add_review', ['product' => $product, 'order_id'=>$order_id]);
    }

    public function addReview(Request $request)
    {
        $v = Validator::make($request->all(), [
            'order_id'      => 'required',
            'order_item_id' => 'required',
            'product_id'    => 'required',
            'rating'        => 'required',
            'comment'       => 'required'
        ]);

        if ($v->fails())
        {
            return response()->json(['error'=>$v->errors()]);
        }

        Review::create([
            'order_item_id' => $request->order_item_id,
            'product_id'    => $request->product_id,
            'reviewers'     => Auth::id(),
            'rating'        => $request->rating,
            'comment'       => $request->comment
        ]);

        return $request->order_id;
    }

    public function changeReviewView($order_id, $order_item_id)
    {
        $review=Review::with('products.product_image_first', 'products.product_category', 'products.product_subcategory')->where('order_item_id', $order_item_id)->first();

        return view('user.account.modal_change_review', ['review' => $review, 'order_id'=>$order_id]);
    }

    public function changeReview(Request $request)
    {
        $v = Validator::make($request->all(), [
            'order_id'      => 'required',
            'order_item_id' => 'required',
            'product_id'    => 'required',
            'rating'        => 'required',
            'comment'       => 'required'
        ]);

        if ($v->fails())
        {
            return response()->json(['error'=>$v->errors()]);
        }

        Review::where('order_item_id', $request->order_item_id)->update([
            'product_id'    => $request->product_id,
            'reviewers'     => Auth::id(),
            'rating'        => $request->rating,
            'comment'       => $request->comment
        ]);

        return $request->order_id;
    }

    public function saleView()
    {
        /* :D
        $order_id=Order_Item::select('order_id')->whereHas('products', function($q){
            $q->where('user_id', Auth::id());
        })->groupBy('order_id')->pluck('order_id');
        */
        
        $order=Order::select('id', 'created_at', 'order_status_id')->with('order_status','shipments.buyer_address.user.user_name','order_item.products.product_image_first', 'order_item.products.product_category', 'order_item.products.product_subcategory', 'order_status', 'order_item.reviews')
        ->whereHas('order_item.products', function($q){
            $q->where('user_id', Auth::id());
        })->whereHas('invoices', function($q){
            $q->where('invoice_status_id', 3);
        })->orderBy('created_at', 'desc')->paginate(5);
        
        return view('user.account.setting_sale', ['orders' => $order]);
    }

    public function saleDetail($id)
    {
        $barang=0;
        $kirim=0;

        $order=Order::select('id', 'created_at', 'order_status_id', 'order_details')->with('order_status','shipments','order_item.products.product_image_first', 'order_item.products.product_category', 'order_item.products.product_subcategory', 'order_status', 'order_item.reviews','shipments.buyer_address.kecamatan','shipments.buyer_address.kota','shipments.buyer_address.provinsi','shipments.buyer_address.user.user_name', 'selling_mutation', 'refund_mutation')->where('id', $id)->first();
        
        foreach($order->order_item as $o)
        {
            $barang+=$o->price;
        }
        $kirim=$order->shipments->cost;

        $total=$barang+$kirim;
        
        return view('user.account.sale_detail')->with(['orders'=>$order, 'barang'=>$barang, 'kirim'=>$kirim, 'total'=>$total]);
    }

    public function rejectOrder(Request $request)
    {
        $barang=0;
        $kirim=0;

        $this->validate($request, [
            'order_id'  => 'required',
            'reason'    => 'required|min:10|max:191'
        ]);

        Order::where('id', $request->order_id)->update([
            'order_status_id'   => 5,
            'order_details'     => $request->reason
        ]);

        $order=Order::select('id')->with('order_item', 'shipments.buyer_address.user.user_name')->where('id', $request->order_id)->first();

        return back();
    }

    public function updateTrackingNumber(Request $request)
    {
        $this->validate($request,[
            'order_id'     => 'required',
            'resi'         => 'required'
        ]);

        Shipment::where('order_id', $request->order_id)->update([
            'tracking_number'  => $request->resi
        ]);

        Order::where('id', $request->order_id)->update([
            'order_status_id'  => 3
        ]);

        return back();
    }

    public function reviewView($order_id)
    {
        $orders=Order::with('order_item.reviews','order_item.products.product_image_first', 'order_item.products.product_category', 'order_item.products.product_subcategory')->where('id', $order_id)->first();

        return view('user.account.review')->with('orders', $orders);
    }

    public function storeView(Request $request, $username)
    {
        $user_info=User::select('id', 'username', 'created_at')->with('store','product', 'address', 'courier','user_name')->where('username', $username)->first();

        $filters=collect($request->only(['search']));
        
        $products=Product::select('products.id', 'name', 'price', 'quantity', 'product_categories_id','product_subcategories_id')->with('product_category','product_subcategory','product_image_first','review_rating')
        ->where(function($q) use($filters){
            if($filters->get('search')!=null)
                return $q->where('name', 'like', '%'.$filters->get('search').'%');
        })
        ->where('user_id', $user_info->id)
        ->orderBy('created_at', 'desc')
        ->paginate(20);

        $customer=Invoice::select('buyer_id')->whereHas('order.order_item.products', function($q) use($user_info){
            $q->select('user_id')->where('user_id', $user_info->id);
        })->groupBy('buyer_id')->get()->count();

        $terima=Order::select('order_status_id')->whereHas('order_item.products', function($q)use($user_info){
            $q->where('user_id', $user_info->id);
        })->whereIn('order_status_id', [3,4])->count();
        $semua=Order::select('order_status_id')->whereHas('order_item.products', function($q)use($user_info){
            $q->where('user_id', $user_info->id);
        })->whereNotIn('order_status_id', [1,2,6,7])->count();

        return view('user.account.store')->with(['user'=>$user_info, 'products'=>$products, 'customer'=>$customer, 'terima'=>$terima, 'semua'=>$semua]);
    }

    public function storeSetting()
    {
        $data=Store::where('user_id', Auth::id())->first();
        return view('user.account.store_setting', ['data'=>$data]);
    }

    public function editView(Request $request)
    {
        switch($request->get('section')){
            case 'general':
                $data=User_Information::where('user_id', Auth::id())->first();
            break;
            case 'email':
                $data=User::select('email')->where('id', Auth::id())->first();
            break;
            case 'password':
                $data=null;
            break;
            default:
                return 'Error 404';
            break;
        }
        return view('user.account.edit_view')->with('data', $data);
    }

    public function editStoreView(Request $request)
    {
        switch($request->get('section')){
            case 'store_general':
                $data=Store::select('image', 'description')->where('user_id', Auth::id())->first();
            break;
            case 'seller_note':
                $data=Store::select('seller_note')->where('user_id', Auth::id())->first();
            break;
            default:
                return 'Error 404';
            break;
        }
        return view('user.account.edit_store_view')->with('data', $data);
    }

    public function generalUpdate(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required',
            'birth' => 'required',
            'sex'   => 'required',
            'image' => 'image|mimes:jpeg,png,gif|max:1024'
        ]);

        $ui=User_Information::where('user_id', Auth::id())->first();
        $ui->name   = $request->name;
        $ui->birth  = $request->birth;
        $ui->sex    = $request->sex;

        if($request->hasFile('image'))
        {
            $image=$request->file('image');
            $imageName=$this->uniqueImageName(Auth::id(), $image->getClientOriginalName(), $image->getClientOriginalExtension());
            
            $old_image=User_Information::select('image')->where('user_id', Auth::id())->first();
            $path=public_path('assets/images/profile/');
    
            $ui->image  = $imageName;
    
            if(File::exists($path.$old_image->image))
            {
                unlink($path.$old_image->image);
            }

            $image->move('assets/images/profile/', $imageName);
        }
            
        $ui->save();
        
        return redirect()->back();
    }

    public function emailUpdate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required'
        ]);
            
        if(!Hash::check($request->password, Auth::user()->password))
            return back()->withErrors('Password salah');
        else
        {
            $token=str_random(25);

            $user=User::find(Auth::id());
            $user->timestamps   = false;
            $user->email        = $request->email;
            $user->confirmed    = '0';
            $user->token        = $token;
            $user->save();
            
            $data=[
                'email' => $request->email,
                'name'  => Auth::user()->username,
                'token' => $token
            ];
            
            Mail::send('user.mails.confirmation', $data, function($message) use($data){
                $message->to($data['email']);
                $message->subject('Registration Confirmation');
            });
    
            return back()->with('status','Confirmation email has been send, please check your email.');
        }
        
    }

    public function passwordUpdate(Request $request)
    {
        $this->validate($request, [
            'password'      => 'required|confirmed',
            'old_password'  => 'required'
        ]);

        if(!Hash::check($request->old_password, Auth::user()->password))
            return back()->withErrors('Password lama salah');
        else
        {
            User::where('id', Auth::id())->update([
                'password' => bcrypt($request->password)
            ]);

            return back()->with('status', 'Password berhasi diperbarui.');
        }
    }

    public function storeGeneralUpdate(Request $request)
    {
        $this->validate($request, [
            'description'  => 'required|max:320',
            'image' => 'image|mimes:jpeg,png,gif|max:1024|dimensions:min_width=945,min_height=190'
        ]);
        
        $store=Store::where('user_id', Auth::id())->first();
        $store->description   = $request->description;

        if($request->hasFile('image'))
        {
            $image=$request->file('image');
            $imageName=$this->uniqueImageName(Auth::id(), $image->getClientOriginalName(), $image->getClientOriginalExtension());
            
            $old_image=Store::select('image')->where('user_id', Auth::id())->first();
            $path=public_path('assets/images/store/');
    
            $store->image  = $imageName;
    
            if(File::exists($path.$old_image->image))
            {
                unlink($path.$old_image->image);
            }

            $image->move('assets/images/store/', $imageName);
        }
            
        $store->save();
        
        return redirect()->back();
    }

    public function sellerNoteUpdate(Request $request)
    {
        $this->validate($request, [
            'note'  => 'required'
        ]);

        Store::where('user_id', Auth::id())->update([
            'seller_note'   => $request->note
        ]);

        return back()->with('status', 'Catatan Penjual berhasil diperbarui.');
    }

    public function closeStoreUpdate(Request $request)
    {
        $close=Store::where('user_id', Auth::id())->first();
        $request->status=='true' ? $close->closed = 1 : $close->closed = 0;
        $close->save();

    }

    public function uniqueImageName($user_id, $imageName, $ext)
    {
        $imageUnique=md5($imageName . time());

        return $user_id.'_'.$imageUnique.'.'.$ext;
    }

    public function dompetView()
    {
        $wallet=Wallet::where('user_id', Auth::id())->with('wallet_mutations.wallet_mutation_detail', 'wallet_mutations.invoice', 'wallet_mutations.order.invoices')->first();
        return view('user.account.wallet', ['wallet'=>$wallet]);
    }
}
