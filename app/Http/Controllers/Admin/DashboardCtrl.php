<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Http\Models\Invoice;
use App\Http\Models\Order;
use App\Http\Models\Product;
use App\Http\Models\Product_Image;
use Illuminate\Http\Request;

class DashboardCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $now= Carbon::today();
        $format = $now->format('Y-m-d');

        $maincard1=Invoice::where('created_at', 'like', $format. '%')->count();
        $maincard2=Order::where('created_at', 'like', $format. '%')->count();
        $maincard3=Invoice::count();
        $maincard4=Order::count();

        $card1=Invoice::where('invoice_status_id', 1)->count();
        $card2=Invoice::where('invoice_status_Id', 2)->count();
        $card3=Order::where('order_status_id', 2)->count();
        $card4=Order::where('order_status_id', 3)->count();
        $card5=Invoice::where('invoice_status_id', 4)->count();
        $card6=Invoice::where('invoice_status_id', 5)->count();
        $card7=Order::where('order_status_id', 5)->count();
        $card8=Order::where('order_status_id', 4)->count();
        
        return view('admin.dashboard')->with(['maincard1' => $maincard1, 'maincard2' => $maincard2, 'maincard3' => $maincard3, 'maincard4' => $maincard4, 'card1' => $card1, 'card2' => $card2, 'card3' => $card3, 'card4' => $card4, 'card5' => $card5, 'card6' => $card6, 'card7' => $card7, 'card8' => $card8]);
    }

    public function transactionDetail($order_id)
    {
        $barang=0;
        $kirim=0;

        $order=Order::with('order_item.products.stores.users.user_name', 'order_item.products.product_image_first', 'order_item.shipments.buyer_address.kecamatan','order_item.shipments.buyer_address.kota','order_item.shipments.buyer_address.provinsi')->where('id', $order_id)->first();
        
        foreach($order->order_item as $o)
        {
            $barang+=$o->price*$o->quantity;
            $kirim+=$o->shipments->cost;
        }

        $total=$barang+$kirim;
        

        return view('admin.transaction.transaction_detail')->with(['orders'=>$order, 'barang'=>$barang, 'kirim'=>$kirim, 'total'=>$total]);
    }

    public function deleteProduct(Request $request)
    {
        $product_id = $request->product_id;
        $user_id=$request->user_id;

        $image_name=Product_Image::select('name')->where('product_id',$product_id)->pluck('name');
        dd($image_name);
        foreach($image_name as $image){
            unlink('assets/images/products/'.$user_id.'/'.$image_name);
        }
        Product::destroy($product_id); 
    }
}
