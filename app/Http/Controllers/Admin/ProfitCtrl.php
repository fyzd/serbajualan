<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Models\Profit;

class ProfitCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $profit=Profit::with('order.invoices')->paginate(50);

        return view('admin.profit')->with(['profits'=>$profit]);
    }
}
