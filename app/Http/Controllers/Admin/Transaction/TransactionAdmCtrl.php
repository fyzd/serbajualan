<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Datatables;

use App\Http\Models\Invoice;
use App\Http\Models\Invoice_Status;
use App\Http\Models\Order;
use App\Http\Models\Order_Item;
use App\Http\Models\Order_Status;
use App\Http\Models\Wallet;
use App\Http\Models\Wallet_Mutation;
use App\Http\Models\Product;
use App\Http\Models\Commission;
use App\Http\Models\Profit;

class TransactionAdmCtrl extends Controller
{
    public $status, $status2;
    
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.transaction.transaction');
    }

    public function datatables()
    {
        $orders=Order::get();

        return Datatables::collection($orders)->make(true);
    }

    public function transactionDetail($order_id)
    {
        $barang=0;
        $kirim=0;

        $order=Order::with('order_item.products.stores.users.user_name', 'order_item.products.product_image_first', 'order_item.shipments.buyer_address.kecamatan','order_item.shipments.buyer_address.kota','order_item.shipments.buyer_address.provinsi')->where('id', $order_id)->first();
        
        foreach($order->order_item as $o)
        {
            $barang+=$o->price*$o->quantity;
            $kirim+=$o->shipments->cost;
        }

        $total=$barang+$kirim;
        

        return view('admin.transaction.transaction_detail')->with(['orders'=>$order, 'barang'=>$barang, 'kirim'=>$kirim, 'total'=>$total]);
    }

    public function invoice(Request $request)
    {
        $filters=collect($request->only(['filter']));

        $invoice=Invoice::with('invoice_status', 'order.shipments.buyer_address.user.user_name', 'order.order_item')->when($filters->get('filter')!=null, function($q) use ($filters){
            $this->status=$filters->get('filter');
            return $q->where('invoice_status_id', $filters->get('filter'));
        })->orderBy('created_at', 'desc')->paginate(25);

        $status_detail=Invoice_Status::get();

        return view('admin.transaction.invoice')->with(['invoices' => $invoice, 'status' => $this->status, 'status_detail' => $status_detail]);
    }

    public function invoiceDetail($id)
    {
        $invoice=Invoice::with('users.user_info','order.order_item.products.product_image_first', 'order.order_item.products.product_category', 'order.order_item.products.product_subcategory', 'order.shipments.buyer_address.kecamatan','order.shipments.buyer_address.kota','order.shipments.buyer_address.provinsi', 'order.shipments.store_address.kecamatan','order.shipments.store_address.kota','order.shipments.store_address.provinsi', 'invoice_status', 'order.order_status', 'order.refund_mutation', 'order.selling_mutation')->where('id', $id)->first();

        $commission=Commission::all();
        
        return view('admin.transaction.invoice_detail')->with(['invoice'=>$invoice, 'commission'=>$commission]);
    }

    public function cancelPurchase(Request $request)
    {
        Invoice::where('id', $request->id)->update([
            'invoice_status_id' => 4
        ]);

        Order::where('invoice_id', $request->id)->update([
            'order_status_id'   => 6
        ]);

        $order_id=Order::select('id')->where('invoice_id', $request->id)->pluck('id');

        Order_Item::whereIn('order_id', $order_id)->update([
            'order_item_status_id'   => 5
        ]);
        
        $items=Order_Item::select('product_id', 'quantity')->whereIn('order_id', $order_id)->get();

        foreach($items as $item)
        {
            $product=Product::find($item->product_id);
            $product->quantity+=$item->quantity;
            $product->save();
        }
        
        return back();
    }

    public function receivedConfirmation(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required'
        ]);

        Order::where('id', $request->order_id)->update([
            'order_status_id'  => 4
        ]);

        Order_Item::where('order_id', $request->order_id)->update([
            'order_item_status_id'  => 3
        ]);

        return back();
    }

    public function rejectOrder(Request $request)
    {

        $this->validate($request, [
            'order_id'  => 'required',
            'reason'    => 'required|min:10|max:191'
        ]);

        Order::where('id', $request->order_id)->update([
            'order_status_id'   => 7,
            'order_details'     => $request->reason
        ]);

        return back();
    }

    public function order(Request $request)
    {
        $filters=collect($request->only(['filter']));

        $order=Order::with('order_status', 'invoices')->when($filters->get('filter')!=null, function($q) use ($filters){
            $this->status2=$filters->get('filter');
            return $q->where('order_status_id', $filters->get('filter'));
        })->orderBy('created_at', 'desc')->paginate(25);

        $status_detail=Order_Status::get();

        return view('admin.transaction.order')->with(['orders' => $order, 'status' => $this->status2, 'status_detail' => $status_detail]);
    }

    public function orderDetail($id)
    {
        $order=Order::select('id', 'invoice_id', 'created_at', 'order_status_id', 'order_details')->with('invoices.invoice_status', 'order_status','shipments','order_item.products.product_image_first', 'order_item.products.product_category', 'order_item.products.product_subcategory', 'order_status', 'order_item.reviews','shipments.buyer_address.kecamatan','shipments.buyer_address.kota','shipments.buyer_address.provinsi','shipments.buyer_address.user.user_name', 'selling_mutation')->where('id', $id)->first();

        $commission=Commission::all();

        return view('admin.transaction.order_detail')->with(['o'=>$order, 'commission'=>$commission]);
    }

    public function mutation($mutdetid, array $data)
    {
        Validator::make($data, [
            /*'buyer_id' => 'required',*/
            'order_id' => 'required',
            'amount' => 'required|numeric'
        ]);

        $wallet=Wallet::firstOrCreate([
            'user_id'   => $data['user_id']
        ]);
        $wallet->balance += $data['amount'];
        $wallet->save();
        
        $mutation= new Wallet_Mutation();
        $mutation->wallet_id            = $wallet->id;
        $mutation->mutation             = +$data['amount'];
        $mutation->remaining_bal        += $data['amount'];
        $mutation->mutation_detail_id   = $mutdetid;
        $mutation->order_id             = $data['order_id'];
        $mutation->save();
    }
    public function refund(Request $request)
    {
        $this->mutation(2, $request->all());

        return back();
    }

    public function transfer(Request $request)
    {
        $this->mutation(3, $request->all());

        Profit::create([
            'order_id'=>$request->order_id,
            'amount'=>$request->commission
        ]);

        return back();
    }
}
