<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;
use Auth;

//Models
use App\Http\Models\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'username' => 'required|string|max:191|unique:users',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $role=DB::table('roles')->select('id')->where('name','user')->first();
        foreach($role as $roleId){
            $roleid=$roleId;
        }

        $user=User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'api_token' => bcrypt($request->email),
            'username'  => $request->username,
            'role_id'   => $roleid
        ]);

        $datauser=$user->toArray();
        $datauser['token']=str_random(25);

        $user_id=User::find($user->id);
        $user_id->token=$datauser['token'];
        $user_id->save();
        
        $mail=Mail::send('user.mails.confirmation', $datauser, function($message) use($user){
            $message->to($user['email']);
            $message->subject('Registration Confirmation');
        });

        return response()->json(['status' => 'success', 'message' => 'Harap cek email'], 201);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'emailusername' => 'required',
            'password'      => 'required'
        ]);

        if(Auth::attempt(['email' => $request->emailusername, 'password' => $request->password, 'confirmed' => 1]))
        {
            return response()->json(['status' => 'success', 'message' => 'Berhasil Login'], 200);
        }
        else if(Auth::attempt(['username' => $request->emailusername, 'password' => $request->password, 'confirmed' => 1]))
        {
            return response()->json(['status' => 'success', 'message' => 'Berhasil Login'], 200);
        }
        else if(Auth::attempt(['email' => $request->emailusername, 'password' => $request->password, 'confirmed' => 0]))
        {
            return response()->json(['status' => 'failed', 'message' => 'Harap konfirmasi email'], 422);
        }
        else if(Auth::attempt(['username' => $request->emailusername, 'password' => $request->password, 'confirmed' => 0]))
        {
            return response()->json(['status' => 'failed', 'message' => 'Harap konfirmasi email'], 422);
        }
        else
        {
            return response()->json(['status' => 'failed', 'message' => 'Email/Password salah'], 422);
        }
    }

    public function cek()
    {
        $user=User::all();
        return response()->json($user);
    }
}
