<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Http\Controllers\User\Checkout\CartCtrl;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $cart;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CartCtrl $cart)
    {
        $this->middleware('guest')->except('logout');
        $this->cart = $cart;
    }

    public function index()
    {
        return view('user.auth.login');
    }

    protected function login(Request $request)
    {
        $this->validator($request->all())->validate();
        
        if(Auth::guard('web')->attempt(['email' => $request->emailusername, 'password' => $request->password, 'role_id'=>2],$request->has('remember')) || Auth::guard('web')->attempt([ 'username' => $request->emailusername, 'password' => $request->password, 'role_id'=>2], $request->has('remember'))){
            $cookie=$this->cart->merge();
            return redirect()->intended();
        } 
        else
        {
            return back()->withErrors('Wrong E-mail address or Password');
        }
        
    }

    protected function indexAdmin()
    {
        return view('admin.auth.login');
    }

    protected function loginAdmin(Request $request)
    {
        $this->validator($request->all())->validate();

        if(Auth::guard('web')->attempt(['email' => $request->emailusername, 'password' => $request->password, 'role_id'=>1],$request->has('remember')) || Auth::guard('web')->attempt([ 'username' => $request->emailusername, 'password' => $request->password, 'role_id'=>1], $request->has('remember'))){
            return redirect('admin');
        }
        else
        {
            return back()->withErrors('Wrong E-mail address or Password');
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'emailusername' => 'required',
            'password' => 'required',
        ]);
    }
    

   
}
