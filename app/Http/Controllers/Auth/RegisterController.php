<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//Auth Facade used in guard
use Auth;
use Mail;

use App\Http\Models\User;
use App\Http\Models\User_Information;
use App\Http\Models\Store;
use App\Http\Models\Seller_Courier;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'confirmation']);
    }

    public function register(Request $request)
    {
        //Validates data
        $this->validator($request->all())->validate();

       //Create user
        $data=$this->create($request->all())->toArray();

        User_Information::create([
            'user_id' => $data['id'],
            'name' => $request->name,
            'sex' => $request->sex
        ]);

        Store::create([
            'user_id' => $data['id'],
        ]);

        Seller_Courier::create([
            'user_id' => $data['id'],
        ]);

        $data['token']=str_random(25);
        $data['name']=$request->name;
        $user=User::find($data['id']);
        $user->token=$data['token'];
        $user->save();

        Mail::send('user.mails.confirmation', $data, function($message) use($data){
            $message->to($data['email']);
            $message->subject('Konfirmasi Pendaftaran Serbajualan');
        });

        //$this->guard()->login($user);

       //Redirects user
        return redirect($this->redirectTo)->with('status','E-mail konfirmasi telah dikirim, harap periksa e-mail Anda.');
    }
    
    public function confirmation($token)
    {
        $user=User::where('token', $token)->first();
        if(!is_null($user)){
            $user->timestamps=false;
            $user->confirmed=1;
            $user->token='';
            $user->save();
            return redirect($this->redirectTo)->with('status','Selamat! akun Anda telah aktif, kini Anda dapat melakukan aktivitas jual beli di Serbajualan.');
        }
        return redirect($this->redirectTo)->with('status','Terjadi kesalahan.');
    }

    public function index()
    {
        return view('user.auth.register');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:191',
            'username' => 'required|string|max:191|unique:users',
            'sex' => 'required|max:1',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $role=DB::table('roles')->select('id')->where('name','user')->first();
        foreach ($role as $id) {
            $namarole = $id;
        }

        return User::create([
            'role_id' => $namarole,
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
    }

    protected function guard()
    {
        return Auth::guard('web_user');
    }


}
