<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Http\Controllers\User\Checkout\CartCtrl;

class CartComposer{
    protected $cart;
    
    public function __construct(CartCtrl $cart)
    {
        $this->cart = $cart;
    }

    public function compose(View $view)
    {
        $view->with('cart', $this->cart);
    }
}
?>
