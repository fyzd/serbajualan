<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Http\Models\Product_Category;

class CategoryComposer{

    public function compose(View $view)
    {
        $category=Product_Category::select('id','description','slug')->with('product_subcategory')->get();

        $view->with('categories', $category);
    }
}
?>
