<?php

namespace App\Http\Traits;

use Auth;
use RajaOngkir;
use DB;
use Session;
use App\Http\Controllers\User\Checkout\CartCtrl;

use App\Http\Models\User_Address;
use App\Http\Models\Seller_Courier;
use App\Http\Models\User_Information;
use App\Http\Models\Product;

trait OngkirTrait
{
    public $address, $info, $cart2, $result, $weight=0, $subtotal=0;

    public function getOngkir()
    {
        $this->cart2=new CartCtrl;
        $this->address=User_Address::where(['user_id' => Auth::id()])->with(['provinsi', 'kota', 'kecamatan'])->OrderBy('primary', 'desc')->get();
        $this->info=User_Information::where('user_id', Auth::id())->first();
        
        $ongkir=$this->ongkir($this->address[0]->kota->nama);

        return $ongkir;
    }

    public function getOngkirIfAddressChanged($address_id)
    {
        $this->cart2=new CartCtrl;
        $this->address=User_Address::where(['id' => $address_id])->with(['provinsi', 'kota', 'kecamatan'])->first();
        $this->info=User_Information::where('user_id', Auth::id())->first();
        
        $ongkir=$this->ongkir($this->address->kota->nama);

        return $ongkir;
    }

    public function ongkir($destination)
    {
        $api_kota=RajaOngkir::Kota()->all();
        if($api_kota == null)
            return false;
        else
        {
            foreach($this->cart2->details() as $order)
            {
                foreach($order->productsOnCart as $product)
                {
                    $this->weight+=$product->weight * $product->cart->quantity;
                    $this->subtotal+=$product->price_after_disc * $product->cart->quantity;

                    //Session Quantity
                    $q[$product->product_id]=$product->cart->quantity;

                }

                $kurir=[];
                $cost=[];
    
                $courier=Seller_Courier::select('jne','pos','tiki')->where('user_id', $order->user_id)->first();
    
                if($courier->jne==1)
                    array_push($kurir, 'jne');
                if($courier->pos==1)
                    array_push($kurir, 'pos');
                if($courier->tiki==1)
                    array_push($kurir, 'tiki');
    
                foreach ($kurir as $k)
                {
    
                    $data[] = RajaOngkir::Cost([
                        'origin' 		=> $this->getIdKota($order->users->address->kota->nama, $api_kota), // id kota asal
                        'destination' 	=> $this->getIdKota($destination, $api_kota), // id kota tujuan
                        'weight' 		=> $this->weight, // berat satuan gram
                        'courier' 		=> $k, // kode kurir pengantar ( jne / tiki / pos )
                    ])->get();
                }
                
                if(count($data[0][0]['costs'])==0)
                {
                    $cost=0;
                    $c[$order->id]=$cost;
                    $service=0;
                    $u[$order->id]=$service;
                }
                else
                {
                    
                    $cost=$data[0][0]['costs'][0]['cost'][0]['value'];
    
                    $service=strtoupper($data[0][0]['code']).' '.$data[0][0]['costs'][0]['service'];
    
                    $sess=Session::get('ongkir', []);
                    $serv=Session::get('service', []);
                    
                    if(array_key_exists($order->id, $sess)){
                        $cost= $sess[$order->id]; 
                    }
                    $c[$order->id]=$cost;
    
                    if(array_key_exists($order->id, $serv)){
                        $service= $serv[$order->id]; 
                    }
                    $u[$order->id]=$service;
                }
                    
                
    
                //$kota_asal[]=$order['kota_asal']->nama;
                if($data == null)
                    return 'Kurir kosong';
                    /* 
                foreach($data as $kurir){
                    $ongkir[]=$kurir;
                    foreach($kurir['costs'] as $d){
                        foreach($d['cost'] as $a){
                            $ongkir[]='<option value="'.$d['service'].'">'.$d['service'].' Rp '.$a['value'].' '.$a['etd'].' hari</option>';
                        }
                    }
                    */
                    //$product=Product::select('name', 'price')->where('id', $order['product_id'])->first();
                    /*$product=DB::table('products')
                                ->select('products.id','products.name as name','products.price','users.name as user', 'stores.id as store_id','products.user_id as user_id', 'product_image.name as image')
                                ->join('users', 'products.user_id', '=', 'users.id')
                                ->join('stores', 'products.store_id', '=', 'stores.id')
                                ->join('product_image', 'products.id', 'product_image.product_id')
                                ->where('products.id', $order['product_id'])
                                ->first();
                                */
                    
                //$store_address_id=User_Address::select('id')->where('user_id', $product->user_id)->where('primary', 1)->first();


                $order['ongkir']= $data;
                $order['costongkir']= $cost;
                $order['service']= $service;
                $order['subtotal']= $this->subtotal;
                $order['store_address']=$order->users->address->id;
                
                unset($data);
                $this->weight=0;
                $this->subtotal=0;

                $this->result[]=$order;
            }
            Session::put('ongkir', $c);
            Session::put('service', $u);
            Session::put('quantity', $q);

            return $this->result;
            
        }
    }

    public function getIdKota($kota_asal, $api_kota)
    {
        //Ambil id kota yang sama dengan kota user dari api RajaOngkir
        if(strpos($kota_asal, 'KOTA ') === false) 
        {
            $kota_asal=ucwords(strtolower(str_replace('KAB. ','',$kota_asal)));
            foreach($api_kota as $k)
            {
                if($k['type'] == 'Kabupaten' && $k['city_name'] == $kota_asal)
                    return $k['city_id'];
            }
        }
        else 
        {
            $kota_asal=ucwords(strtolower(str_replace('KOTA ','',$kota_asal)));
            foreach($api_kota as $k)
            {
                if($k['type'] == 'Kota' && $k['city_name'] == $kota_asal )
                    return $k['city_id'];
            }
        }
    }

}