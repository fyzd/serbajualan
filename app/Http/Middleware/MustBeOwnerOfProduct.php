<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use App\Http\Models\Product;
use Auth;

class MustBeOwnerOfProduct
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $product_id = $request->route('product_id');
        $user = Product::select('user_id')->where('id',$product_id)->first();
        $user_id=$user->user_id;
        if ($user_id === Auth::id()) {
            return $next($request);
        }

        return redirect()->to('/'); // Nope! Get outta' here.
    }
}
