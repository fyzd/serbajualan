<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\User\Checkout\CartCtrl;

use App\Http\Models\Product;

class ProductMustBeAvailable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cart=new CartCtrl();
        
        foreach($cart->details() as $order)
        {
            foreach($order->productsOnCart as $product)
            {
                if($product->quantity < $product->cart->quantity || $product->cart->quantity<=0)
                    return back()->withErrors('Pembelian '.$product->name.' tidak boleh lebih dari '.$product->quantity);
            }
           
        }
        
        return $next($request);
    }
}
