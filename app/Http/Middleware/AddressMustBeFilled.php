<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

use App\Http\Models\User_Address;

class AddressMustBeFilled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User_Address::select('user_id')->where('user_id', Auth::id())->count()>0)
            return $next($request);
        else
            return redirect('user_addresses')->withErrors('Alamat harus diisi terlebih dahulu');
    }
}
