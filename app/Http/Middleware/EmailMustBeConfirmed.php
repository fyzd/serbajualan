<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class EmailMustBeConfirmed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->confirmed > 0){
            return $next($request);
        }
        return back()->withErrors('Email harus dikonfirmasi terlebih dahulu');
    }
}
