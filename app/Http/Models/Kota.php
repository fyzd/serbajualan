<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table='kota';

    public function provinsi()
    {
        return $this->belongsTo('App\Http\Models\Provinsi');
    }

    public function kecamatan()
    {
        return $this->hasMany('App\Http\Models\Kecamatan');
    }
}
