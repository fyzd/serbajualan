<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Order extends Model
{
    protected $table='orders';
    protected $guarded=[];
    
    public function order_item()
    {
        return $this->hasMany('App\Http\Models\Order_Item');
    }

    /* :D
    public function sale_order_item()
    {
        return $this->hasMany('App\Http\Models\Order_Item', 'order_id')->select('order_id', 'order_items.id as oitid','shipments.service', 'order_items.store_id', 'products.name', 'products.id', 'product_image.name as image', 'products.user_id', 'order_items.quantity', 'product_categories.slug as cat', 'product_subcategories.slug as subcat', 'address', 'kecamatan.nama as kec', 'kota.nama as kota', 'provinsi.nama as prov', 'order_items.price', 'shipments.cost', 'products.weight', 'pp', 'zip_code', 'users_addresses.phone', 'order_item_status_id', 'tracking_number')->whereHas('products', function($q){
            $q->where('user_id', Auth::id());
        })
        ->join('products', 'order_items.product_id', 'products.id')
        ->join('product_categories', 'products.product_categories_id', 'product_categories.id')
        ->join('product_subcategories', 'products.product_subcategories_id', 'product_subcategories.id')
        ->join('product_image', 'products.id', 'product_image.product_id')
        ->join('shipments', 'order_items.id', 'shipments.order_item_id')
        ->join('users_addresses', 'shipments.buyer_address_id', 'users_addresses.id')
        ->join('provinsi', 'users_addresses.prov_id', 'provinsi.id')
        ->join('kota', 'users_addresses.kota_id', 'kota.id')
        ->join('kecamatan', 'users_addresses.kec_id', 'kecamatan.id')
        ->groupBy('order_items.id');
    }
    */

    public function order_status()
    {
        return $this->belongsTo('App\Http\Models\Order_Status');
    }

    public function shipments()
    {
        return $this->hasOne('App\Http\Models\Shipment', 'order_id')->select('order_id','service','cost', 'tracking_number', 'buyer_address_id','store_address_id', 'updated_at');
    }

    public function invoices()
    {
        return $this->belongsTo('App\Http\Models\Invoice', 'invoice_id');
    }

    public function refund_mutation()
    {
        return $this->hasOne('App\Http\Models\Wallet_Mutation', 'order_id')->where('mutation_detail_id', 2);
    }

    public function selling_mutation()
    {
        return $this->hasOne('App\Http\Models\Wallet_Mutation', 'order_id')->where('mutation_detail_id', 3);
    }
}
