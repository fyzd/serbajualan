<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table='carts';
    public $timestamps=false;

    protected $guarded=[];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User');
    }
}
