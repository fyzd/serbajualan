<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    protected $table='commission';
    protected $guarded=[];
    public $timestamps=false;

}
