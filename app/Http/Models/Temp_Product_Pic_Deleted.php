<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Temp_Product_Pic_Deleted extends Model
{
    protected $table='temp_product_pic_deleted';
    public $timestamps=false;

    protected $fillable = [
        'product_id', 'user_id', 'name' 
    ];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User');
    }
}
