<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword;

class User extends Authenticatable
{
    use Notifiable;
    protected $table='users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username','role_id','token','api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function temp_product_pic()
    {
        return $this->hasMany('App\Http\Models\Temp_Product_Pic');
    }

    public function product()
    {
        return $this->hasMany('App\Http\Models\Product');
    }

    public function address()
    {
        return $this->hasOne('App\Http\Models\User_Address')->select('id','user_id', 'prov_id', 'kota_id')->where('primary', 1)->with('provinsi', 'kota');
    }

    public function courier()
    {
        return $this->hasOne('App\Http\Models\Seller_Courier');
    }

    public function user_info()
    {
        return $this->hasOne('App\Http\Models\User_Information');
    }

    public function store()
    {
        return $this->hasOne('App\Http\Models\Store');
    }

    public function user_name()
    {
        return $this->hasOne('App\Http\Models\User_Information', 'user_id')->select('user_id','name');
    }

    public function user_image()
    {
        return $this->hasOne('App\Http\Models\User_Information', 'user_id')->select('user_id','image');
    }
    
}
