<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class User_Address extends Model
{
    protected $table='users_addresses';
    public $timestamps=false;
    protected $guarded=[];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User');
    }

    public function provinsi()
    {
        return $this->belongsTo('App\Http\Models\Provinsi', 'prov_id');
    }

    public function kota()
    {
        return $this->belongsTo('App\Http\Models\Kota', 'kota_id');
    }

    public function kecamatan()
    {
        return $this->belongsTo('App\Http\Models\Kecamatan', 'kec_id');
    }
}
