<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table='reviews';
    protected $guarded=[];

    public function users()
    {
        return $this->belongsTo('App\Http\Models\User', 'reviewers')->select('id');
    }

    public function products()
    {
        return $this->belongsTo('App\Http\Models\Product', 'product_id')->select('id', 'name', 'user_id', 'product_categories_id', 'product_subcategories_id', 'price', 'quantity');
    }
}
