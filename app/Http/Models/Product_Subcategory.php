<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Product_Subcategory extends Model
{
    protected $table='product_subcategories';
    public $timestamps=false;

    public function product()
    {
        return $this->hasMany('App\Http\Models\Product', 'product_subcategories_id');
    }

    public function product_category()
    {
        return $this->belongsTo('App\Http\Models\Product_Category', 'parent_category_id');
    }
}
