<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Product_Image extends Model
{
    protected $table="product_image";
    public $timestamps=false;

    public function products()
    {
        return $this->belongsTo('App\Http\Models\Product', 'product_id');
    }
}
