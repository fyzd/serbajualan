<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Product_Category extends Model
{
    protected $table='product_categories';
    public $timestamps=false;

    public function product()
    {
        return $this->hasMany('App\Http\Models\Product', 'product_categories_id');
    }

    public function product_subcategory()
    {
        return $this->hasMany('App\Http\Models\Product_Subcategory', 'parent_category_id')->withCount('product');
    }
}
