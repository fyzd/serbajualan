<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Store extends Model
{
    protected $table='stores';
    protected $guarded=[];
    public $timestamps=false;

    public function users()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id')->select('id','username');
    }

    public function productsOnCart()
    {
        return $this->hasMany('App\Http\Models\Product', 'store_id')->select('id', 'store_id', 'quantity',  'product_categories_id', 'product_subcategories_id', 'name', 'price', 'weight')->with('product_category', 'product_subcategory', 'product_image_first', 'users.address')->whereHas('cart', function($q){
            $q->where('user_id', Auth::id());
        });
    }

    public function products()
    {
        return $this->hasMany('App\Http\Models\Product', 'store_id')->select('id', 'store_id', 'quantity',  'product_categories_id', 'product_subcategories_id', 'name', 'price', 'weight')->with('product_category', 'product_subcategory', 'product_image_first');
    }
}
