<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Invoice extends Model
{
    protected $table='invoices';
    protected $guarded=[];

    public function order()
    {
        return $this->hasMany('App\Http\Models\Order', 'invoice_id');
    }

    public function invoice_status()
    {
        return $this->belongsTo('App\Http\Models\Invoice_Status');
    }

    public function users()
    {
        return $this->belongsTo('App\Http\Models\User', 'buyer_id')->select('id', 'username','email');
    }
}
