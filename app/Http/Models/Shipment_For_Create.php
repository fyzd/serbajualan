<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Shipment_For_Create extends Model
{
    protected $table='shipments';
    protected $guarded=[];
    
    public function setUpdatedAt($value) {
		
	}

    public function buyer_address()
    {
        return $this->belongsTo('App\Http\Models\User_Address', 'buyer_address_id');
    }

}
