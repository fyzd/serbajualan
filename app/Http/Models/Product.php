<?php

namespace App\Http\Models;

use Auth;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User');
    }

    public function users()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id')->select('id', 'username', 'created_at');
    }

    public function product_category()
    {
        return $this->belongsTo('App\Http\Models\Product_Category', 'product_categories_id')->select(['id','description','slug']);
    }

    public function product_subcategory()
    {
        return $this->belongsTo('App\Http\Models\Product_Subcategory', 'product_subcategories_id')->select(['id','description','slug']);
    }

    public function product_image()
    {
        return $this->hasMany('App\Http\Models\Product_Image')->select(['product_id', 'name']);
    }

    public function product_image_first()
    {
        return $this->hasOne('App\Http\Models\Product_Image')->select(['product_id', 'name']);
    }

    public function visitor()
    {
        return $this->hasMany('App\Http\Models\Visitor')->select('product_id', 'ip');
    }

    public function stores()
    {
        return $this->belongsTo('App\Http\Models\Store', 'store_id');
    }

    public function store_status()
    {
        return $this->belongsTo('App\Http\Models\Store', 'store_id')->select('id', 'closed');
    }

    public function reviews()
    {
        return $this->hasMany('App\Http\Models\Review', 'product_id')->select('product_id', 'reviewers', 'rating', 'comment', 'updated_at')->orderBy('created_at', 'desc');;
    }

    public function review_rating()
    {
        return $this->hasMany('App\Http\Models\Review', 'product_id')->select('product_id', 'rating');
    }

    public function sold()
    {
        return $this->hasMany('App\Http\Models\Order_Item', 'product_id')->where('order_item_status_id', 4);
    }

    public function discount()
    {
        return $this->hasOne('App\Http\Models\Discount', 'product_id')->where('expire', '>', date('Y-m-d H:i:s'));
    }

    public function discount_expire()
    {
        return $this->hasOne('App\Http\Models\Discount', 'product_id');
    }

    public function cart()
    {
        return $this->hasOne('App\Http\Models\Cart', 'product_id')->select('user_id', 'product_id', 'quantity')->where('user_id', Auth::id());
    }
}
