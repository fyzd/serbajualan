<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet_Mutation extends Model
{
    protected $table='wallet_mutations';
    protected $guarded=[];
    public $timestamps=true;

    public function wallets()
    {
        return $this->belongsTo('App\Http\Models\Wallet');
    }

    public function wallet_mutation_detail()
    {
        return $this->belongsTo('App\Http\Models\Wallet_Mutation_Detail', 'mutation_detail_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Http\Models\Invoice', 'invoice_id')->select('id', 'invoice_number');
    }

    public function order()
    {
        return $this->belongsTo('App\Http\Models\Order', 'order_id')->select('id', 'invoice_id');
    }
}
