<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class User_Information extends Model
{
    protected $table='users_informations';
    protected $guarded=[];
    public $timestamps=false;

    public function users()
    {
        return $this->belongsTo('App\Http\Models\User');
    }
}
