<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Temp_Product_Pic extends Model
{
    protected $table='temp_product_pic';
    public $timestamps=false;

    protected $fillable = [
        'user_id', 'name' 
    ];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User');
    }

}
