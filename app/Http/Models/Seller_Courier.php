<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Seller_Courier extends Model
{
    protected $table='seller_courier';
    public $timestamps=false;

    protected $guarded=[];

    public function users()
    {
        return $this->belongsTo('App\Http\Models\User');
    }
}
