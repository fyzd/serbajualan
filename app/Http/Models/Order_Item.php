<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Order_Item extends Model
{
    protected $table='order_items';
    protected $guarded=[];
    public $timestamps=false;

    public function users()
    {
        return $this->belongsTo('App\Http\Models\User');
    }

    public function orders()
    {
        return $this->belongsTo('App\Http\Models\Order', 'order_id')->select('id', 'order_details','created_at');
    }

    public function products()
    {
        return $this->belongsTo('App\Http\Models\Product', 'product_id')->select('id','user_id','name','weight','quantity','store_id', 'product_categories_id', 'product_subcategories_id', 'price');
    }

    public function order_item_status()
    {
        return $this->belongsTo('App\Http\Models\Order_Item_Status', 'order_item_status_id');
    }

    public function reviews()
    {
        return $this->hasOne('App\Http\Models\Review', 'order_item_id')->select('order_item_id', 'rating', 'updated_at', 'comment');
    }
}
