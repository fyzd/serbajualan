<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table='kecamatan';

    public function kota()
    {
        return $this->belongsTo('App\Http\Models\Kecamatan');
    }
}
