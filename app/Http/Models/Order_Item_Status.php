<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Order_Item_Status extends Model
{
    protected $table='order_item_status';
    protected $guarded=[];
    public $timestamps=false;

}
