<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table='discounts';
    protected $guarded=[];
    public $timestamps=false;

    public function products()
    {
        return $this->belongsTo('App\Http\Models\Product');
    }
}
