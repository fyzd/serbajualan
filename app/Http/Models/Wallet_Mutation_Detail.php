<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet_Mutation_Detail extends Model
{
    protected $table='wallet_mutation_details';
    protected $guarded=[];
    public $timestamps=false;

    public function wallet_mutations()
    {
        return $this->hasMany('App\Http\Models\Wallet_Mutation');
    }
}
