<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table='visitor_registry';

    public function products()
    {
        return $this->belongsTo('App\Http\Models\Product');
    }
}
