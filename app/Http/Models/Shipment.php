<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $table='shipments';
    protected $guarded=[];

    public function setCreatedAt($value) {
		
	}

    public function buyer_address()
    {
        return $this->belongsTo('App\Http\Models\User_Address', 'buyer_address_id');
    }

    public function store_address()
    {
        return $this->belongsTo('App\Http\Models\User_Address', 'store_address_id');
    }

}
