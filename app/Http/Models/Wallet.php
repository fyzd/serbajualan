<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table='wallets';
    protected $guarded=[];
    public $timestamps=false;

    public function users()
    {
        return $this->belongsTo('App\Http\Models\User');
    }

    public function wallet_mutations()
    {
        return $this->hasMany('App\Http\Models\Wallet_Mutation')->orderBy('created_at', 'desc');
    } 
}
