<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Profit extends Model
{
    protected $table='profit';
    protected $guarded=[];

    public function order()
    {
        return $this->hasOne('App\Http\Models\Order', 'id')->select('id', 'invoice_id');
    }

}
