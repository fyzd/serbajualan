<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateShipmentsAndOrderItemsAddSomeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedInteger('store_id')->after('order_id')->nullable();

            $table->foreign('store_id')->references('id')->on('stores')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('buyer_address_id')->after('cost')->nullable();
            $table->unsignedInteger('store_address_id')->after('buyer_address_id')->nullable();

            $table->foreign('buyer_address_id')->references('id')->on('users_addresses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('store_address_id')->references('id')->on('users_addresses')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });

        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('buyer_address_id');
            $table->dropColumn('store_address_id');
        });
    }
}
