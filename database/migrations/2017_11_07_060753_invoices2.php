<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoices2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
        });

        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->char('invoice_number', 25);
            $table->unsignedInteger('buyer_id');
            $table->unsignedInteger('amount');
            $table->unsignedInteger('invoice_status_id');
            $table->string('details');
            $table->timestamps();

            $table->foreign('buyer_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('invoice_status_id')->references('id')->on('invoice_status')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_status');
        Schema::dropIfExists('invoices');
    }
}
