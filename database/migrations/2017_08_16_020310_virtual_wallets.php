<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VirtualWallets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('virtual_wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('balance');

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });

        Schema::create('virtual_wallet_mutation_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');   
        });

        Schema::create('virtual_wallet_mutations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('virtual_wallet_id');
            $table->integer('mutation');
            $table->unsignedInteger('remaining_bal');
            $table->unsignedInteger('mutation_detail_id');
            $table->unsignedInteger('invoice_id')->nullable();
            $table->unsignedInteger('order_id')->nullable();

            $table->timestamps();

            $table->foreign('virtual_wallet_id')->references('id')->on('virtual_wallets')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('mutation_detail_id')->references('id')->on('virtual_wallet_mutation_details')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_wallets');
        Schema::dropIfExists('virtual_wallet_mutation_details');
        Schema::dropIfExists('virtual_wallet_mutations');
    }
}
