<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route User
Route::get('/users/account', 'User\Account\AccountCtrl@index');
Route::get('user_addresses', 'User\Account\AccountCtrl@settingAddress');
Route::get('user_addresses/new', 'User\Account\AccountCtrl@newAddressView');
Route::post('user_addresses/new/post', 'User\Account\AccountCtrl@newAddress');
Route::get('user_addresses/{id}/update', 'User\Account\AccountCtrl@updateAddressView');
Route::put('user_addresses/{id}/update/put', 'User\Account\AccountCtrl@updateAddress');
Route::delete('user_addresses/{id}/delete', 'User\Account\AccountCtrl@deleteAddress');

Route::get('u/{username}', 'User\Account\AccountCtrl@storeView');


//Route Setting User
Route::put('user_addresses/set_primary', 'User\Account\AccountCtrl@updatePrimaryAddress');
Route::get('account_settings', 'User\Account\AccountCtrl@settingHome');
Route::get('store_settings', 'User\Account\AccountCtrl@storeSetting');
Route::get('payment/invoices', 'User\Account\AccountCtrl@transactionView');
Route::get('payment/invoices/{invoice_number}', 'User\Account\AccountCtrl@transactionDetail');
Route::get('sale', 'User\Account\AccountCtrl@saleView');
Route::get('sale/{id}', 'User\Account\AccountCtrl@saleDetail');
Route::put('sale/update_tr', 'User\Account\AccountCtrl@updateTrackingNumber');
Route::post('payment/received', 'User\Account\AccountCtrl@receivedConfirmation');
Route::get('payment/transaction/{order_id}/product_reviews', 'User\Account\AccountCtrl@reviewView');

Route::put('sale/reject_order', 'User\Account\AccountCtrl@rejectOrder');

Route::get('payment/add-review/{order_id}/{order_item_id}', 'User\Account\AccountCtrl@addReviewView');
Route::post('payment/add-review', 'User\Account\AccountCtrl@addReview');
Route::get('payment/change-review/{order_id}/{order_item_id}', 'User\Account\AccountCtrl@changeReviewView');
Route::post('payment/change-review', 'User\Account\AccountCtrl@changeReview');

Route::get('users/courier_settings', 'User\Account\AccountCtrl@courierSettingView');
Route::put('users/courier_settings/post', 'User\Account\AccountCtrl@courierSetting');
Route::get('users/edit', 'User\Account\AccountCtrl@editView');
Route::get('users/edit_store', 'User\Account\AccountCtrl@editStoreView');

Route::put('users/general/put', 'User\Account\AccountCtrl@generalUpdate');
Route::put('users/store_general/put', 'User\Account\AccountCtrl@storeGeneralUpdate');
Route::put('users/email/put', 'User\Account\AccountCtrl@emailUpdate');
Route::put('users/password/put', 'User\Account\AccountCtrl@passwordUpdate');
Route::put('users/seller_note/put', 'User\Account\AccountCtrl@sellerNoteUpdate');
Route::put('users/close_store/put', 'User\Account\AccountCtrl@closeStoreUpdate');
//

Route::get('show/kota/{prov_id}', 'User\Account\AccountCtrl@getKota');
Route::get('show/kecamatan/{kota_id}', 'User\Account\AccountCtrl@getKecamatan');

Route::get('my_products', 'User\Product\ProductCtrl@index');
Route::post('/datatables','User\Product\ProductCtrl@datatables')->name('datatables.data');
Route::delete('my_products/delete', 'User\Product\ProductCtrl@multiDelete');

Route::get('dompet', 'User\Account\AccountCtrl@dompetView');

//Home
Route::get('/', 'User\Product\ShowProductCtrl@index');
Route::get('products', 'User\Product\ShowProductCtrl@showProduct');

Route::get('c/{category}', 'User\Product\SortProductCtrl@showByCategory');
Route::get('c/{category}/{subcategory}', 'User\Product\SortProductCtrl@showBySubcategory');
Route::post('change/featured', 'User\Product\ShowProductCtrl@changeFeatured');
Route::post('change/popular', 'User\Product\ShowProductCtrl@changePopular');
Route::post('change/bestselling', 'User\Product\ShowProductCtrl@changeBestSelling');
Route::post('change/discount', 'User\Product\ShowProductCtrl@changeDiscount');
Route::post('change/muchreviewed', 'User\Product\ShowProductCtrl@changeMuchReviewed');

Route::group(['namespace' => 'User\Product', 'middleware' => 'confirmed.email'], function () {
    Route::get('my_products/new', 'ProductCtrl@createView');
    Route::post('my_products/store', 'ProductCtrl@store');
    Route::get('my_products/edit/{product_id}', 'ProductCtrl@editView')->middleware('product.owner');
    Route::put('my_products/update/{product_id}', 'ProductCtrl@update');
    Route::get('my_products/server-images/{product_id}', 'ProductCtrl@getServerImages')->name('server-images');
    Route::get('my_products/get_subcategory', 'ProductCtrl@getSubcategory');
    
});
Route::get('p/{category}/{subcategory}/{product_id}', 'User\Product\ShowProductCtrl@detail');
Route::post('estimated_cost', 'User\Product\ShowProductCtrl@estimatedCost');
Route::post('ro/get_kota', 'User\Product\ShowProductCtrl@roGetKota');
Route::post('ro/total', 'User\Product\ShowProductCtrl@roTotal');

Route::get('/cek', 'User\Checkout\CartCtrl@getProductId');
Route::get('/cart', 'User\Checkout\CartCtrl@index');
Route::post('/cart', 'User\Checkout\CartCtrl@addProduct');
Route::put('/cart/{product_id}', 'User\Checkout\CartCtrl@updateQty');
Route::delete('/cart/{product_id}', 'User\Checkout\CartCtrl@removeProduct');


Route::post('dropzone/store', 'User\Product\DropzoneCtrl@uploadFiles');
Route::post('dropzone/delete', 'User\Product\DropzoneCtrl@delete');



Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/postlogin', 'Auth\LoginController@login');

Route::get('/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/reset', 'Auth\ResetPasswordController@reset');

Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
});

Route::get('/register', 'Auth\RegisterController@index');
Route::post('/postregister','Auth\RegisterController@register');
Route::get('/register/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation');

//Checkout
Route::get('payment/purchases/new', 'User\Checkout\CheckoutCtrl@checkoutView')->middleware('product.available');
Route::post('payment/purchases/new', 'User\Checkout\CheckoutCtrl@orderProduct');
Route::get('payment/purchases/{invoice_number}', 'User\Checkout\CheckoutCtrl@purchasesView');

Route::post('checkout/login', 'User\Checkout\CheckoutCtrl@login');
Route::post('change/courier', 'User\Checkout\CheckoutCtrl@changeCourier');
Route::post('checkout/delete_session', 'User\Checkout\CheckoutCtrl@deleteSession');
Route::post('change/address', 'User\Checkout\CheckoutCtrl@changeAddress');

Route::get('cek', 'User\Checkout\CheckoutCtrl@ongkir');

Route::get('kota', 'User\Checkout\CheckoutCtrl@kota');

//Midtrans
Route::get('/vt_transaction', 'User\Checkout\TransactionController@transaction');
Route::post('/vt_transaction', 'User\Checkout\TransactionController@transaction_process');

Route::get('/snap', 'User\Checkout\CheckoutCtrl@snap');
Route::get('/snaptoken/{order_id}', 'User\Checkout\SnapController@token');
Route::post('snapfinish', 'User\Checkout\SnapController@finish');
Route::get('/tes', 'User\Checkout\SnapController@order_id');
Route::post('notif', 'User\Checkout\SnapController@notification');

Route::post('purchase/wallet', 'User\Checkout\CheckoutCtrl@purchaseWallet');



//Route Admin
Route::get('admin/login', 'Auth\LoginController@indexAdmin');
Route::post('admin/login', 'Auth\LoginController@loginAdmin');

Route::get('admin', 'Admin\DashboardCtrl@index');

Route::group(['namespace' => 'Admin\Transaction'], function() {
    Route::get('admin/transaction', 'TransactionAdmCtrl@index');
    Route::post('admin/transaction/datatables', 'TransactionAdmCtrl@datatables');

    Route::get('admin/transaction/invoices', 'TransactionAdmCtrl@invoice');
    Route::get('admin/transaction/invoices/{id}', 'TransactionAdmCtrl@invoiceDetail');
    Route::put('admin/transaction/invoices/cancel', 'TransactionAdmCtrl@cancelPurchase');
    Route::post('admin/transaction/invoices/receive', 'TransactionAdmCtrl@receivedConfirmation');
    Route::put('admin/transaction/invoices/reject', 'TransactionAdmCtrl@rejectOrder');
    Route::get('admin/transaction/orders', 'TransactionAdmCtrl@order');
    Route::get('admin/transaction/orders/{id}', 'TransactionAdmCtrl@orderDetail');
    Route::post('admin/transaction/orders/refund', 'TransactionAdmCtrl@refund');
    Route::post('admin/transaction/orders/transfer', 'TransactionAdmCtrl@transfer');
});

//Transaksi
Route::get('admin/transaction/{order_id}', 'Admin\Transaction\TransactionAdmCtrl@transactionDetail');

//Komisi
Route::get('admin/income', 'Admin\ProfitCtrl@index');

//Kelola Produk
Route::delete('admin/products/delete', 'Admin\DashboardCtrl@deleteProduct');